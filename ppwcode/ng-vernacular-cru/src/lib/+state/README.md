# Angular Vernacular CRU State Management

This section of the library contains an abstract implementation of how we treat state management in our applications.
The abstract implementation gets you going fast without the need to set up everything over and over again.

> NOTE
>
> We keep track of the presentation mode. This is handled automatically by the `AbstractCruFacade` implementation.
> Because we are relying on the Angular router as well to change the presentation mode visually for the user, it could
> definitely be possible that the presentation mode will no longer be kept in the state itself in the future.

## Example

The following is an example of a full state management service, leveraging the implementations in this CRU library:

```typescript
export declare type StocksStateModel = CruStateModel<Stock>

@StateRepository()
@State<StocksStateModel>({
    name: 'stocks',
    defaults: getDefaultCruState<Stock>()
})
@Injectable()
export class StocksState extends AbstractCruState<Stock, StocksStateModel> {}
```

## Third party library

For state management, we use [ngxs](https://www.ngxs.io/) and the following plugins:

-   [`@ngxs-labs/data`](https://npmjs.com/package/@ngxs-labs/data)

## `CruStateModel<TResource>`

The `CruStateModel` interface defines the basic properties that every state model needs to adhere to. It allows state
management to keep track of the current selected instance (`selectedResource`), how it is presented to the user
(`presentationMode`), the history of the selected resource (`selectedResourceHistory`) and whether something is
happening to it (`isBusy`). The `isBusy` flag is intended to track asynchronous calls, not "the user is busy changing
the values of the resource".

This is a generic interface, allowing you to specify the type of resource that will be tracked in this state.

To use the `CruStateModel` interface for your state model, simply extend it to make your own interface:

```typescript
export interface PersonStateModel extends CruStateModel<Person> {
    allPersons: Array<Person>
}
```

In case the `CruStateModel` interface is sufficient, we still advise to create a dedicated type. This will make it
easier for you in the future when you _do_ need extra properties in your state because you don't need to refactor any
other code.

```typescript
export declare type StocksStateModel = CruStateModel<Stock>
```

## `getDefaultCruState<TResource>`

The `getDefaultCruState<TResource>` function allows you to generate the default value for a state that adheres to the
`CruStateModel` interface. It generates an object where the selected resource and history are set to `null`, the
presentation mode is unset and the busy tracker is set to false.

If you have extended the `CruStateModel<TResource>` interface to match the signature of your state, we advise to also
create a function to get the defaults. This will benefit the testability of the state and allows you to stay up to date
with library changes more easily.

```typescript
export const getDefaultPersonsState = (): PersonsStateModel => ({
    ...getDefaultCruState<Person>(),
    allPersons: []
})
```

## `AbstractCruState<TResource, TStateModel>`

This abstract class represents a base implementation for managing the CRU state of a resource. It handles the selection
of a resource (together with its history) and keeps track of the way the resource is presented to the user or whether
something else is busy working on the selected resource.

The generics force you to make your extension of this abstract class type safe.

-   `TResource extends Record<string, unknown>`
    -   Enforces that your model can be indexed using a string. This has a consequence that your models will likely need
        to extend `Record<string, unknown>` as well, but it allows us in the library to ensure that what you are storing
        in the state has string property names at the root level. We don't know what your property values will be typed
        like, so we set it to `unknown` which is different from `any`.
-   `TStateModel extends CruStateModel<TResource>`
    -   Enforces type safety on the state model that is used internally but more importantly the methods that you are
        writing in your extended class. It provides type safety on `this.ctx.setState` (and others) because
        `TStateModel` is passed as the generic to make `AbstractCruState` extend `NgxsDataRepository`.

> NOTE
>
> For the time being, we are extending `NgxsDataRepository`. We are considering to extend `NgxsImmutableDataRepository`
> instead.

Usage:

```typescript
export class StocksState extends AbstractCruState<Stock, StocksStateModel> {}
```
