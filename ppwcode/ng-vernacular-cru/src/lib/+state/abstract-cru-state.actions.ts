/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { HistoricVersion } from '../data/CRU/historic-version'
import { PresentationMode } from '../presentations/presentation-mode'

export class LoadHistory {
    static readonly type = '[CRU-State] Load History'
    constructor(public historicVersions: Array<HistoricVersion>, public stateName: string) {}
}

export class ResetState {
    static readonly type = '[CRU-State] Reset State'
    constructor(public stateName: string) {}
}

export class SelectResource {
    static readonly type = '[CRU-State] Select Resource'
    constructor(public selectedResource: Record<string, unknown> | null, public stateName: string) {}
}

export class SetBusy {
    static readonly type = '[CRU-State] Set Busy'
    constructor(public isBusy: boolean, public stateName: string) {}
}

export class SetHistoryStale {
    static readonly type = '[CRU-State] Set History Stale'
    constructor(public isStale: boolean, public stateName: string) {}
}

export class SetPresentationMode {
    static readonly type = '[CRU-State] Set Presentation Mode'
    constructor(public presentationMode: PresentationMode, public stateName: string) {}
}
