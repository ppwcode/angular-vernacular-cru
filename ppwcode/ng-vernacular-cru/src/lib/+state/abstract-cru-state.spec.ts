/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { TestState, TestStateModel } from '../testing/state'
import { cruStateTestSuite } from './testing/cru-state-test-suite'
import { createServiceFactory, SpectatorService, SpectatorServiceFactory } from '@ngneat/spectator'
import { NgxsModule } from '@ngxs/store'

import { TestingPerson, testingPersons } from '../testing/testing-person'
import { NgxsDispatchPluginModule } from '@ngxs-labs/dispatch-decorator'
import { NgxsSelectSnapshotModule } from '@ngxs-labs/select-snapshot'
import { PresentationMode } from '../presentations/presentation-mode'
import { HistoricVersion } from '../data/CRU/historic-version'

cruStateTestSuite('AbstractCruState', TestState, testingPersons.johnDoe)

describe('AbstractCruState', () => {
    let spectator: SpectatorService<TestState>
    let resource: TestingPerson

    const createService: SpectatorServiceFactory<TestState> = createServiceFactory({
        service: TestState,
        imports: [
            NgxsModule.forRoot([TestState], { developmentMode: true }),
            NgxsDispatchPluginModule.forRoot(),
            NgxsSelectSnapshotModule.forRoot()
        ]
    })

    beforeEach(() => {
        resource = testingPersons.johnDoe
        spectator = createService()
    })

    afterEach(() => spectator.service.reset())

    it('isBusy selector should work correctly', () => {
        const expectFalse = TestState.isBusy({
            isBusy: false
        } as TestStateModel)
        expect(expectFalse).toEqual(false)
        const expectTrue = TestState.isBusy({ isBusy: true } as TestStateModel)
        expect(expectTrue).toEqual(true)
    })

    it('isHistoryStale selector should work correctly', () => {
        const expectFalse = TestState.isHistoryStale({
            isHistoryStale: false
        } as TestStateModel)
        expect(expectFalse).toEqual(false)
        const expectTrue = TestState.isHistoryStale({ isHistoryStale: true } as TestStateModel)
        expect(expectTrue).toEqual(true)
    })

    it('selectedResource selector should work correctly', () => {
        const expectNull = TestState.selectedResource({
            selectedResource: null
        } as TestStateModel)
        expect(expectNull).toBeNull()
        const expectResource = TestState.selectedResource({
            selectedResource: resource
        } as TestStateModel)
        expect(expectResource).toEqual(resource)
    })

    it('selectedResourceHistory selector should work correctly', () => {
        const expectNull = TestState.selectedResourceHistory({
            selectedResourceHistoricVersions: null
        } as TestStateModel)
        expect(expectNull).toBeNull()
        const historicVersion: HistoricVersion = {
            createdAt: new Date().toISOString(),
            href: 'test'
        }
        const expectHistory = TestState.selectedResourceHistory({
            selectedResourceHistoricVersions: [historicVersion]
        } as TestStateModel)
        expect(expectHistory).toEqual([historicVersion])
    })

    it('presentationMode selector should work correctly', () => {
        const expectUnset = TestState.presentationMode({
            presentationMode: PresentationMode.UNSET
        } as TestStateModel)
        expect(expectUnset).toEqual(PresentationMode.UNSET)
    })
})
