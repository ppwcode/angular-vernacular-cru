/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { HistoricVersion } from '../data/CRU/historic-version'
import { PresentationMode } from '../presentations/presentation-mode'
import { CruStateModel, getDefaultCruState } from './cru-state-model'
import { Action, Selector, StateContext, StateToken } from '@ngxs/store'
import { Dispatch } from '@ngxs-labs/dispatch-decorator'
import {
    LoadHistory,
    ResetState,
    SelectResource,
    SetBusy,
    SetHistoryStale,
    SetPresentationMode
} from './abstract-cru-state.actions'

/**
 * This abstract class represents a base implementation for managing the CRU state of a resource.
 * It handles the selection of a resource and keeps track of the way the resource is presented to the user or whether
 * something else is busy working on the selected resource.
 *
 * The `isBusy` flag is intended for asynchronous API calls that are still being made. It is not intended for
 * "the user is busy changing the values of the resource".
 */
export abstract class AbstractCruState<
    TResource extends Record<string, unknown>,
    TStateModel extends CruStateModel<TResource>
> {
    /** Returns a state token that can be used to identify the current state instance **/
    public get token(): StateToken<TStateModel> {
        // TODO msomers: find way to avoid this cast to any
        return new StateToken<TStateModel>(this.stateIdentifier as any)
    }

    protected abstract stateIdentifier: string

    /** Gets the current isBusy flag from the state. */
    @Selector()
    static isBusy(state: CruStateModel<Record<string, unknown>>): boolean {
        return state.isBusy
    }

    /** Should the next request perform cache busting on the history call? */
    @Selector()
    static isHistoryStale(state: CruStateModel<Record<string, unknown>>): boolean {
        return state.isHistoryStale
    }

    /** Gets the currently selected resource instance from the state or `null` if there isn't one. */
    @Selector()
    static selectedResource(state: CruStateModel<Record<string, unknown>>): Record<string, unknown> | null {
        return state.selectedResource
    }

    /** Gets the currently selected resource history from the state or `null` if there isn't one. */
    @Selector()
    static selectedResourceHistory(state: CruStateModel<Record<string, unknown>>): Array<HistoricVersion> | null {
        return state.selectedResourceHistoricVersions
    }

    /** Gets the mode in which the selected resource is currently presented. */
    @Selector()
    static presentationMode(state: CruStateModel<Record<string, unknown>>): PresentationMode {
        return state.presentationMode
    }

    /**
     * Handles selection of a resource. Changes the presentation mode to VIEW.
     * @param selectedResource The resource to select or null to unselect the current resource.
     */
    @Dispatch() public selectResource(selectedResource: TResource | null) {
        return new SelectResource(selectedResource, this.stateIdentifier)
    }

    @Action(SelectResource)
    public selectResourceAction(ctx: StateContext<TStateModel>, { selectedResource, stateName }: SelectResource) {
        if (stateName === this.stateIdentifier) {
            const presentationMode: PresentationMode = PresentationMode.VIEW
            ctx.setState((state: TStateModel) => ({
                ...state,
                selectedResource,
                presentationMode
            }))
        }
    }

    /**
     * Handles changing the presentation mode.
     * @param presentationMode The new presentation mode to store.
     */
    @Dispatch() public setPresentationMode(presentationMode: PresentationMode) {
        return new SetPresentationMode(presentationMode, this.stateIdentifier)
    }

    @Action(SetPresentationMode)
    public setPresentationModeAction(
        ctx: StateContext<TStateModel>,
        { presentationMode, stateName }: SetPresentationMode
    ) {
        if (stateName === this.stateIdentifier) {
            ctx.setState((state: TStateModel) => ({
                ...state,
                presentationMode
            }))
        }
    }

    /**
     * Handles the flag that indicates whether a process is taking place in which the user should not be able
     * to do things on the selected resource.
     * @param isBusy Whether the resource should be indicated as busy.
     */
    @Dispatch() public setBusy(isBusy: boolean) {
        return new SetBusy(isBusy, this.stateIdentifier)
    }

    @Action(SetBusy)
    public setBusyAction(ctx: StateContext<TStateModel>, { isBusy, stateName }: SetBusy) {
        if (stateName === this.stateIdentifier) {
            ctx.setState((state: TStateModel) => ({
                ...state,
                isBusy
            }))
        }
    }

    /**
     * Handles the flag that indicates whether the history in cache is likely stale.
     * @param isStale Whether the history in cache is likely stale.
     */
    @Dispatch() public setHistoryStale(isStale: boolean) {
        return new SetHistoryStale(isStale, this.stateIdentifier)
    }

    @Action(SetHistoryStale)
    public setHistoryStaleAction(ctx: StateContext<TStateModel>, { isStale, stateName }: SetHistoryStale) {
        if (stateName === this.stateIdentifier) {
            ctx.setState((state: TStateModel) => ({
                ...state,
                isHistoryStale: isStale
            }))
        }
    }

    /**
     * Loads the historic version for the selected resource into the state.
     * @param historicVersions The historic versions to store.
     */
    @Dispatch() public loadHistory(historicVersions: Array<HistoricVersion>) {
        return new LoadHistory(historicVersions, this.stateIdentifier)
    }

    @Action(LoadHistory)
    public loadHistoryAction(ctx: StateContext<TStateModel>, { historicVersions, stateName }: LoadHistory) {
        if (stateName === this.stateIdentifier) {
            ctx.setState((state: TStateModel) => ({
                ...state,
                selectedResourceHistoricVersions: historicVersions
            }))
        }
    }

    /**
     * Resets the state to its default values.
     */
    @Dispatch() reset() {
        return new ResetState(this.stateIdentifier)
    }

    @Action(ResetState)
    // @ts-ignore
    private doReset(ctx: StateContext<TStateModel>, { stateName }: ResetState) {
        if (stateName === this.stateIdentifier) {
            ctx.setState((state: TStateModel) => ({
                ...state,
                ...getDefaultCruState<TResource>()
            }))
        }
    }
}
