/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { PresentationMode } from '../presentations/presentation-mode'
import { getDefaultCruState } from './cru-state-model'

describe('cru-state-model', () => {
    it('should give a default cru state', () => {
        expect(getDefaultCruState()).toEqual({
            selectedResource: null,
            selectedResourceHistoricVersions: null,
            presentationMode: PresentationMode.UNSET,
            isBusy: false,
            isHistoryStale: false
        })
    })
})
