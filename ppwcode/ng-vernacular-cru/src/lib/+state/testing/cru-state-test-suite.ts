/* eslint-disable max-lines-per-function */
/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Type } from '@angular/core'
import { createServiceFactory, SpectatorService, SpectatorServiceFactory } from '@ngneat/spectator'
import { NgxsModule, Store } from '@ngxs/store'
import { PresentationMode } from '../../presentations/presentation-mode'
import { AbstractCruState } from '../abstract-cru-state'
import { CruStateModel } from '../cru-state-model'
import { HistoricVersion } from '../../data/CRU/historic-version'
import { NgxsDispatchPluginModule } from '@ngxs-labs/dispatch-decorator'
import { NgxsSelectSnapshotModule } from '@ngxs-labs/select-snapshot'

/**
 * This test suite can be used to verify the implementation of a state.
 * @param suiteName The name of the test suite. Most likely the same as the name of the state.
 * @param state The state type.
 * @param testingResource The resource instance to use during testing.
 */
export const cruStateTestSuite = <
    TResource extends Record<string, unknown>,
    TState extends AbstractCruState<TResource, CruStateModel<TResource>>
>(
    suiteName: string,
    state: Type<TState>,
    testingResource: TResource
): void => {
    describe(suiteName, () => {
        let spectator: SpectatorService<TState>
        let store: Store
        let resource: TResource
        const createService: SpectatorServiceFactory<TState> = createServiceFactory({
            service: state,
            imports: [
                NgxsModule.forRoot([state], { developmentMode: true }),
                NgxsDispatchPluginModule.forRoot(),
                NgxsSelectSnapshotModule.forRoot()
            ]
        })

        beforeEach(() => {
            spectator = createService()
            resource = testingResource
            store = spectator.inject(Store)
        })

        it('should select a resource', () => {
            const expectNull = store.selectSnapshot((state) => state.teststate.selectedResource)
            expect(expectNull).toBeNull()

            spectator.service.selectResource(resource)
            const expectResource = store.selectSnapshot((state) => state.teststate.selectedResource)
            expect(expectResource).toBe(resource)

            spectator.service.selectResource(null)
            const expectNullAgain = store.selectSnapshot((state) => state.teststate.selectedResource)
            expect(expectNullAgain).toBeNull()
        })

        Object.values(PresentationMode)
            .filter((pm: PresentationMode) => isNaN(parseInt(pm)))
            .forEach((mode: PresentationMode) => {
                it(`should change the presentation mode to ${mode}`, () => {
                    const expectUnset = store.selectSnapshot((state) => state.teststate.presentationMode)
                    expect(expectUnset).toEqual(PresentationMode.UNSET)

                    spectator.service.setPresentationMode(mode)
                    const expectMode = store.selectSnapshot((state) => state.teststate.presentationMode)
                    expect(expectMode).toEqual(mode)
                })
            })
        ;[true, false].forEach((isBusy: boolean) => {
            it(`should update the busy state to ${isBusy}`, () => {
                const expectFalse = store.selectSnapshot((state) => state.teststate.isBusy)
                expect(expectFalse).toEqual(false)

                spectator.service.setBusy(isBusy)

                const expectIsBusy = store.selectSnapshot((state) => state.teststate.isBusy)
                expect(expectIsBusy).toEqual(isBusy)
            })
        })

        it('should load the historic versions', () => {
            const historicVersions: Array<HistoricVersion> = [{ createdAt: new Date().toISOString(), href: '/history' }]

            spectator.service.loadHistory(historicVersions)

            const storeVersions = store.selectSnapshot((state) => state.teststate.selectedResourceHistoricVersions)

            expect(storeVersions).toBe(historicVersions)
        })
    })
}
