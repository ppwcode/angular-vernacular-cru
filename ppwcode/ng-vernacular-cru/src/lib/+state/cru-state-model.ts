/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { HistoricVersion } from '../data/CRU/historic-version'
import { PresentationMode } from '../presentations/presentation-mode'

export interface CruStateModel<TResource> {
    selectedResource: TResource | null
    selectedResourceHistoricVersions: Array<HistoricVersion> | null
    presentationMode: PresentationMode
    isBusy: boolean
    isHistoryStale: boolean
}

export const getDefaultCruState = <TResource>(): CruStateModel<TResource> => ({
    selectedResource: null,
    selectedResourceHistoricVersions: null,
    presentationMode: PresentationMode.UNSET,
    isBusy: false,
    isHistoryStale: false
})
