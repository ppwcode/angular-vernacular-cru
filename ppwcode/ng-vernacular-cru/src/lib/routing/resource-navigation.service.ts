/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { SearchResult } from '../search/search-results'

@Injectable({
    providedIn: 'root'
})
export class ResourceNavigationService {
    constructor(private readonly router: Router) {}

    public async navigateToResource(clickEvent: MouseEvent, resource: SearchResult): Promise<void> {
        if (clickEvent.ctrlKey || clickEvent.metaKey) {
            // Ctrl/Cmd click, open the searchResult in new window!
            window.open(`${location.origin}${location.pathname}#${resource.href}`, '_blank')
            return
        }

        // We haven't got a single clue which searchResult is selected in the search results.
        // All we know is that each search result has a href, and we request navigation to this href.
        // Resolvers down the line will handle all data retrieval.
        await this.router.navigateByUrl(resource.href)
    }
}
