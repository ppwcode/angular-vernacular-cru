/* eslint-disable max-lines-per-function */
/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Type } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { ActivatedRouteSnapshot, Router } from '@angular/router'
import { createServiceFactory, SpectatorService, SpectatorServiceFactory, SpyObject } from '@ngneat/spectator'
import { of, throwError } from 'rxjs'
import { first } from 'rxjs/operators'

import { CruStateModel } from '../../+state/cru-state-model'
import { AbstractCruResolver } from '../abstract-cru-resolver'
import { RouterTestingModule } from '@angular/router/testing'
import { AbstractReadFacade } from '../../data/CRU/abstract-read-facade'
import { GetContext } from '../../data/api-interaction/abstract-read-service'

export interface CruResolverTestSuiteDependencies<
    TResource extends Record<string, unknown>,
    TResolver extends AbstractCruResolver<TResource, TResourceDto, TGetContext>,
    TResourceDto extends Record<string, unknown> = TResource,
    TGetContext extends GetContext<string> = GetContext<string>
> {
    Resolver: Type<TResolver>
    Facade: Type<AbstractReadFacade<TResource, CruStateModel<TResource>, TResourceDto, GetContext<string>>>
}

export const cruResolverTestSuite = <
    TResource extends Record<string, unknown>,
    TResolver extends AbstractCruResolver<TResource, TResourceDto, TGetContext>,
    TResourceDto extends Record<string, unknown> = TResource,
    TGetContext extends GetContext<string> = GetContext<string>
>(
    suiteName: string,
    dependencies: CruResolverTestSuiteDependencies<TResource, TResolver, TResourceDto, TGetContext>,
    testResource: TResource
): void => {
    describe(suiteName, () => {
        let spectator: SpectatorService<TResolver>
        let facade: SpyObject<AbstractReadFacade<TResource, CruStateModel<TResource>, TResourceDto, GetContext<string>>>
        let resource: TResource
        let activatedRouteSnapshot: ActivatedRouteSnapshot
        const createService: SpectatorServiceFactory<TResolver> = createServiceFactory({
            service: dependencies.Resolver,
            imports: [RouterTestingModule],
            mocks: [dependencies.Facade],
            providers: [
                {
                    provide: ActivatedRouteSnapshot,
                    useValue: {
                        params: { id: '1' },
                        queryParamMap: { get: () => null },
                        data: { resourceType: 'person' }
                    }
                }
            ]
        })

        beforeEach(() => {
            resource = testResource
            activatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot)

            spectator = createService()
            facade = spectator.inject(dependencies.Facade)
        })

        describe('happy flow', () => {
            beforeEach(() => {
                facade.getResource.and.returnValue(of(resource))
            })

            it('should load the resource based on the route parameter', async () => {
                await expectAsync(
                    spectator.service.resolve(activatedRouteSnapshot).pipe(first()).toPromise()
                ).toBeResolvedTo(resource)
                expect(facade.selectResource).toHaveBeenCalledWith(resource)
            })

            describe('at query param', () => {
                it('should include the timestamp when in the query params', async () => {
                    const timestamp: string = new Date().toISOString()
                    spyOn(activatedRouteSnapshot.queryParamMap, 'get').and.returnValue(timestamp)
                    await expectAsync(
                        spectator.service.resolve(activatedRouteSnapshot).pipe(first()).toPromise()
                    ).toBeResolvedTo(resource)
                    expect(facade.getResource).toHaveBeenCalledWith({ id: '1' }, timestamp)
                })

                it('should not include the timestamp when not in the query params', async () => {
                    spyOn(activatedRouteSnapshot.queryParamMap, 'get').and.returnValue(null)
                    await expectAsync(
                        spectator.service.resolve(activatedRouteSnapshot).pipe(first()).toPromise()
                    ).toBeResolvedTo(resource)
                    expect(facade.getResource).toHaveBeenCalledWith({ id: '1' }, undefined)
                })
            })
        })

        describe('error flow', () => {
            ;[401, 403, 404].forEach((errorCode) => {
                it(`should redirect to the application root when a ${errorCode} error is thrown`, async () => {
                    facade.getResource.and.returnValue(throwError({ status: errorCode }))
                    const router = spectator.inject(Router)
                    spyOn(router, 'navigateByUrl')
                    await spectator.service.resolve(activatedRouteSnapshot).pipe(first()).toPromise()
                    expect(router.navigateByUrl).toHaveBeenCalledWith('/')
                })
            })

            it(`should trigger last resort handler when other errors are encountered`, async () => {
                facade.getResource.and.returnValue(throwError({ status: 500 }))
                await expectAsync(
                    spectator.service.resolve(activatedRouteSnapshot).pipe(first()).toPromise()
                ).toBeRejected()
            })
        })
    })
}
