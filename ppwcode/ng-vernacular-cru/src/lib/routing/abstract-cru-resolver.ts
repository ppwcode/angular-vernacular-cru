/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router'
import { Observable, of, throwError } from 'rxjs'
import { catchError, tap } from 'rxjs/operators'

import { CruStateModel } from '../+state/cru-state-model'
import { HttpErrorResponse } from '@angular/common/http'
import { inject } from '@angular/core'
import { AbstractReadFacade } from '../data/CRU/abstract-read-facade'
import { GetContext } from '../data/api-interaction/abstract-read-service'

/**
 * This abstract class represents a base implementation for a CRU resource resolver in an Angular application.
 *
 * It implements the Resolve interface and handles resource resolving using the provided facade and route param name.
 */
export abstract class AbstractCruResolver<
    TResource extends Record<string, unknown>,
    TResourceDto extends Record<string, unknown> = TResource,
    TGetContext extends GetContext<string> = GetContext<string>,
    TStateModel extends CruStateModel<TResource> = CruStateModel<TResource>
> implements Resolve<TResource | null>
{
    public abstract facade: AbstractReadFacade<TResource, TStateModel, TResourceDto, TGetContext>
    protected readonly router: Router = inject(Router)

    /**
     * Resolves the resource by getting the param value from the route and passing it to the getByIdentifier on the facade.
     * Calls the `selectResource` method of the facade when the resource has been resolved.
     * @param route The activated route snapshot.
     */
    public resolve(route: ActivatedRouteSnapshot): Observable<TResource | null> {
        const atQueryParam: string | undefined = route.queryParamMap.get('at') ?? undefined

        const getResource =
            route.data.resourceType === this.facade.resourceType
                ? this.facade.getResourceWithHistory(route.params as TGetContext, atQueryParam)
                : this.facade.getResource(route.params as TGetContext, atQueryParam)

        return getResource.pipe(
            tap((resource: TResource) => this.facade.selectResource(resource)),

            catchError((err: HttpErrorResponse) => {
                if ([401, 403, 404].includes(err.status)) {
                    // Resource could not be retrieved, redirect to home
                    void this.router.navigateByUrl('/')
                    return of(null)
                } else {
                    // throw upstream
                    return throwError(err)
                }
            })
        )
    }
}
