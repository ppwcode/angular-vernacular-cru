/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TestBed } from '@angular/core/testing'
import { Router } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'
import { firstValueFrom, of } from 'rxjs'
import { NgxsModule } from '@ngxs/store'
import { TestState } from '../testing/state'
import { testingPersons } from '../testing/testing-person'
import { navigateToSelectedResource } from './navigation'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { NgxsDispatchPluginModule } from '@ngxs-labs/dispatch-decorator'
import { NgxsSelectSnapshotModule } from '@ngxs-labs/select-snapshot'
import { TestCruService } from '../testing/cru-service'
import { TestReadService } from '../testing/read-service'
import { TestFacade } from '../testing/cru-facade'

describe('navigation', () => {
    describe('navigateToSelectedResource', () => {
        let router: Router
        let facade: TestFacade

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [
                    RouterTestingModule,
                    HttpClientTestingModule,
                    NgxsModule.forRoot([TestState], { developmentMode: true }),
                    NgxsDispatchPluginModule.forRoot(),
                    NgxsSelectSnapshotModule.forRoot()
                ],
                providers: [TestCruService, TestFacade, TestReadService]
            })

            router = TestBed.inject(Router)
            facade = TestBed.inject(TestFacade)

            spyOn(router, 'navigateByUrl').and.callFake(() => Promise.resolve(true))
        })

        it('should navigate if there is a selected resource', async () => {
            facade.selectedResource$ = of(testingPersons.johnDoe)

            const navigated: boolean = await firstValueFrom(navigateToSelectedResource(router, facade))

            expect(router.navigateByUrl).toHaveBeenCalledWith(`/testpersons/16`)
            expect(navigated).toBeTrue()
        })

        it('should not navigate if there is no selected resource', async () => {
            facade.selectedResource$ = of(null)

            const navigated: boolean = await firstValueFrom(navigateToSelectedResource(router, facade))

            expect(router.navigateByUrl).not.toHaveBeenCalled()
            expect(navigated).toBeFalse()
        })
    })
})
