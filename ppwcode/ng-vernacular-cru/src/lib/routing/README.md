# Angular CRU Routing

This section of the library focuses on a standardized routing implementation for handling the CRU process.

## `generateCruRoutes`

This function is introduced to speed up the development process and generates route definitions for:

-   A `'create'` route for creating a new resource.
-   A `'read'` route for displaying the resource.
-   An `'update'` route for modifying the resource.
-   A wildcard route (`**`) redirecting to the `'read'` route if a route definition is added.

> It will not generate a route definition for those routes where the component was set to `null`.

It sets up the route configuration to make sure the resource can be displayed as a breadcrumb as well.

The function accepts the resource type as the first parameter (to make sure to call the correct facade when necessary).

There are 4 generic types that can be specified explicitly but can also be inferred based on the value for the
parameters you provide.

-   `TCreate`
    -   The type of the component used to create a new resource.
-   `TRead`
    -   The type of the component used to display the resource details.
-   `TUpdate`
    -   The type of the component used to update the resource.
-   `TRecource`
    -   The type of the resource that is managed in the route.

### Example

```typescript
import { generateCruRoutes } from '@ppwcode/ng-vernacular-cru'

const routes: Routes = generateCruRoutes(
    'MyAwesomeProject.Models.Stock',
    {
        components: {
            create: StockCreateComponent,
            read: StockDetailsComponent,
            update: StockUpdateComponent
        },
        resourceRouteParamName: ':stockId'
    },
    StockResolver
)
```

This example will generate the following route definitions:

```typescript
const routes: Routes = [
    {
        path: 'create',
        component: StockCreateComponent
    },
    {
        path: ':stockId',
        children: [
            {
                path: 'read',
                component: StockDetailsComponent,
                data: {
                    resourceType: 'MyAwesomeProject.Models.Stock'
                },
                resolve: {
                    resource: StockResolver
                }
            },
            {
                path: 'update',
                component: StockUpdateComponent,
                data: {
                    resourceType: 'MyAwesomeProject.Models.Stock'
                },
                resolve: {
                    resource: StockResolver
                }
            },
            {
                path: '**',
                redirectTo: 'read'
            }
        ]
    }
]
```

## `AbstractCruResolver<TResource, TResourceDto>`

The `AbstractCruResolver` contains a base implementation to resolve a resource and load it in state management. It
implements the `Resolve` interface and handles resource resolving using the provided facade and route param name.

The `routeParamName` property is used to define which parameter in the route definition should be used as the unique
identification of the resource. For route `/person/:personId/stock/:stockId`, there are two parameters (`personId` and
`routeId`). We can assign `personId` or `stockId` as the value for `routeParamName` and the standardized resolver
implementation will take care of loading the resource:

1. Get the resource by calling `getByIdentifier` on the facade.
2. Call `selectResource` on the facade to store it in state management.

### Example implementation

The following example is a minimal implementation for the resolver.

```typescript
@Injectable({
    providedIn: 'root'
})
export class StockResolver extends AbstractCruResolver<Stock> {
    public routeParamName: string = 'id'

    constructor(public facade: StocksFacade) {
        super()
    }
}
```

## `navigateToSelectedResource`

According to the facade pattern, it is not allowed to have routing logic in the facade. This function helps with
navigation to ensure you don't have too much duplicate code.

The function receives the Angular Router instance and the facade containing the selected resource instance. It is
important that a resource has been selected before calling this function.

```typescript
@Component(/** ... */)
export class MyComponent {
    public async onClickStock(stock: Stock): Promise<void> {
        await this.stocksFacade.selectResource(resource)
        await navigateToSelectedResource(this.router, this.stocksFacade).toPromise()
    }
}
```

## Testing

A test suite has been made available to verify your own resolvers that extend the `AbstractCruResolver` interface.

At first, you specify the name of the test suite, then the dependencies it needs and finally a resource instance for the
tests to use.

```typescript
cruResolverTestSuite(
    'StockResolver',
    {
        Facade: StocksFacade,
        Resolver: StockResolver
    },
    notUndefined(stockExamples[0])
)
```
