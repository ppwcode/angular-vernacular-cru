/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Type } from '@angular/core'
import { Resolve, Route, Routes } from '@angular/router'

const getReadRouteDefinition = <TRead, TResource = unknown>(
    resourceType: string,
    readComponent: Type<TRead>,
    resolver: Type<Resolve<TResource>>,
    additionalResolvers?: Array<{ name: string; resolver: Type<Resolve<unknown>> }>
): Route => {
    const route = {
        path: 'read',
        component: readComponent,
        data: {
            resourceType
        },
        resolve: {
            resource: resolver
        }
    }

    if (additionalResolvers) {
        const moreResolvers: { [key: string]: Type<Resolve<unknown>> } = {}
        additionalResolvers.forEach((ar) => {
            moreResolvers[ar.name] = ar.resolver
        })
        route.resolve = {
            ...moreResolvers,
            resource: resolver
        }
    }

    return route
}

const getCreateRouteDefinition = <TCreate>(createComponent: Type<TCreate>): Route => ({
    path: 'create',
    component: createComponent
})

const getUpdateRouteDefinition = <TRead, TResource = unknown>(
    resourceType: string,
    updateComponent: Type<TRead>,
    resolver: Type<Resolve<TResource>>,
    additionalResolvers?: Array<{ name: string; resolver: Type<Resolve<unknown>> }>
): Route => {
    const route = {
        path: 'update',
        component: updateComponent,
        data: {
            resourceType
        },
        resolve: {
            resource: resolver
        }
    }

    if (additionalResolvers) {
        const moreResolvers: { [key: string]: Type<Resolve<unknown>> } = {}
        additionalResolvers.forEach((ar) => {
            moreResolvers[ar.name] = ar.resolver
        })
        route.resolve = {
            ...moreResolvers,
            resource: resolver
        }
    }

    return route
}

/**
 * Generates route definitions for:
 * - A `'create'` route for creating a new resource if a component has been specified.
 * - A `'read'` route for displaying the resource if a component has been specified.
 * - An `'update'` route for modifying the resource if a component has been specified.
 * - A wildcard route redirecting to the `'read'` route if the route has been added.
 * @param resourceType The type of resource associated to the routes.
 * @param options The configuration options for generating the routes.
 * @param resolver The resolver used to resolve the resource.
 * @param additionalResolvers Optional array of additional resolvers, tuples { name, resolver }
 */
export const generateCruRoutes = <TRead, TCreate, TUpdate, TResource = unknown>(
    resourceType: string,
    options: CruRoutesOptions<TRead, TCreate, TUpdate>,
    resolver: Type<Resolve<TResource>>,
    additionalResolvers?: Array<{ name: string; resolver: Type<Resolve<unknown>> }>
): Routes => {
    const { components, resourceRouteParamName } = options
    const routes: Routes = []
    const resourceDetailRoutes: Routes = []
    let includeWildcard: boolean = false

    // Routes independent of a resource instance.
    if (components.create) {
        routes.push(getCreateRouteDefinition(components.create))
    }

    // Routes dependent on a resource instance.
    if (components.read) {
        includeWildcard = true
        resourceDetailRoutes.push(getReadRouteDefinition(resourceType, components.read, resolver, additionalResolvers))
    }
    if (components.update) {
        resourceDetailRoutes.push(
            getUpdateRouteDefinition(resourceType, components.update, resolver, additionalResolvers)
        )
    }
    if (includeWildcard) {
        resourceDetailRoutes.push({
            path: '**',
            redirectTo: 'read'
        })
    }
    if (resourceDetailRoutes.length) {
        routes.push({
            path: resourceRouteParamName,
            children: resourceDetailRoutes
        })
    }

    return routes
}

export interface CruRoutesOptions<TRead, TCreate, TUpdate> {
    components: CruRoutesComponents<TRead, TCreate, TUpdate>
    resourceRouteParamName: string
}

export interface CruRoutesComponents<TRead, TCreate, TUpdate> {
    create: Type<TCreate> | null
    read: Type<TRead> | null
    update: Type<TUpdate> | null
}
