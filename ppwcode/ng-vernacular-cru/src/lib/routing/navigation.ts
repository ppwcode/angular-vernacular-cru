/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Router } from '@angular/router'
import { Observable, of } from 'rxjs'
import { first, switchMap } from 'rxjs/operators'

import { CruStateModel } from '../+state/cru-state-model'
import { GetContext } from '../data/api-interaction/abstract-read-service'
import { AbstractCruFacade } from '../data/CRU/abstract-cru-facade'

/**
 * This function will navigate the application to the selected resource path of the given facade.
 * If there's no selected resource available for the facade, no navigation will take place.
 * @param router The Angular router taking care of the navigation.
 * @param facade The facade for which to start the navigation.
 * @param pathContext Extra context, used in some cases to determine resource identifier. Optional.
 */
export const navigateToSelectedResource = <
    TResource extends Record<string, unknown>,
    TStateModel extends CruStateModel<TResource> = CruStateModel<TResource>,
    TResourceDto extends Record<string, unknown> = TResource,
    TGetContext extends GetContext<string> = GetContext<string>,
    TPathContext extends Record<string, unknown> = TResource
>(
    router: Router,
    facade: AbstractCruFacade<TResource, TStateModel, TResourceDto, TGetContext, TPathContext>,
    pathContext?: TPathContext
): Observable<boolean> => {
    return facade.selectedResource$.pipe(
        first(),
        switchMap((selectedResource: TResource | null) => {
            if (selectedResource) {
                return router.navigateByUrl(
                    `${facade.resourceAppPath}/${facade.getResourceUniqueIdentifier(selectedResource, pathContext)}`
                )
            } else {
                return of(false)
            }
        })
    )
}
