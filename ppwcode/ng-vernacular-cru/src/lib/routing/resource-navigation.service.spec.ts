import { createServiceFactory, SpectatorService } from '@ngneat/spectator'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { TestBed } from '@angular/core/testing'
import { ResourceNavigationService } from './resource-navigation.service'
import { Router } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'

describe('ResourceNavigationService', () => {
    let spectator: SpectatorService<ResourceNavigationService>
    const createService = createServiceFactory({
        service: ResourceNavigationService,
        imports: [HttpClientTestingModule, RouterTestingModule],
        providers: []
    })
    let router: Router

    beforeEach(() => {
        spectator = createService()
        router = TestBed.inject(Router)
        spyOn(router, 'navigateByUrl').and.callFake(() => Promise.resolve(true))
    })

    it('should navigate when selecting a resource', async () => {
        const href = '/some/path/to/resource/112'
        await spectator.service.navigateToResource(new MouseEvent('click'), { href, discriminator: 'test' })
        expect(router.navigateByUrl).toHaveBeenCalledWith(href)
    })

    it('should open resource in new tab when ctrl/cmd+clicking a search result', async () => {
        const openSpy = spyOn(window, 'open')
        const href = '/some/path/to/resource/112'
        await spectator.service.navigateToResource(new MouseEvent('click', { ctrlKey: true }), {
            href,
            discriminator: 'test'
        })
        expect(openSpy).toHaveBeenCalledWith(`${location.origin}${location.pathname}#${href}`, '_blank')
    })
})
