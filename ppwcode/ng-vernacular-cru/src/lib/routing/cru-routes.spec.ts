/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Component, Injectable } from '@angular/core'
import { Resolve } from '@angular/router'

import { generateCruRoutes } from './cru-routes'

@Component({ template: '' })
class MockCreateComponent {}

@Component({ template: '' })
class MockReadComponent {}

@Component({ template: '' })
class MockUpdateComponent {}

@Injectable()
class MockResolver implements Resolve<boolean> {
    public resolve(): boolean {
        return true
    }
}

describe('cru-routes', () => {
    it('should not generate a route if there were no components given', () => {
        expect(
            generateCruRoutes(
                'TestingPerson',
                {
                    components: {
                        create: null,
                        read: null,
                        update: null
                    },
                    resourceRouteParamName: ':testingPersonId'
                },
                MockResolver
            )
        ).toEqual([])
    })

    it('should generate a create route', () => {
        expect(
            generateCruRoutes(
                'TestingPerson',
                {
                    components: {
                        create: MockCreateComponent,
                        read: null,
                        update: null
                    },
                    resourceRouteParamName: ':testingPersonId'
                },
                MockResolver
            )
        ).toEqual([
            {
                path: 'create',
                component: MockCreateComponent
            }
        ])
    })

    it('should generate a read route with wildcard route', () => {
        expect(
            generateCruRoutes(
                'TestingPerson',
                {
                    components: {
                        create: null,
                        read: MockReadComponent,
                        update: null
                    },
                    resourceRouteParamName: ':testingPersonId'
                },
                MockResolver
            )
        ).toEqual([
            {
                path: ':testingPersonId',
                children: [
                    {
                        path: 'read',
                        component: MockReadComponent,
                        data: { resourceType: 'TestingPerson' },
                        resolve: { resource: MockResolver }
                    },

                    { path: '**', redirectTo: 'read' }
                ]
            }
        ])
    })

    it('should generate a read route with wildcard route and additional resolvers', () => {
        expect(
            generateCruRoutes(
                'TestingPerson',
                {
                    components: {
                        create: null,
                        read: MockReadComponent,
                        update: null
                    },
                    resourceRouteParamName: ':testingPersonId'
                },
                MockResolver,
                [{ name: 'extra', resolver: MockResolver }]
            )
        ).toEqual([
            {
                path: ':testingPersonId',
                children: [
                    {
                        path: 'read',
                        component: MockReadComponent,
                        data: { resourceType: 'TestingPerson' },
                        resolve: { resource: MockResolver, extra: MockResolver }
                    },

                    { path: '**', redirectTo: 'read' }
                ]
            }
        ])
    })

    it('should generate an update route', () => {
        expect(
            generateCruRoutes(
                'TestingPerson',
                {
                    components: {
                        create: null,
                        read: null,
                        update: MockUpdateComponent
                    },
                    resourceRouteParamName: ':testingPersonId'
                },
                MockResolver
            )
        ).toEqual([
            {
                path: ':testingPersonId',
                children: [
                    {
                        path: 'update',
                        component: MockUpdateComponent,
                        data: { resourceType: 'TestingPerson' },
                        resolve: { resource: MockResolver }
                    }
                ]
            }
        ])
    })

    it('should generate an update route with additional resolvers', () => {
        expect(
            generateCruRoutes(
                'TestingPerson',
                {
                    components: {
                        create: null,
                        read: null,
                        update: MockUpdateComponent
                    },
                    resourceRouteParamName: ':testingPersonId'
                },
                MockResolver,
                [{ name: 'test', resolver: MockResolver }]
            )
        ).toEqual([
            {
                path: ':testingPersonId',
                children: [
                    {
                        path: 'update',
                        component: MockUpdateComponent,
                        data: { resourceType: 'TestingPerson' },
                        resolve: { resource: MockResolver, test: MockResolver }
                    }
                ]
            }
        ])
    })

    it('should generate a full CRU route list', () => {
        expect(
            generateCruRoutes(
                'TestingPerson',
                {
                    components: {
                        create: MockCreateComponent,
                        read: MockReadComponent,
                        update: MockUpdateComponent
                    },
                    resourceRouteParamName: ':testingPersonId'
                },
                MockResolver
            )
        ).toEqual([
            {
                path: 'create',
                component: MockCreateComponent
            },
            {
                path: ':testingPersonId',
                children: [
                    {
                        path: 'read',
                        component: MockReadComponent,
                        data: { resourceType: 'TestingPerson' },
                        resolve: { resource: MockResolver }
                    },
                    {
                        path: 'update',
                        component: MockUpdateComponent,
                        data: { resourceType: 'TestingPerson' },
                        resolve: { resource: MockResolver }
                    },
                    { path: '**', redirectTo: 'read' }
                ]
            }
        ])
    })
})
