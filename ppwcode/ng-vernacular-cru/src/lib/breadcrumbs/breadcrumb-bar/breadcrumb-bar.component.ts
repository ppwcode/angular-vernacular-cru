import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core'
import { ActivatedRoute, Data, Event, NavigationEnd, ResolveData, Route, Router } from '@angular/router'
import { Observable } from 'rxjs'
import { filter } from 'rxjs/operators'

import { CruResourcesManagerService } from '../../data/CRU/cru-resources-manager.service'
import { mixinHandleSubscriptions } from '../../performance/handle-subscriptions.mixin'
import { GenericBreadcrumb } from '../cru-breadcrumb'
import { GenericReadFacade } from '../../data/CRU/abstract-read-facade'

/**
 * This component is responsible for displaying the breadcrumbs of the application.
 */
@Component({
    selector: 'ppwcode-cru-breadcrumb-bar',
    templateUrl: './breadcrumb-bar.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BreadcrumbBarComponent extends mixinHandleSubscriptions() implements OnInit {
    @Input() public breadcrumbs: Array<GenericBreadcrumb> = []

    /* istanbul ignore next */
    /** Stream that emits when navigation has ended. */
    public navigationEnd$: Observable<NavigationEnd> = this.router.events.pipe(
        filter((event: Event) => event instanceof NavigationEnd)
    ) as Observable<NavigationEnd>

    constructor(
        private readonly router: Router,
        private readonly activatedRoute: ActivatedRoute,
        private readonly cruResourcesManager: CruResourcesManagerService,
        private readonly changeDetectorRef: ChangeDetectorRef
    ) {
        super()
    }

    public ngOnInit(): void {
        this.stopOnDestroy(this.navigationEnd$).subscribe(() => {
            this.breadcrumbs = this.generateBreadcrumbs(this.activatedRoute)
            this.changeDetectorRef.detectChanges()
        })
    }

    /**
     * Generates breadcrumbs for the given activated route and its children.
     * A breadcrumb will be generated if the configuration for the route contains a resolver.
     * Breadcrumbs will be generated top-down so you most likely want to pass the top-level activated route
     * as the parameter.
     * @param activatedRoute The activated route to start with.
     */
    public generateBreadcrumbs(activatedRoute: ActivatedRoute): Array<GenericBreadcrumb> {
        let currentRoute: ActivatedRoute | null = activatedRoute
        const breadcrumbs: Array<GenericBreadcrumb> = []

        while (currentRoute) {
            const routeConfig: Route | null = currentRoute.routeConfig
            if (routeConfig !== null) {
                const resolveConfig: ResolveData | undefined = routeConfig.resolve

                if (resolveConfig && Object.keys(resolveConfig).length) {
                    // TODO (glenstaes): maybe the data should come out of state management?
                    breadcrumbs.push(this.generateBreadcrumb(currentRoute.snapshot.data))
                }
            }

            currentRoute = currentRoute.firstChild
        }

        return breadcrumbs
    }

    /**
     * Generates a breadcrumb for the given resource of the given type.
     * Relies on a facade to have been registered in the CruResourcesManagerService.
     * @param resourceType The type of the resource.
     * @param resource The actual resource value.
     */
    public generateBreadcrumb({ resourceType, resource }: Data): GenericBreadcrumb {
        const facade: GenericReadFacade = this.cruResourcesManager.getFacadeInstance(resourceType)
        return {
            resource,
            resourceType,
            displayText: facade.getResourceDisplayText(resource)
        }
    }
}
