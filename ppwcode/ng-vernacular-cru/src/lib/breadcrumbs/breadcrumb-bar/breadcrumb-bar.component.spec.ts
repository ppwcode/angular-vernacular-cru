import { TestBed } from '@angular/core/testing'
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'
import { createRoutingFactory, Spectator, SpyObject } from '@ngneat/spectator'
import { notNull } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert'
import { Subject } from 'rxjs'

import { CruResourcesManagerService } from '../../data/CRU/cru-resources-manager.service'
import { TestResolver } from '../../testing/resolver'
import { TestingPerson, testingPersons } from '../../testing/testing-person'
import { GenericBreadcrumb } from '../cru-breadcrumb'
import { BreadcrumbBarComponent } from './breadcrumb-bar.component'
import { TestFacade } from '../../testing/cru-facade'

describe('BreadcrumbBarComponent', () => {
    let spectator: Spectator<BreadcrumbBarComponent>
    const createComponent = createRoutingFactory({
        component: BreadcrumbBarComponent,
        imports: [RouterTestingModule],
        mocks: [TestFacade, Router],
        data: {
            resourceType: 'TestingPerson',
            resource: testingPersons.johnDoe
        }
    })

    beforeEach(() => {
        spectator = createComponent({ detectChanges: false })

        const cruResourcesManager: CruResourcesManagerService = TestBed.inject(CruResourcesManagerService)
        cruResourcesManager.registerResource('TestingPerson', TestFacade)

        const testFacade: SpyObject<TestFacade> = spectator.inject(TestFacade)
        testFacade.getResourceDisplayText.and.returnValue('John')
    })

    it('should generate breadcrumbs when navigation ends', () => {
        const subject = new Subject<NavigationEnd>()
        spectator.component.navigationEnd$ = subject.asObservable()
        spectator.detectChanges()

        spyOn(spectator.component, 'generateBreadcrumbs')

        subject.next(new NavigationEnd(1, '/test', '/test'))

        expect(spectator.component.generateBreadcrumbs).toHaveBeenCalledTimes(1)
    })

    describe('when subscribed to navigation end events', () => {
        beforeEach(() => spectator.detectChanges())

        it('should generate breadcrumbs for the activated route and its children', () => {
            const resourceType: string = 'TestingPerson'
            const resource: TestingPerson = testingPersons.johnDoe
            const mockActivatedRoute: ActivatedRoute = {
                routeConfig: { resolve: { resource: TestResolver } },
                snapshot: { data: { resourceType, resource } }
                // For this test, we only need the route config and snapshot data so casting this is safe for now.
            } as unknown as ActivatedRoute

            const bc: Array<GenericBreadcrumb> = spectator.component.generateBreadcrumbs(mockActivatedRoute)
            expect(bc).toEqual([
                {
                    resource,
                    resourceType,
                    displayText: 'John'
                }
            ])
        })

        it('should not generate a breadcrumb when there is no route config available', () => {
            const resourceType: string = 'TestingPerson'
            const resource: TestingPerson = testingPersons.johnDoe
            const mockActivatedRoute: ActivatedRoute = {
                routeConfig: null,
                snapshot: { data: { resourceType, resource } }
                // For this test, we only need the snapshot data so casting this is safe for now.
            } as unknown as ActivatedRoute

            const bc: Array<GenericBreadcrumb> = spectator.component.generateBreadcrumbs(mockActivatedRoute)
            expect(bc).toEqual([])
        })

        it('should not generate a route if there is no resolve config available', () => {
            const resourceType: string = 'TestingPerson'
            const resource: TestingPerson = testingPersons.johnDoe
            const mockActivatedRoute: ActivatedRoute = {
                routeConfig: {},
                snapshot: { data: { resourceType, resource } }
                // For this test, we only need the route config and snapshot data so casting this is safe for now.
            } as unknown as ActivatedRoute

            const bc: Array<GenericBreadcrumb> = spectator.component.generateBreadcrumbs(mockActivatedRoute)
            expect(bc).toEqual([])

            notNull(mockActivatedRoute.routeConfig).resolve = {}
            const bc2: Array<GenericBreadcrumb> = spectator.component.generateBreadcrumbs(mockActivatedRoute)
            expect(bc2).toEqual([])
        })

        it('should generate a breadcrumb for route data', () => {
            const resourceType: string = 'TestingPerson'
            const resource: TestingPerson = testingPersons.johnDoe
            const breadcrumb: GenericBreadcrumb = spectator.component.generateBreadcrumb({
                resourceType,
                resource
            })

            expect(breadcrumb).toEqual({
                resource,
                resourceType,
                displayText: 'John'
            })
        })
    })
})
