# Angular Vernacular Breadcrumbs

This section of the library has a standardized implementation for a breadcrumb bar.

## `CruBreadcrumb<TResource>`

This interface defines the structure of a breadcrumb. It is used internally to keep track of the breadcrumbs to be shown
inside the breadcrumb bar.

As a consequence of the decision to make the resource type for `AbstractCruState` extend `Record<string, unknown>`, the
generic `TResource` here has the same limitation.

An instance of `CruBreadcrumb` contains the related resource instance, the type of related resource (used in other
places to call the corresponding facade) and the text to display on the breadcrumb so that it doesn't need to be
recalculated.

## `GenericBreadcrumb`

This type equals to `CruBreadcrumb<Record<string, unknown>>` and is used internally to keep the code base more readable.

## `BreadcrumbBarComponent`

This component will listen to the `NavigationEnd` router event and generate breadcrumbs when the event occurs.

The following process is implemented to generate breadcrumbs:

1. Start with the root route.
2. Get the config of the route.
3. Get the resolver config of the route.
4. Extract `resourceType` and `resource` from the route data.
5. Get the correct facade based on the `resourceType`.
6. Get the text on to display from the facade by calling `getResourceDisplayText`.
7. Generate the breadcrumb.
8. Continue with the first child route.

Steps 2-8 are repeated as long as there are child routes to continue with.

If there is no route config or resolver on the config, the route is skipped and the process will continue with the next
child route. In the end, there will be an array of `GenericBreadcrumb` instances.

### Make sure a breadcrumb is created

To make sure that a breadcrumb is created for a route, you need to adhere to the following:

-   A route resolver will resolve the instance to the `resource` property.
-   The route config data contains a `resourceType` property telling the library of which type the resource is. This is
    necessary to call the `getResourceDisplayText` on the correct facade.

[Read the routing docs on how you can set this up automatically.](../routing/README.md)
