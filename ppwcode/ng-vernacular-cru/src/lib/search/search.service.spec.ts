/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { HttpClientTestingModule } from '@angular/common/http/testing'
import { createServiceFactory, SpectatorService } from '@ngneat/spectator'
import { HttpCallTester } from '@ppwcode/ng-testing'

import { SearchService } from './search.service'
import { TestingPerson, testingPersons } from '../testing/testing-person'

describe('SearchService', () => {
    let spectator: SpectatorService<SearchService>
    const createService = createServiceFactory({
        service: SearchService,
        imports: [HttpClientTestingModule]
    })

    beforeEach(() => (spectator = createService()))

    it('should get the search results', () => {
        HttpCallTester.expectOneCallToUrl('/api/I/search?terms=a')
            .whenSubscribingTo(
                spectator.service.search('a', {
                    // @ts-ignore
                    '/I/person': (resource: TestingPerson) => ({
                        ...resource,
                        prop: 'value'
                    }),
                    '/I/person2': (_: unknown) => null
                })
            )
            .withResponse({
                results: [
                    { discriminator: '/I/person', ...testingPersons.johnDoe, href: 'href1' },
                    { discriminator: '/I/person2', ...testingPersons.johnDoe, href: 'href2' },
                    // This won't be hit by a mapper function.
                    { discriminator: '/I/person3', ...testingPersons.johnDoe, href: 'href3' }
                ]
            })
            .expectStreamResultTo((result: unknown) => {
                expect(result).toEqual({
                    results: [
                        {
                            discriminator: '/I/person',
                            ...testingPersons.johnDoe,
                            prop: 'value',
                            href: 'href1'
                        },
                        { discriminator: '/I/person2', ...testingPersons.johnDoe, href: 'href2' },
                        { discriminator: '/I/person3', ...testingPersons.johnDoe, href: 'href3' }
                    ]
                })
            })
            .verify()
    })
})
