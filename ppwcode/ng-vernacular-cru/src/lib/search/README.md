# Angular Vernacular CRU Search

This section of the library is focused on a standardized search implementation.

## `SearchResult`

This interface defines that a search result instance should include the `type` property so that it can be distinguished
from other types. The value of this `type` property will also be used to dynamically load the facade instance for when
the search result should be loaded in state as the selected instance.

## `SearchResults`

This interface defines what the API result of a search call should look like.

It consists of an array of instances that have `SearchResult` implemented and information about pagination href calls
that can be made.

## `SearchService`

This service is a generic implementation of a search endpoint to be called. It receives the terms to search for and the
mappers it should apply to the search results. We need to explicitly specify the mappers because we are not using any
facade logic here.

```typescript
export const SEARCH_RESULT_MAPPERS: {
    [key: string]: (resource: unknown) => SearchResultTypes
} = {
    '/I/person': (resource: unknown) => personFromDto(resource as personSchema)
}

// ... component metadata ...
export class AppComponent {
    private readonly searchMappers = {
        '/I/Persons': personFromDto
    }

    public search$: Subject<string> = new Subject<string>()

    public searchResults$: Observable<SearchResults<Record<string, unknown>>> = this.search$
        .asObservable()
        .pipe(
            switchMap((terms: string) =>
                terms.length === 0
                    ? of({ results: [], href: { first: '' } })
                    : this.searchService.search<Record<string, unknown>>(terms, SEARCH_RESULT_MAPPERS)
            )
        )

    constructor(private readonly searchService: SearchService) {}
}
```
