/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { HttpClient, HttpParams } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'

import { SearchResult, SearchResults } from './search-results'

@Injectable({
    providedIn: 'root'
})
export class SearchService {
    constructor(private readonly httpClient: HttpClient) {}

    public search<TResults>(
        terms: string,
        mappers: { [key: string]: (resource: unknown) => TResults }
    ): Observable<SearchResults<TResults & SearchResult>> {
        return this.httpClient
            .get<SearchResults<TResults & SearchResult>>('/api/I/search', {
                params: new HttpParams().set('terms', terms)
            })
            .pipe(
                map((searchResults: SearchResults<TResults & SearchResult>) => ({
                    ...searchResults,
                    results: searchResults.results.map((result: TResults & SearchResult) => ({
                        ...(mappers[result.discriminator]?.(result) ?? result),
                        discriminator: result.discriminator,
                        href: result.href
                    }))
                }))
            )
    }
}
