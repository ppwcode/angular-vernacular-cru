import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { ReactiveFormsModule } from '@angular/forms'
import { MatLegacyButtonModule as MatButtonModule } from '@angular/material/legacy-button'
import { MatLegacyCardModule as MatCardModule } from '@angular/material/legacy-card'
import { MatLegacyFormFieldModule as MatFormFieldModule } from '@angular/material/legacy-form-field'
import { MatIconModule } from '@angular/material/icon'
import { MatLegacyInputModule as MatInputModule } from '@angular/material/legacy-input'
import { MatLegacyListModule as MatListModule } from '@angular/material/legacy-list'
import { MatLegacyProgressBarModule as MatProgressBarModule } from '@angular/material/legacy-progress-bar'
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatToolbarModule } from '@angular/material/toolbar'
import { RouterModule } from '@angular/router'

import { AppFrameComponent } from './app-frame/app-frame.component'
import { BreadcrumbBarComponent } from './breadcrumbs/breadcrumb-bar/breadcrumb-bar.component'
import { DefaultMiniPresentationComponent } from './presentations/resource/default-mini-presentation/default-mini-presentation.component'
import { DynamicResourcePresentationDirective } from './presentations/resource/dynamic-resource-presentation.directive'
import { SpinnerComponent } from './spinner/spinner.component'
import { MatLegacyProgressSpinnerModule as MatProgressSpinnerModule } from '@angular/material/legacy-progress-spinner'
import { ResourceHistoryComponent } from './presentations/history/resource-history/resource-history.component'
import { MatLegacyMenuModule as MatMenuModule } from '@angular/material/legacy-menu'
import { MatLegacyTooltipModule as MatTooltipModule } from '@angular/material/legacy-tooltip'
import { TranslateModule } from '@ngx-translate/core'
import { FilteredResourceListComponent } from './presentations/relations/filtered-resource-list/filtered-resource-list.component'
import { ResourceListComponent } from './presentations/relations/resource-list/resource-list.component'
import { UserNameButtonComponent } from './app-frame/components/user-name-button/user-name-button.component'

@NgModule({
    declarations: [
        AppFrameComponent,
        SpinnerComponent,
        DynamicResourcePresentationDirective,
        DefaultMiniPresentationComponent,
        BreadcrumbBarComponent,
        ResourceHistoryComponent,
        FilteredResourceListComponent,
        ResourceListComponent,
        UserNameButtonComponent
    ],
    imports: [
        CommonModule,
        MatSidenavModule,
        MatListModule,
        MatIconModule,
        MatToolbarModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule,
        MatCardModule,
        RouterModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatMenuModule,
        MatTooltipModule,
        TranslateModule
    ],
    exports: [
        AppFrameComponent,
        SpinnerComponent,
        DynamicResourcePresentationDirective,
        DefaultMiniPresentationComponent,
        BreadcrumbBarComponent,
        ResourceHistoryComponent,
        FilteredResourceListComponent,
        ResourceListComponent
    ]
})
export class PpwcodeVernacularCruModule {}
