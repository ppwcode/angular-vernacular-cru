# Angular Vernacular CRU Performance

This section of the library focuses on performance.

## `mixinHandleSubscriptions`

Within a project, all subscriptions must be handled. When using the `AsyncPipe` in Angular, this is done automatically
but where we subscribe to an observable in our code we should handle the subscription manually. To do this, we leverage
[the most elegant way as described in this article](https://medium.com/@sub.metu/angular-how-to-avoid-memory-leaks-from-subscriptions-8fa925003aa9).
We added some extra syntactic sugar, so we don’t have to add all the boilerplate subjects in our own components,
directives, … .

Basically, what this mixin does is elevating a given base type with the functionality to handle subscriptions. The mixin
returns a new class which we can extend on for our components, directives, pipes, … like this:

```typescript
@Component({ ... })
export class MyComponent extends mixinHandleSubscriptions() {}
```

Because we extend the class returned by the mixin, we can access the stopOnDestroy method to elevate our stream
subscriptions with “automatic” subscription handling:

```typescript
@Component({ ... })
export class MyComponent extends mixinHandleSubscriptions() {
  // ...

  ngOnInit() {
    this.stopOnDestroy(this.service.getAll()).subscribe((allItems) => {
      // ...
    })
  }
}
```

-   This is an OOTB Angular compatible solution, no third party packages are necessary like
    [SubSink](https://github.com/wardbell/subsink).
-   There is no array with subscription instances to maintain.
