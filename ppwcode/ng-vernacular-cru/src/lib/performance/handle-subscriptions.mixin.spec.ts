import { Subject } from 'rxjs'

import { mixinHandleSubscriptions } from './handle-subscriptions.mixin'

describe('mixinHandleSubscriptions', () => {
    it('should extend the base class', () => {
        class BaseClass {
            property = 'value'
        }

        const extendedClass = mixinHandleSubscriptions(BaseClass)

        expect(new extendedClass().property).toEqual('value')
    })

    it('should provide subscription handling logic', () => {
        const classWithSubscriptionLogic = mixinHandleSubscriptions()
        const instance = new classWithSubscriptionLogic()

        const stream$ = new Subject()
        let subscriptionHits = 0
        instance.stopOnDestroy(stream$).subscribe(() => subscriptionHits++)

        for (let i = 0; i < 4; i++) {
            stream$.next('')
        }
        expect(subscriptionHits).toEqual(4)

        instance.ngOnDestroy()

        // Subscription should not have been hit by the next emit.
        stream$.next('')
        expect(subscriptionHits).toEqual(4)

        stream$.complete()
    })
})
