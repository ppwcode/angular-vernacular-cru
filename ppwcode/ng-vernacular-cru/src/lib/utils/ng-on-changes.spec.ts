import { SimpleChange, SimpleChanges } from '@angular/core'

import { testingPersons } from '../testing/testing-person'
import { whenPropertyChanges } from './ng-on-changes'

describe('ngOnChanges utils', () => {
    describe('whenPropertyChanges', () => {
        it('should handle changes to input bindings', () => {
            const simpleChanges: SimpleChanges = {
                resource: new SimpleChange(undefined, testingPersons.johnDoe, true),
                resourceType: new SimpleChange('', 'TestingPerson', false)
            }
            const changeHandlers: { [key: string]: jasmine.Spy } = {
                resource: jasmine.createSpy(),
                resourceType: jasmine.createSpy(),
                unhitHandler: jasmine.createSpy()
            }

            whenPropertyChanges(simpleChanges, changeHandlers)

            expect(changeHandlers.resource).toHaveBeenCalledTimes(1)
            expect(changeHandlers.resourceType).toHaveBeenCalledTimes(1)
            expect(changeHandlers.unhitHandler).toHaveBeenCalledTimes(0)
        })
    })
})
