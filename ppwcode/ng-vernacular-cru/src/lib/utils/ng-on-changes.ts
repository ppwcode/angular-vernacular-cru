import { SimpleChanges } from '@angular/core'
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert'

/**
 * Used to handle changes to the input bindings of a component. Ignores initial changes while instantiating the component.
 *
 * @example
 * ngOnChanges(changes: SimpleChanges): void {
 *   whenPropertyChanges<MyComponent>(changes, {
 *     myProperty: () => this.myFunction()
 *   });
 * }
 * @param changes The SimpleChanges object received in ngOnChanges.
 * @param handleInputChanges The handlers to execute when one of the properties is found in the SimpleChanges object.
 */
export const whenPropertyChanges = <T>(
    changes: SimpleChanges,
    handleInputChanges: Partial<{ [K in keyof T]: () => void }>
): void => {
    // We use this Object.values and the index because of a typing issue if we were to use `handleInputChanges[propertyName]()`.
    // => No index signature with a parameter of type 'string' was found on type 'Partial<{ [K in keyof T]: () => void; }>'.
    const inputChangeHandlers: Array<() => void> = Object.values(handleInputChanges)
    Object.keys(handleInputChanges).forEach((propertyName: string, i: number) => {
        if (changes.hasOwnProperty(propertyName)) {
            notUndefined(inputChangeHandlers[i])()
        }
    })
}
