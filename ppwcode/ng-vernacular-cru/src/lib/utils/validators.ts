/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { AbstractControl, ValidatorFn } from '@angular/forms'
import * as Joi from 'joi'
import { TranslateService } from '@ngx-translate/core'
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert'

/**
 * Creates a Joi validator based on the given schema.
 * Returns the actual Joi error message on the `joi` property.
 * Only includes the first error.
 * @param schema The schema to validate against.
 * @param translateOptions The options for translating the Joi validation errors into readable messages.
 */
export const createJoiValidator = (
    schema: Joi.Schema,
    translateOptions: JoiValidationTranslationOptions
): ValidatorFn => {
    const { translateService, prefix, customValidationMessages }: JoiValidationTranslationOptions = translateOptions

    return (abstractControl: AbstractControl) => {
        const validationResult: Joi.ValidationResult = schema.validate(abstractControl.value)

        if (validationResult.error) {
            const error: Joi.ValidationError = validationResult.error
            const errorDetails: Joi.ValidationErrorItem = notUndefined(error.details[0])
            let errorTranslationKey: string = `${prefix}${errorDetails.type}`

            // Replace the error translation key with a custom one that might be passed.
            // This allows for explicit error messages that better match the implemented logic by the developer.
            // A good use case for this are pattern validators, where regexes are hard to read for an end-user.
            if (customValidationMessages && errorTranslationKey in customValidationMessages) {
                errorTranslationKey = notUndefined(customValidationMessages[errorTranslationKey])
            }

            return {
                joi: {
                    error,
                    ppwTranslatedMessage: translateService.instant(errorTranslationKey, errorDetails.context)
                }
            }
        }

        return null
    }
}

/**
 * This interface describes the necessary options to translate Joi validation errors into readable messages.
 */
export interface JoiValidationTranslationOptions {
    /** The translate service to use. */
    translateService: TranslateService
    /** The prefix of the translation keys. */
    prefix: string
    /** Any custom validation keys to read from first. */
    customValidationMessages?: Record<string, string>
}
