/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Joi from 'joi'
import { FormControl, ValidatorFn } from '@angular/forms'
import { createJoiValidator } from './validators'
import { TestBed } from '@angular/core/testing'
import { TranslateModule, TranslateService } from '@ngx-translate/core'

describe('validator utils', () => {
    describe('createJoiValidator', () => {
        let translateService: TranslateService
        const schema: Joi.Schema = Joi.number().required().min(0).max(100)

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TranslateModule.forRoot()]
            })

            translateService = TestBed.inject(TranslateService)
        })

        it('should create a validator that validates against a Joi schema', () => {
            const validValues: Array<number> = [0, 100, 58]
            const invalidValues: Array<unknown> = [null, undefined, [], {}, 'string value', -1, 101]

            const validator: ValidatorFn = createJoiValidator(schema, {
                translateService,
                prefix: ''
            })
            const testFormControl: FormControl = new FormControl()

            validValues.forEach((validValue: number) => {
                testFormControl.setValue(validValue)
                expect(validator(testFormControl)).toBeNull()
            })

            invalidValues.forEach((invalidValue: unknown) => {
                testFormControl.setValue(invalidValue)
                expect(validator(testFormControl)).not.toBeNull()
            })
        })

        describe('ppwcode validation error messages', () => {
            beforeEach(() => {
                spyOn(translateService, 'instant').and.callThrough()
            })

            it('should support no custom validation messages', () => {
                const validator: ValidatorFn = createJoiValidator(schema, {
                    translateService,
                    prefix: ''
                })

                const testFormControl: FormControl = new FormControl(-3)
                validator(testFormControl)

                expect(translateService.instant).toHaveBeenCalledWith('number.min', {
                    limit: 0,
                    value: -3,
                    label: 'value'
                })
            })

            it('should keep the initial validation message if not found in the custom messages', () => {
                const validator: ValidatorFn = createJoiValidator(schema, {
                    translateService,
                    prefix: '',
                    customValidationMessages: {
                        'number.max': 'custom.number.max'
                    }
                })

                const testFormControl: FormControl = new FormControl(-3)
                validator(testFormControl)

                expect(translateService.instant).toHaveBeenCalledWith('number.min', {
                    limit: 0,
                    value: -3,
                    label: 'value'
                })
            })

            it('should replace the initial validation message with a custom one', () => {
                const validator: ValidatorFn = createJoiValidator(schema, {
                    translateService,
                    prefix: '',
                    customValidationMessages: {
                        'number.max': 'custom.number.max',
                        'number.min': 'custom.number.min'
                    }
                })

                const testFormControl: FormControl = new FormControl(-3)
                validator(testFormControl)

                expect(translateService.instant).toHaveBeenCalledWith('custom.number.min', {
                    limit: 0,
                    value: -3,
                    label: 'value'
                })
            })
        })
    })
})
