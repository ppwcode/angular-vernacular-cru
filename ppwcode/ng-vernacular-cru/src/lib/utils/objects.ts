/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Gets the value for the property at the given path.
 * Returns `undefined` when:
 * - The property path is an empty string.
 * - The given property path is incorrect (missing properties at any level).
 * - The actual value of the property is `undefined`.
 * @param obj The object to look into.
 * @param propertyPath The path to get the property value from.
 */
export const getPropertyValue = (obj: Record<string, unknown>, propertyPath: string): unknown => {
    const properties: Array<string> = propertyPath.split('.').filter((propertyName: string) => propertyName.length)
    let value: Record<string, unknown> | unknown = obj
    let index: number = 0
    let length: number = properties.length
    if (length === 0) {
        return undefined
    }

    let currentPropertyName: string | undefined = properties[index]

    while (index < length && currentPropertyName && (value as Record<string, unknown>)[currentPropertyName]) {
        value = (value as Record<string, unknown>)[currentPropertyName]
        index++
        currentPropertyName = properties[index]
    }

    if (index === length) {
        // We've been through all split property names. This could be `undefined` as well, but in this case it's
        // the actual value of the property.
        return value
    }

    // If we haven't traversed all properties, we couldn't find the property value, so we return `undefined`.
    return undefined
}
