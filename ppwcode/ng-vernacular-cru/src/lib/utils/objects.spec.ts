/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { getPropertyValue } from './objects'

describe('objects utils', () => {
    describe('getPropertyValue', () => {
        const sut: Record<string, unknown> = {
            name: 'John Doe',
            address: {
                firstLine: {
                    streetName: 'Acmestreet',
                    houseNumber: 33
                },
                country: 'USA',
                city: undefined
            }
        }

        const valueExpectations: Record<string, unknown> = {
            name: 'John Doe',
            'address.firstLine': { streetName: 'Acmestreet', houseNumber: 33 },
            'address.firstLine.houseNumber': 33,
            'address.country': 'USA'
        }
        Object.keys(valueExpectations).forEach((propertyPath: string) => {
            it(`should return a value for ${propertyPath}`, () => {
                const expectedValue: unknown = valueExpectations[propertyPath]
                expect(getPropertyValue(sut, propertyPath)).toEqual(expectedValue)
            })
        })

        it('should return undefined as the value of the deepest level', () => {
            expect(getPropertyValue(sut, 'address.city')).toBeUndefined()
        })

        it('should return undefined if not all properties were found', () => {
            expect(getPropertyValue(sut, 'address.secondLine.streetName')).toBeUndefined()
        })

        it('should return undefined if an empty string is given as property path', () => {
            expect(getPropertyValue(sut, '')).toBeUndefined()
        })
    })
})
