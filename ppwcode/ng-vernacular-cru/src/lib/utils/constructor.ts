/** Defines a constructable type. */
// Array<any> is required for mixins.
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Constructor<T> = new (...args: Array<any>) => T
