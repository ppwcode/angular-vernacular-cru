# Angular Vernacular CRU Presentations

This section of the library is focused on presentation information to the user.

## Mini presentations

A mini presentation is a representation of a resource in a small card. The default mini card displays the business
identification (most likely the name or something alike) as the primary text and the summary as the secondary text. The
values for these are retrieved from the facade related to the resource type.

### Custom mini presentations

A custom mini presentation can be registered in the `DynamicMiniPresentationService`:

```typescript
export class AppModule {
    constructor(dynamicMiniPresentationService: DynamicMiniPresentationService) {
        dynamicMiniPresentationService.registerComponents([
            ['/I/person', PersonMiniComponent],
            ['/I/stock', StockMiniComponent]
        ])
    }
}
```

The custom mini presentation components must implement the `MiniPresentationComponent` interface. This interface
enforces the `resource` property to be available. When dynamically generating an instance of this component, the
resource instance will be assigned to that property.

Most of the time, a mini presentation will have almost no logic as its purpose is to display information.

## Resource history

The library is shipped with a component that is capable of displaying the resource history. To keep the taken space as
small as possible, you can pass it the timestamp of the current version that is being displayed. Upon clicking the
timestamp, a menu will open where the user can select the timestamp of a different version.

## `PresentationMode`

This enum contains the different ways in which a resource can be presented to the user.

> NOTE
>
> As we rely on routing, this enum will probably be removed.
