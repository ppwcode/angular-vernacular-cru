/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DynamicResourcePresentationDirective } from './dynamic-resource-presentation.directive'
import { Component, ViewChild } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { DynamicResourcePresentationService } from './dynamic-resource-presentation.service'
import { PpwcodeVernacularCruModule } from '../../vernacular-cru.module'
import { TestingSearchResult, testingSearchResults } from '../../testing/testing-search-result'

describe('DynamicResourcePresentationDirective ', () => {
    let resourcePresentationService: DynamicResourcePresentationService

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [TestComponent],
            imports: [PpwcodeVernacularCruModule]
        })

        resourcePresentationService = TestBed.inject(DynamicResourcePresentationService)
        spyOn(resourcePresentationService, 'createComponent')
    })

    it('should create a component', () => {
        const fixture = TestBed.createComponent(TestComponent)
        fixture.detectChanges()

        expect(resourcePresentationService.createComponent).toHaveBeenCalled()
    })
})

/** Component to test that a text control is instantiated. */
@Component({
    template: `<ng-container ppwcodeCruDynamicResourcePresentation [resource]="resource" [type]="type"></ng-container>`
})
class TestComponent {
    public resource: TestingSearchResult = testingSearchResults.johnDoe

    public type: string = 'person'

    @ViewChild(DynamicResourcePresentationDirective)
    public directive!: DynamicResourcePresentationDirective<TestingSearchResult>
}
