import { MatLegacyCardModule as MatCardModule } from '@angular/material/legacy-card'
import { createComponentFactory, Spectator } from '@ngneat/spectator'

import { DefaultMiniPresentationComponent } from './default-mini-presentation.component'
import { testingPersons } from '../../../testing/testing-person'

describe('DefaultMiniPresentationComponent', () => {
    let spectator: Spectator<DefaultMiniPresentationComponent>
    const createComponent = createComponentFactory({
        component: DefaultMiniPresentationComponent,
        imports: [MatCardModule]
    })

    it('should throw if any of the required properties is not filled', () => {
        expect(() => createComponent()).toThrow()
        expect(() =>
            createComponent({
                props: {
                    resource: testingPersons.johnDoe
                }
            })
        ).toThrow()
        expect(() =>
            createComponent({
                props: {
                    resource: testingPersons.johnDoe,
                    identifierText: 'John'
                }
            })
        ).toThrow()
        expect(() =>
            createComponent({
                props: {
                    resource: testingPersons.johnDoe,
                    identifierText: 'John',
                    summaryText: ''
                }
            })
        ).not.toThrow()
    })

    describe('with assigned properties', () => {
        beforeEach(() => {
            spectator = createComponent({
                props: {
                    resource: testingPersons.johnDoe,
                    identifierText: 'John',
                    summaryText: ''
                }
            })
        })

        it('should create', () => {
            expect(spectator.component).toBeTruthy()
        })
    })
})
