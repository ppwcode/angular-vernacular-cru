import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert'

import { ResourcePresentationComponent } from '../resource-presentation-component'

/**
 * This component is used as a mini presentation for a resource if there's no explicit mini presentation
 * component registered.
 *
 * It displays the identifier text that is returned by the corresponding facade and also its summary text
 * if it has a length.
 */
@Component({
    selector: 'ppwcode-cru-default-mini-presentation',
    templateUrl: './default-mini-presentation.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DefaultMiniPresentationComponent implements OnInit, ResourcePresentationComponent<unknown> {
    // TODO (glenstaes): Custom resource styling
    @Input() public resource: unknown
    @Input() public isMini: boolean = false

    public identifierText!: string
    public summaryText!: string

    public ngOnInit(): void {
        notUndefined(this.resource)
        notUndefined(this.identifierText)
        notUndefined(this.summaryText)
    }
}
