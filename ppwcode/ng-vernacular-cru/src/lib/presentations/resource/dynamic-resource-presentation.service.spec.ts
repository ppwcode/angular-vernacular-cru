import { HttpClientTestingModule } from '@angular/common/http/testing'
import { Component, ComponentRef, ViewContainerRef } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { createComponentFactory, Spectator } from '@ngneat/spectator'
import { NgxsModule } from '@ngxs/store'
import { TestingPersonMiniPresentationComponent } from '../../testing/testing-person-mini-presentation.component'
import { CruResourcesManagerService } from '../../data/CRU/cru-resources-manager.service'
import { TestState } from '../../testing/state'
import { DefaultMiniPresentationComponent } from './default-mini-presentation/default-mini-presentation.component'
import { DynamicResourcePresentationService } from './dynamic-resource-presentation.service'
import { TestFacade } from '../../testing/cru-facade'
import { TestCruService } from '../../testing/cru-service'
import { testingSearchResults } from '../../testing/testing-search-result'

/**
 * This host component is being used to have a real ViewContainerRef to work with.
 */
@Component({
    template: ''
})
class HostComponent {
    constructor(public readonly viewContainerRef: ViewContainerRef) {}
}

describe('DynamicResourcePresentationService', () => {
    let spectator: Spectator<HostComponent>
    const createService = createComponentFactory({
        component: HostComponent,
        declarations: [TestingPersonMiniPresentationComponent, HostComponent],
        imports: [
            HttpClientTestingModule,
            RouterTestingModule,
            NgxsModule.forRoot([TestState], { developmentMode: true })
        ],
        providers: [TestFacade, TestCruService]
    })
    let service: DynamicResourcePresentationService

    beforeEach(() => {
        spectator = createService()
        service = TestBed.inject(DynamicResourcePresentationService)
    })

    it('should register a component', () => {
        expect(service.componentTypeMap.size).toEqual(0)

        service.registerComponent('TestingSearchResult', TestingPersonMiniPresentationComponent)

        expect(service.componentTypeMap.size).toEqual(1)
    })

    it('should register an array of components', () => {
        expect(service.componentTypeMap.size).toEqual(0)

        service.registerComponents([['TestingSearchResult', TestingPersonMiniPresentationComponent]])

        expect(service.componentTypeMap.size).toEqual(1)
    })

    describe('with a component registered', () => {
        beforeEach(() => {
            service.registerComponent('TestingSearchResult', TestingPersonMiniPresentationComponent)

            const cruResourcesManager: CruResourcesManagerService = TestBed.inject(CruResourcesManagerService)
            cruResourcesManager.registerResource('UnknownType', TestFacade)
        })

        it('should create a new instance of the component', () => {
            expect(() => {
                const componentRef: ComponentRef<TestingPersonMiniPresentationComponent> = service.createComponent(
                    spectator.component.viewContainerRef,
                    'TestingSearchResult',
                    testingSearchResults.johnDoe,
                    false
                )
                expect(componentRef.instance).toBeInstanceOf(TestingPersonMiniPresentationComponent)
                expect(componentRef.instance.resource).toBe(testingSearchResults.johnDoe)
            }).not.toThrow()
        })

        it('should fallback to the default mini presentation if the component is not registered', () => {
            const componentRef: ComponentRef<TestingPersonMiniPresentationComponent> = service.createComponent(
                spectator.component.viewContainerRef,
                'UnknownType',
                testingSearchResults.johnDoe,
                false
            )
            expect(componentRef.instance).toBeInstanceOf(DefaultMiniPresentationComponent)
            expect(componentRef.instance.resource).toBe(testingSearchResults.johnDoe)
        })
    })
})
