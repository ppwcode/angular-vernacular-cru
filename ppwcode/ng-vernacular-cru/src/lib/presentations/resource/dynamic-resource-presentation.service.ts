import {
    ComponentFactory,
    ComponentFactoryResolver,
    ComponentRef,
    Injectable,
    Type,
    ViewContainerRef
} from '@angular/core'
import { AbstractDynamicService } from '../../dynamic-components/abstract-dynamic-service'
import { DefaultMiniPresentationComponent } from './default-mini-presentation/default-mini-presentation.component'
import { ResourcePresentationComponent } from './resource-presentation-component'
import { SearchResult } from '../../search/search-results'

/**
 * This service is responsible for managing the different types of mini components for a resource and instantiating
 * them on a view container reference.
 *
 * Each resource type can only have a single mini presentation component so that a uniform layout will be applied
 * everywhere throughout the application.
 */
@Injectable({
    providedIn: 'root'
})
export class DynamicResourcePresentationService extends AbstractDynamicService<ResourcePresentationComponent<unknown>> {
    constructor(private readonly componentFactoryResolver: ComponentFactoryResolver) {
        super()
    }

    /**
     * Instantiates a component on the given view container reference based on the provided type.
     * @param viewContainerRef The view container reference to instantiate the component on.
     * @param type The type to instantiate the component for.
     * @param resource The resource that should be presented in the component.
     * @param isMini Is this a mini presentation (for use in the search results)
     * @param viewedBy Optional indicator of the resource that is viewing this component, for complex resources.
     */
    public createComponent<TComponent extends ResourcePresentationComponent<unknown>>(
        viewContainerRef: ViewContainerRef,
        type: string,
        resource: SearchResult,
        isMini: boolean,
        viewedBy?: string
    ): ComponentRef<TComponent> {
        const componentFactory: ComponentFactory<TComponent> = this.getComponentFactory(
            type
        ) as ComponentFactory<TComponent>
        const componentRef: ComponentRef<TComponent> = viewContainerRef.createComponent(componentFactory)

        componentRef.instance.resource = resource
        componentRef.instance.viewedBy = viewedBy
        componentRef.instance.isMini = isMini

        return componentRef
    }

    /**
     * Gets the component factory based on the given type.
     * If no component type could be found for the given type, an error is thrown.
     * @param type The type to get the component factory for.
     * @private
     */
    private getComponentFactory(type: string): ComponentFactory<ResourcePresentationComponent<unknown>> {
        const componentType: Type<ResourcePresentationComponent<unknown>> =
            this.componentTypeMap.get(type) ?? DefaultMiniPresentationComponent
        return this.componentFactoryResolver.resolveComponentFactory(componentType)
    }
}
