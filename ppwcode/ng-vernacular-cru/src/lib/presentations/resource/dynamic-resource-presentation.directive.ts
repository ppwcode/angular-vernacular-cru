/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Directive, Input, OnInit, ViewContainerRef } from '@angular/core'
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert'

import { DynamicResourcePresentationService } from './dynamic-resource-presentation.service'
import { ResourcePresentationComponent } from './resource-presentation-component'
import { SearchResult } from '../../search/search-results'

@Directive({
    selector: '[ppwcodeCruDynamicResourcePresentation]'
})
export class DynamicResourcePresentationDirective<TResource extends SearchResult>
    implements OnInit, ResourcePresentationComponent<TResource>
{
    @Input() public resource!: TResource
    @Input() public type!: string
    @Input() public isMini: boolean = false
    @Input() public viewedBy?: string | undefined

    constructor(
        private readonly viewContainerRef: ViewContainerRef,
        private readonly dynamicMiniPresentationService: DynamicResourcePresentationService
    ) {}

    public ngOnInit(): void {
        notUndefined(this.resource)
        notUndefined(this.type)

        this.dynamicMiniPresentationService.createComponent(
            this.viewContainerRef,
            this.type,
            this.resource,
            this.isMini,
            this.viewedBy
        )
    }
}
