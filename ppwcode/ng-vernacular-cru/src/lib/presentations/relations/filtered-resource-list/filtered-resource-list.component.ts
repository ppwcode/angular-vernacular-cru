import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { debounceTime } from 'rxjs/operators'
import { FormControl } from '@angular/forms'
import { ResourceNavigationService } from '../../../routing/resource-navigation.service'
import { ResourceListComponent } from '../resource-list/resource-list.component'
import { mixinHandleSubscriptions } from '../../../performance/handle-subscriptions.mixin'
import { SearchResult } from '../../../search/search-results'

const SEARCH_DEBOUNCE_TIME: number = 250

@Component({
    selector: 'ppwcode-cru-filtered-resource-list',
    templateUrl: './filtered-resource-list.component.html',
    styleUrls: [],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilteredResourceListComponent extends mixinHandleSubscriptions(ResourceListComponent) implements OnInit {
    @Input() override resources!: Array<SearchResult>
    @Input() override viewedBy?: string
    @Input() override titleTranslationKey!: string
    @Output() filterChange: EventEmitter<string> = new EventEmitter<string>()

    /** The form control instance used to track the search input by the user. */
    public readonly filterControl: FormControl = new FormControl('')

    constructor(resourceNavigation: ResourceNavigationService) {
        super(resourceNavigation)
    }

    ngOnInit(): void {
        this.stopOnDestroy(this.filterControl.valueChanges)
            .pipe(debounceTime(SEARCH_DEBOUNCE_TIME))
            .subscribe((value: string) => this.filterChange.emit(value))
    }

    /**
     * Resets the filter control value to an empty string.
     */
    public clearFilter(): void {
        if (this.filterControl.value === '') {
            return
        }

        this.filterControl.patchValue('')
    }
}
