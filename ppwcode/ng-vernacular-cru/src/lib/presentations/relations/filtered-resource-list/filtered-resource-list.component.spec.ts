import { createHostFactory, SpectatorHost } from '@ngneat/spectator'
import { HarnessLoader } from '@angular/cdk/testing'
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed'
import { Component } from '@angular/core'
import { Subscription } from 'rxjs'
import { fakeAsync, tick } from '@angular/core/testing'
import { MatLegacyInputHarness as MatInputHarness } from '@angular/material/legacy-input/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { FilteredResourceListComponent } from './filtered-resource-list.component'
import { PpwcodeVernacularCruModule } from '../../../vernacular-cru.module'
import { TranslateModule } from '@ngx-translate/core'

@Component({ template: '' })
class FilteredResourceListHostComponent {}

const filteredResourceListTestSuite = () => {
    let spectator: SpectatorHost<FilteredResourceListComponent, FilteredResourceListComponent>
    let harnessLoader: HarnessLoader
    const createHostComponent = createHostFactory({
        component: FilteredResourceListComponent,
        host: FilteredResourceListHostComponent,
        imports: [PpwcodeVernacularCruModule, RouterTestingModule, TranslateModule.forRoot()]
    })

    describe('filter', () => {
        beforeEach(() => {
            spectator = createHostComponent('<ppwcode-cru-filtered-resource-list></ppwcode-cru-filtered-resource-list>')
            harnessLoader = TestbedHarnessEnvironment.loader(spectator.hostFixture)
        })

        it('should emit after 250ms', fakeAsync(async () => {
            let subscriptionHits: number = 0

            const subscription: Subscription = spectator.component.filterChange.subscribe(() => subscriptionHits++)
            const filterInput = await harnessLoader.getHarness(MatInputHarness)

            await filterInput.setValue('a')
            expect(subscriptionHits).toEqual(0)
            tick(200)
            expect(subscriptionHits).toEqual(0)
            tick(51) // 251ms total
            expect(subscriptionHits).toEqual(1)

            subscription.unsubscribe()
        }))

        it('should clear the input if it is not empty', async () => {
            spyOn(spectator.component.filterControl, 'patchValue').and.callThrough()
            const filterInput = await harnessLoader.getHarness(MatInputHarness)

            await filterInput.setValue('abc')
            expect(spectator.component.filterControl.value).toEqual('abc')

            spectator.component.clearFilter()
            expect(spectator.component.filterControl.value).toEqual('')
            expect(spectator.component.filterControl.patchValue).toHaveBeenCalledTimes(1)

            spectator.component.clearFilter()
            expect(spectator.component.filterControl.value).toEqual('')
            expect(spectator.component.filterControl.patchValue).toHaveBeenCalledTimes(1)

            await filterInput.setValue('defg')
            spectator.component.clearFilter()
            expect(spectator.component.filterControl.patchValue).toHaveBeenCalledTimes(2)
        })
    })
}

describe('FilteredResourceListComponent', () => {
    filteredResourceListTestSuite()
})
