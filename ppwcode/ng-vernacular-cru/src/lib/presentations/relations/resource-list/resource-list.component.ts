import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { SearchResult } from '../../../search/search-results'
import { ResourceNavigationService } from '../../../routing/resource-navigation.service'

@Component({
    selector: 'ppwcode-cru-resource-list',
    templateUrl: './resource-list.component.html',
    styleUrls: [],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResourceListComponent {
    @Input() resources!: Array<SearchResult>
    @Input() viewedBy?: string
    @Input() titleTranslationKey!: string

    constructor(public resourceNavigation: ResourceNavigationService) {}
}
