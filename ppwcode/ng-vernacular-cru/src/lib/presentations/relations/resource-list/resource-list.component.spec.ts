import { createHostFactory, SpectatorHost } from '@ngneat/spectator'
import { Component } from '@angular/core'
import { fakeAsync } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { ResourceListComponent } from './resource-list.component'
import { PpwcodeVernacularCruModule } from '../../../vernacular-cru.module'
import { TranslateModule } from '@ngx-translate/core'

@Component({ template: '' })
class ResourceListHostComponent {}

const resourceListTestSuite = () => {
    let spectator: SpectatorHost<ResourceListComponent, ResourceListComponent>
    const createHostComponent = createHostFactory({
        component: ResourceListComponent,
        host: ResourceListHostComponent,
        imports: [PpwcodeVernacularCruModule, RouterTestingModule, TranslateModule.forRoot()]
    })

    describe('component', () => {
        beforeEach(() => {
            spectator = createHostComponent('<ppwcode-cru-resource-list></ppwcode-cru-resource-list>')
        })

        it('should create', fakeAsync(async () => {
            expect(spectator.component).toBeTruthy()
        }))
    })
}

describe('ResourceListComponent', () => {
    resourceListTestSuite()
})
