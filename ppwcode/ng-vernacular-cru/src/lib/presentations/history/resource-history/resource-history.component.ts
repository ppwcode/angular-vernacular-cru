import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core'

import { HistoricVersion } from '../../../data/CRU/historic-version'
import { PreventNullOrUndefined } from '../../../decorators/prevent-null-or-undefined'
import { whenPropertyChanges } from '../../../utils/ng-on-changes'
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert'

/**
 * This component is responsible for displaying a list of historic versions of a resource.
 * It displays the current timestamp, which is clickable and shows some versions disabled.
 */
@Component({
    selector: 'ppwcode-cru-resource-history',
    templateUrl: './resource-history.component.html',
    styleUrls: ['./resource-history.component.scss']
})
export class ResourceHistoryComponent implements OnChanges {
    /** The list of all historic versions to display. */
    @PreventNullOrUndefined([])
    @Input()
    public history: Array<HistoricVersion> = []

    /** The `createdAt` timestamps of the versions to disable in the menu. */
    @PreventNullOrUndefined([])
    @Input()
    public disabledVersions: Array<string> = []

    /** The current timestamp to display. */
    @PreventNullOrUndefined('')
    @Input()
    public currentResourceTimestamp: string = ''

    /** Event that emits when a version is selected in the menu. */
    @Output() public readonly versionSelected: EventEmitter<HistoricVersion> = new EventEmitter<HistoricVersion>()

    /** Version timestamp that is shown in the UI, as trigger for the menu. */
    public displayedVersion: string = ''

    /** A map of historic versions that should be disabled in the UI. Usually this is the currently displayed version. */
    public disabledVersionsMap: Map<string, HistoricVersion> = new Map<string, HistoricVersion>()

    public ngOnChanges(changes: SimpleChanges): void {
        whenPropertyChanges<ResourceHistoryComponent>(changes, {
            history: () => {
                this.setDisplayedVersion()
                this.calculateDisabledVersions()
            },
            disabledVersions: () => this.calculateDisabledVersions(),
            currentResourceTimestamp: () => {
                this.setDisplayedVersion()
                this.calculateDisabledVersions()
            }
        })
    }

    /**
     * Updates the disabled version map.
     *
     * All versions in the disabledVersion array are marked as disabled, as well as the version most closely matching the currentResourceTimestamp
     *
     * It is possible that the currentResourceTimestamp does *not* exactly match any of the historic versions
     * e.g. someone requested a knowledge time that is in between two versions.
     *
     * To calculate the correct version to disable, we will need to interpret the dates, and find the date that is either
     *      a. an exact match for the currentResourceTimestamp
     * or, if such a date can't be found
     *      b. the most recent historic version *before* the currentResourceTimestamp
     */
    public calculateDisabledVersions() {
        // Reset the disabledVersionsMap to  all explicitly disabled versions
        const explicitlyDisabled = this.history.filter(
            (hv: HistoricVersion) => this.disabledVersions.indexOf(hv.createdAt) > -1
        )
        this.disabledVersionsMap = new Map(explicitlyDisabled.map((hv) => [hv.createdAt, hv]))

        // Check if we have a currentResourceTimestamp
        if (this.currentResourceTimestamp) {
            // YES,
            // Find exact match
            const exact = this.history.find((hv: HistoricVersion) => hv.createdAt === this.currentResourceTimestamp)
            if (exact) {
                this.disabledVersionsMap.set(exact.createdAt, exact)
            } else {
                // If no exact match is found, find most recent historic version before the currentResourceTimestamp
                const beforeCurrent: Array<HistoricVersion> = []
                this.history.forEach((hv: HistoricVersion) => {
                    if (new Date(hv.createdAt) < new Date(this.currentResourceTimestamp)) {
                        beforeCurrent.push(hv)
                    }
                })

                if (beforeCurrent.length) {
                    // Sort by createdAt date, ascending
                    if (beforeCurrent.length > 1) {
                        beforeCurrent.sort(this.sortByDateAscending)
                    }

                    // Last entry is the closest match to the currentResourceTimestamp
                    const closestMatch = notUndefined(beforeCurrent.pop())
                    this.disabledVersionsMap.set(closestMatch.createdAt, closestMatch)
                }
            }
        } else {
            // NO,
            // By convention we are then displaying the most recent version
            const mostRecent = this.getMostRecentHistoricVersion()

            if (mostRecent) {
                this.disabledVersionsMap.set(mostRecent.createdAt, mostRecent)
            }
        }
    }

    private setDisplayedVersion(): void {
        this.displayedVersion =
            this.currentResourceTimestamp && this.currentResourceTimestamp !== ''
                ? this.currentResourceTimestamp
                : this.getMostRecentHistoricVersion()?.createdAt ?? ''
    }

    private getMostRecentHistoricVersion(): HistoricVersion | undefined {
        return this.history.length ? [...this.history].sort(this.sortByDateAscending).pop() : undefined
    }

    private sortByDateAscending(a: HistoricVersion, b: HistoricVersion): number {
        return new Date(a.createdAt) < new Date(b.createdAt) ? -1 : 1
    }
}
