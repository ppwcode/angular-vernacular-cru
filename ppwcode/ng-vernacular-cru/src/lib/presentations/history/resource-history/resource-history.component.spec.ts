import { ResourceHistoryComponent } from './resource-history.component'
import { createComponentFactory, Spectator } from '@ngneat/spectator'
import { MatLegacyMenuModule as MatMenuModule } from '@angular/material/legacy-menu'

describe('ResourceHistoryComponent', () => {
    let spectator: Spectator<ResourceHistoryComponent>
    const createComponent = createComponentFactory({
        component: ResourceHistoryComponent,
        imports: [MatMenuModule]
    })
    let now: Date = new Date()
    const historyEntry = { createdAt: now.toISOString(), href: 'hrefvalue' }

    beforeEach(() => {
        spectator = createComponent({
            props: {
                disabledVersions: [now.toISOString()],
                currentResourceTimestamp: now.toISOString(),
                history: [historyEntry]
            }
        })
    })

    it('should have set the disabled versions map', () => {
        expect(spectator.component.disabledVersionsMap.has(historyEntry.createdAt)).toEqual(true)
    })

    it('should update the disabled versions map when the input bindings change', () => {
        const yesterday = new Date()
        yesterday.setDate(yesterday.getDate() - 1)

        spyOn(spectator.component, 'calculateDisabledVersions').and.callThrough()

        spectator.setInput({ disabledVersions: [] })
        expect(spectator.component.calculateDisabledVersions).toHaveBeenCalledTimes(1)
        expect(spectator.component.disabledVersionsMap.size).toEqual(1)

        spectator.setInput({
            history: [
                historyEntry,
                { createdAt: now.toISOString(), href: 'hrefvalue' },
                { createdAt: yesterday.toISOString(), href: 'hrefvalue' }
            ]
        })
        expect(spectator.component.calculateDisabledVersions).toHaveBeenCalledTimes(2)
        expect(spectator.component.disabledVersionsMap.size).toEqual(1)
        expect(spectator.component.displayedVersion).toEqual(historyEntry.createdAt)

        spectator.setInput({ disabledVersions: [yesterday.toISOString()] })
        expect(spectator.component.calculateDisabledVersions).toHaveBeenCalledTimes(3)
        expect(spectator.component.disabledVersionsMap.size).toEqual(2)

        const tomorrow = new Date()
        tomorrow.setDate(tomorrow.getDate() + 1)
        spectator.setInput({
            currentResourceTimestamp: tomorrow.toISOString()
        })
        expect(spectator.component.calculateDisabledVersions).toHaveBeenCalledTimes(4)
        expect(spectator.component.disabledVersionsMap.size).toEqual(2)
        expect(spectator.component.displayedVersion).toEqual(tomorrow.toISOString())

        spectator.setInput({
            currentResourceTimestamp: undefined
        })
        expect(spectator.component.calculateDisabledVersions).toHaveBeenCalledTimes(5)
        expect(spectator.component.disabledVersionsMap.size).toEqual(2)
        expect(spectator.component.displayedVersion).toEqual(historyEntry.createdAt)

        spectator.setInput({
            history: []
        })
        expect(spectator.component.displayedVersion).toEqual('')
    })
})
