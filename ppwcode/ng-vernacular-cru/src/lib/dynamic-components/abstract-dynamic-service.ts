import { Type } from '@angular/core'

/**
 * This abstract class holds a base implementation for a service that is used to track component types
 * for a set of types. It contains a map that holds the component type related to the type and functions to
 * register new or overwrite existing type maps.
 */
export class AbstractDynamicService<TComponent> {
    /** Map that keeps track of the components for each type. */
    public componentTypeMap: Map<string, Type<TComponent>> = new Map<string, Type<TComponent>>()

    /**
     * Register a certain component to be instantiated for a given type.
     * Doesn't check for existing types so calling it a second time for the same type
     * will overwrite the first registration.
     */
    public registerComponent(type: string, component: Type<TComponent>): void {
        this.componentTypeMap.set(type, component)
    }

    /**
     * Registers a set of components to be instantiated for the given types.
     * Doesn't check for existing types so calling it a second time for the same type
     * will overwrite the first registration.
     */
    public registerComponents(typesWithComponents: Array<[string, Type<TComponent>]>): void {
        for (const [type, component] of typesWithComponents) {
            this.registerComponent(type, component)
        }
    }
}
