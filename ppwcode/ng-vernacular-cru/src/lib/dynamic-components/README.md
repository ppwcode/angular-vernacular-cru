# Angular Vernacular CRU Dynamic Components

This section of the library focuses on shared logic for dynamic components.

## `AbstractDynamicService<TComponent>`

The standardized implementation of `AbstractDynamicService` gets you up and running quickly for creating your own
services that'll handle dynamic creation of components based on a type.

It holds a base implementation for a service that is used to track component types for a set of types. It contains a map
that holds the component type related to the type and methods to register new or overwrite existing type maps.

The generic `TComponent` allows you to have only component types registered that have a certain interface implemented.

```typescript
@Injectable({
    providedIn: 'root'
})
export class DynamicControlService extends AbstractDynamicService<FormControlComponent> {
    // ... logic to create a component instance for a control based on its type ...
}
```

> NOTE
>
> It could be useful to have the logic for instantiating a component in the abstract implementation as well.
