# Control Groups

Control groups are just a set of controls that belong to each other visually. You can use them to group them together
vertically or horizontally.

## `VerticalControls`

Group controls together vertically.

## `HorizontalControls`

Group controls together horizontally.

## Visibility conditions

It is possible to show or hide a full group of controls based on a condition. The boolean returned by the condition
function specifies whether the group should be visible. Returning `true` thus means that the group should be visible.

## Examples

```typescript
import { VerticalControls } from './vertical-controls'

const allwaysHidden: VerticalControls = new VerticalControls(
    [
        // ... control definitions ...
    ],
    (_: MyFormValue) => false
)

const hiddenWhenCheckboxIsUnchecked: VerticalControls = new VerticalControls(
    [
        // ... control definitions ...
    ],
    (formValue: MyFormValue) => formValue.includeAddress
)

const hiddenWhenSmallGroup: VerticalControls = new VerticalControls(
    [
        // ... control definitions ...
    ],
    (formValue: MyFormValue) => formValue.participantsCount < 10
)
```
