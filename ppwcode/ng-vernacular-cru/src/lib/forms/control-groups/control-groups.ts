/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { FormControlConfig } from '../controls/form-control-config'

export type ControlGroupDirection = 'row' | 'column'

/**
 * Represents a group of control configurations to be shown close to each other.
 */
export abstract class AbstractControlGroup {
    public abstract readonly direction: ControlGroupDirection

    constructor(
        /**
         * The controls to be shown in the group.
         */
        public controls: Array<FormControlConfig>,
        /**
         * An optional condition to check whether the control group should be visible.
         * All controls are visible when unspecified.
         */
        public visibilityCondition?: (formValue: Record<string, unknown>) => boolean
    ) {}
}
