# Angular Vernacular CRU Forms

This section of the library is focused on a standardized implementation of forms. It gets you up and running quickly for
creating forms for your templates and keeps the templates clean.

-   [Control Groups](./control-groups/README.md)
-   [Controls](./controls/README.md)
-   [Dynamic Form Generation](./dynamic-form/README.md)
