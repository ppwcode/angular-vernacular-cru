import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    DoCheck,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output
} from '@angular/core'
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn } from '@angular/forms'
import { TranslateService } from '@ngx-translate/core'
import { notNull, notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert'
import * as Joi from 'joi'
import { Subscription } from 'rxjs'

import { PreventNullOrUndefined } from '../../decorators/prevent-null-or-undefined'
import { AbstractControlGroup } from '../control-groups/control-groups'
import { FormControlConfig } from '../controls/form-control-config'
import { ValueChangeEvent } from './value-change-event'
import { getPropertyValue } from '../../utils/objects'
import { createJoiValidator } from '../../utils/validators'

@Component({
    selector: 'ppwcode-cru-dynamic-form',
    templateUrl: './dynamic-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['../../tailwind.scss']
})
export class DynamicFormComponent implements OnInit, OnDestroy, DoCheck, AfterViewInit {
    /**
     * The groups of controls to display in the form.
     */
    @PreventNullOrUndefined([])
    @Input()
    public controlGroups: Array<AbstractControlGroup> = []

    /**
     * The initial value to apply to the form.
     */
    @PreventNullOrUndefined({ test: 123 })
    @Input()
    public initialValue: Record<string, unknown> = {}

    /**
     * The validators for the fields to be used.
     */
    @PreventNullOrUndefined({})
    @Input()
    public validators: Record<string, Array<ValidatorFn> | Joi.Schema> | Joi.ObjectSchema = {}

    /**
     * Whether the form should be disabled.
     * Very useful in read-only modes.
     */
    @PreventNullOrUndefined(false)
    @Input()
    public disabled: boolean | null = false

    /** Emits whenever the value of the form changes. */
    @Output() public readonly valueChange: EventEmitter<ValueChangeEvent> = new EventEmitter<ValueChangeEvent>()

    /** The local form group instance to track the form value. */
    public formGroup!: FormGroup

    /** Gets whether the current form values are different from the initial form values. */
    public get hasChanges(): boolean {
        return this.initialStringifiedValue !== this.currentStringifiedValue
    }

    /**
     * Gets the given validators as a Joi ObjectSchema or null if it's not a Joi schema.
     * @private
     */
    private get validatorsAsJoiSchema(): Joi.ObjectSchema | null {
        return Joi.isSchema(this.validators) ? (this.validators as Joi.ObjectSchema) : null
    }

    /** Subscription to the value changes event of the form group. */
    private formGroupChangesSubscription?: Subscription

    /** Cache of a stringified representation of the initial form value. */
    private initialStringifiedValue!: string

    /** Cache of a stringified representation of the current form value. */
    private currentStringifiedValue!: string

    constructor(private readonly formBuilder: FormBuilder, private readonly translateService: TranslateService) {}

    public ngOnInit(): void {
        this.createForm()

        this.cacheCurrentValuesAsInitialValues()
    }

    public ngAfterViewInit(): void {
        this.formGroupChangesSubscription = this.formGroup.valueChanges.subscribe(() => {
            const fullValue: Record<string, unknown> = this.formGroup.getRawValue()
            this.currentStringifiedValue = JSON.stringify(fullValue)
            this.valueChange.emit({ fullValue })
        })
    }

    public ngOnDestroy(): void {
        // This will probably never be undefined.
        /* istanbul ignore next */
        this.formGroupChangesSubscription?.unsubscribe()
    }

    public ngDoCheck(): void {
        if (!this.formGroup.disabled) {
            // Runs over all controls to check whether they should be enabled or disabled.
            this.controlGroups.forEach((controlGroup: AbstractControlGroup) => {
                controlGroup.controls.forEach((control: FormControlConfig) => {
                    const formControlInstance: AbstractControl = notNull(this.formGroup.get(control.name))
                    const shouldBeDisabled: boolean = this.isControlDisabled(control)

                    if (shouldBeDisabled && formControlInstance.enabled) {
                        formControlInstance.disable()
                    } else if (!shouldBeDisabled && formControlInstance.disabled) {
                        formControlInstance.enable()
                    }
                })
            })
        }
    }

    /**
     * Initialises the form based on the provided control configurations and initial values.
     * Sets up listening to the values of other controls if there are dependencies between controls.
     */
    public createForm(): void {
        this.formGroup = this.controlGroups
            .reduce(
                (allControls: Array<FormControlConfig>, group: AbstractControlGroup) =>
                    allControls.concat(group.controls),
                []
            )
            .reduce((formGroupConfig: FormGroup, control: FormControlConfig) => {
                const splitControlName: Array<string> = control.name.split('.')
                if (splitControlName.length === 1) {
                    // No dots used in the name of the control.
                    formGroupConfig.addControl(
                        control.name,
                        this.formBuilder.control(
                            this.getControlInitialValue(control),
                            this.getControlValidators(control)
                        )
                    )
                    return formGroupConfig
                } else {
                    // Dots used in the name of the control. The config for the control should be nested.
                    let currentConfigForSubKey: FormGroup | undefined = formGroupConfig
                    for (let i = 0; i < splitControlName.length; i++) {
                        const currentSubKey: string = notUndefined(splitControlName[i])

                        if (i === splitControlName.length - 1) {
                            // We are at the last part of the control name, so this should be a value assignment.
                            notUndefined(currentConfigForSubKey as FormGroup).addControl(
                                currentSubKey,
                                this.formBuilder.control(
                                    this.getControlInitialValue(control),
                                    this.getControlValidators(control)
                                )
                            )
                        } else {
                            // There are still parts of the control name to follow. We are constructing a form group here:
                            // {
                            //      A: FormGroup {
                            //          B: FormGroup {
                            //              C: FormControl ['', [Validators.required]] <-- Handled in the if-branch
                            //          }
                            //      }
                            // }
                            currentConfigForSubKey = notUndefined(currentConfigForSubKey as FormGroup).get(
                                currentSubKey
                            ) as FormGroup | undefined
                            if (!currentConfigForSubKey && i !== splitControlName.length - 1) {
                                const newFormGroup: FormGroup = new FormGroup({})
                                formGroupConfig.addControl(currentSubKey, newFormGroup)
                                currentConfigForSubKey = newFormGroup
                            }
                        }
                    }
                    return formGroupConfig
                }
            }, new FormGroup({}))

        if (notNull(this.disabled)) {
            this.formGroup.disable()
        }
    }

    /**
     * Takes the raw value of the form and caches it as the initial stringified value and current stringified value.
     */
    public cacheCurrentValuesAsInitialValues(): void {
        this.initialStringifiedValue = JSON.stringify(this.formGroup.getRawValue())
        this.currentStringifiedValue = this.initialStringifiedValue
    }

    /**
     * Gets the initial value for the control based on its name.
     * @param controlConfig The full control configuration.
     */
    public getControlInitialValue(controlConfig: FormControlConfig): unknown {
        return getPropertyValue(this.initialValue, controlConfig.name)
    }

    /**
     * Gets the validators for a form control. Extracts them from the given Joi schema or the validators object.
     * Returns `undefined` if the form control name is not part of the schema or validators object.
     * @param controlConfig The configuration of the form control.
     */
    public getControlValidators(controlConfig: FormControlConfig): ValidatorFn | Array<ValidatorFn> | undefined {
        const validationJoiSchema: Joi.ObjectSchema | null = this.validatorsAsJoiSchema
        if (validationJoiSchema) {
            try {
                // It is possible that controls are added that are not a part of the Joi schema.
                // The `.extract` method will throw in such case, hence the try-catch here.
                const propertySchema: Joi.Schema = validationJoiSchema.extract(controlConfig.name)

                return createJoiValidator(propertySchema, {
                    translateService: this.translateService,
                    prefix: 'joi-errors.',
                    customValidationMessages: controlConfig.options?.validationMessages
                })
            } catch {
                return undefined
            }
        }

        const fromValidators: Array<ValidatorFn> | Joi.Schema | undefined = (
            this.validators as Record<string, Array<ValidatorFn> | Joi.Schema>
        )[controlConfig.name]
        return Joi.isSchema(fromValidators)
            ? createJoiValidator(fromValidators as Joi.Schema, {
                  translateService: this.translateService,
                  prefix: 'joi-errors.',
                  customValidationMessages: controlConfig.options?.validationMessages
              })
            : (fromValidators as Array<ValidatorFn> | undefined)
    }

    /**
     * Checks whether the given group of controls should be visible.
     * Falls back to true if no condition has been set on the group.
     * @param group The control group to check the visibility for.
     */
    public isGroupVisible(group: AbstractControlGroup): boolean {
        return group.visibilityCondition ? group.visibilityCondition(this.formGroup.value) : true
    }

    /**
     * Checks whether the given control is disabled.
     * Executes the isDisabled function on the configuration or returns false if no function is available.
     * @param controlConfig The control config to check the disabled condition for.
     */
    public isControlDisabled(controlConfig: FormControlConfig): boolean {
        return controlConfig.options?.isDisabled?.(this.formGroup.value) ?? false
    }

    /**
     * Resets the form to the given values and marks it as pristine and untouched.
     * @param value The value to reset the form to.
     */
    public reset(value: Record<string, unknown>): void {
        this.formGroup.patchValue(value)
        this.formGroup.markAsPristine()
        this.formGroup.markAsUntouched()
    }
}
