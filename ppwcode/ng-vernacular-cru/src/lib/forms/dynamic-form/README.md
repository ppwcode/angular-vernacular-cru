# Angular Vernacular CRU Dynamic Form

This section of the library is focused on the dynamic form generation part.

## `DynamicFormComponent`

A dynamic form is one of the most powerful elements to have in an application. This library allows you to generate a
full form based on configuration that works and validates on its own. The only thing you need to do is tell the
component what controls it needs to render and what validators to apply. In case of an update scenario you can also pass
the resource to get the update values from.

Internally, the component will generate a `FormGroup` instance based on the given control definitions and validators.
That `FormGroup` is then used like in a normal Angular application to track value changes made to the control and update
the form state based on it.

There is no saving functionality implemented in the library. This means that the dynamic form component will only do the
form part. Any save, cancel or reset buttons, you need to implement yourself. However, you can have a look at the state
of the `FormGroup` inside the dynamic form component to have the buttons available or not.

### Change tracking

The dynamic form component handles change tracking by itself. Every time a value changes, this is emitted using the
`(valueChange)` output emitter. The value emitted through this event is a `ValueChangeEvent`, containing the full form
value.

> NOTE
>
> As there was no need yet to know the control of which the value has changed, this has not been implemented. The
> `ValueChangeEvent` has been set up in such way that it can be easily extended without causing breaking changes.

You can get whether the value of the form differs from its initial value by getting the `hasChanges` value. It's also
possible to reset the form to a certain value and mark it as pristine and untouched again.

In most cases this will be the initial value of the form.

```typescript
export class MyComponent {
    @ViewChild(DynamicFormComponent)
    public dynamicFormComponent!: DynamicFormComponent

    public onClickResetButton(): void {
        this.dynamicFormComponent.reset(this.selectedPerson)
    }
}
```

### Example

Using the `[controlGroups]` input binding, you can pass an array of control group definitions to the component.

```typescript
export class MyComponent {
    public personControls: Array<AbstractControlGroup> = [
        new VerticalControls([
            new TextControl('inss', { label: 'INSS' }),
            new TextControl('dateOfBirth', {
                label: 'Date of Birth',
                inputType: 'date'
            }),
            new TextControl('pronouns', { label: 'Pronouns' }),
            new TextControl('language', { label: 'Language' })
        ])
    ]
}
```

```html
<ppwcode-cru-dynamic-form
    [controlGroups]="controlGroups"
    [initialValue]="selectedPerson"
    [validators]="validators"></ppwcode-cru-dynamic-form>
```

Some magic is used to generate a `FormGroup` based on these control definitions.

First, the name of the control in the `FormGroup` will be the name given in the control definition ('inss',
'dateOfBirth', ...). You can use a dot-notation to generate `FormGroup` instances within a `FormGroup`.

`'address.city.postalCode'` will result in a main `FormGroup` for the form, then a `FormGroup` for `address`, within the
`FormGroup` for `address` will be a `FormGroup` for
`city and finally within the `FormGroup`for`city`there will be a`FormControl`for`postalCode`. This will result in a form
value like this:

```json
{
    "address": {
        "city": {
            "postalCode": 2600
        }
    }
}
```

#### Validation

If you want to pass a validator for this, simply use the same dot-notation as the property name of the validators
object:

```typescript
export class MyComponent {
    public validators: Record<string, Array<ValidatorFn>> = {
        'address.city.postalCode': [Validators.required]
    }
}
```

You can also add a Joi schema for validation:

```typescript
export class MyComponent {
    public validators: Joi.ObjectSchema = Joi.object({
        address: Joi.object({
            city: Joi.required()
        })
    })
}
```

It's also possible to combine both a Joi schema and pure Angular validators:

```typescript
export class MyComponent {
    public validators: Record<string, Array<ValidatorFn> | Joi.Schema> = {
        'address.city.postalCode': [Validators.required],
        name: Joi.string().required()
    }
}
```

#### Validation error messages

The library doesn't ship with a default set of validation messages (yet?). Because the library depends on
`@ngx-translate-core` you can easily add the error translations you need by adding a `'joi-errors'` property in your
translation files:

```json
{
    "joi-errors": {
        "string.pattern.base": "This field doesn't match the pattern {{ regex }}"
    }
}
```

If you want to derive from these default messages for specific controls, you can specify this in the options of the
control definition:

```typescript
export class MyComponent {
    public personControls: Array<AbstractControlGroup> = [
        new VerticalControls([
            new TextControl('inss', { label: 'INSS' }),
            new TextControl('dateOfBirth', {
                label: 'Date of Birth',
                inputType: 'date'
            }),
            new TextControl('pronouns', { label: 'Pronouns' }),
            new TextControl('language', {
                label: 'Language',
                customValidationMessages: {
                    'string.pattern.base': 'person.language.pattern-error'
                }
            })
        ])
    ]
}
```

```json
{
    "joi-errors": {
        "string.pattern.base": "This field doesn't match the pattern {{ regex }}"
    },
    "person": {
        "language": {
            "pattern-error": "The language must be two characters in lowercase."
        }
    }
}
```

#### Initial value

The initial value for this kind of controls can just be passed as if it were the value of the `FormGroup`. So there's no
need to flatten these out like the validators.

```typescript
export class MyComponent {
    public selectedPerson: Person = {
        address: {
            city: {
                postalCode: 2000
            }
        }
    }
}
```
