import { Spectator, createComponentFactory } from '@ngneat/spectator'
import { TranslateModule } from '@ngx-translate/core'

import { PpwcodeVernacularFormsModule } from '../ppwcode-vernacular-forms.module'
import { VerticalControls } from '../control-groups/vertical-controls'
import { TextControl } from '../controls/text-control/text-control'
import { DynamicFormComponent } from './dynamic-form.component'
import { testingPersons } from '../../testing/testing-person'
import { AbstractControlGroup } from '../control-groups/control-groups'
import { Subscription } from 'rxjs'
import { ValueChangeEvent } from './value-change-event'
import { TextControlOptions } from '../controls/text-control/text-options'
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert'
import Joi from 'joi'
import { FormGroup, ValidatorFn, Validators } from '@angular/forms'

describe('DynamicFormComponent', () => {
    let spectator: Spectator<DynamicFormComponent>
    let component: DynamicFormComponent
    let controlGroups: Array<AbstractControlGroup> = [
        new VerticalControls([
            new TextControl('firstName', { validationMessages: {} }),
            new TextControl('lastName'),
            new TextControl('middleName'),
            new TextControl('email', { isDisabled: () => true })
        ])
    ]
    const testPersonSchema: Joi.ObjectSchema = Joi.object({
        firstName: Joi.string().min(3).required(),
        lastName: Joi.string().min(3).required(),
        middleName: Joi.string().min(1).required()
    })
    const testPersonValidators: Record<string, Array<ValidatorFn>> = {
        firstName: [Validators.required, Validators.minLength(3)],
        lastName: [Validators.required, Validators.minLength(3)],
        middleName: [Validators.required, Validators.minLength(1)]
    }
    const testPersonValidatorsMix: Record<string, Array<ValidatorFn> | Joi.Schema> = {
        firstName: Joi.string().min(3).required(),
        lastName: [Validators.required],
        middleName: Joi.string().min(1).required()
    }

    const createComponent = createComponentFactory({
        component: DynamicFormComponent,
        imports: [PpwcodeVernacularFormsModule, TranslateModule.forRoot()]
    })

    describe('validation', () => {
        ;[testPersonSchema, testPersonValidators, testPersonValidatorsMix].forEach(
            (validatorsForTest: Joi.ObjectSchema | Record<string, Array<ValidatorFn> | Joi.Schema>) => {
                beforeEach(() => {
                    spectator = createComponent({
                        props: {
                            controlGroups,
                            initialValue: testingPersons.johnDoe,
                            validators: validatorsForTest
                        }
                    })
                })

                it('should validate', () => {
                    const formGroup: FormGroup = spectator.component.formGroup
                    const { firstName, lastName } = formGroup.controls
                    expect(formGroup.valid).toBeFalse()
                    expect(notUndefined(firstName).valid).toBeTrue()
                    expect(notUndefined(lastName).valid).toBeFalse()

                    formGroup.patchValue({ firstName: '' })
                    spectator.detectChanges()

                    expect(notUndefined(firstName).valid).toBeFalse()
                    expect(notUndefined(lastName).valid).toBeFalse()

                    formGroup.patchValue({ lastName: '' })

                    expect(formGroup.valid).toBeFalse()
                    expect(notUndefined(firstName).valid).toBeFalse()
                    expect(notUndefined(lastName).valid).toBeFalse()

                    formGroup.patchValue({
                        ...testingPersons.johnDoe,
                        lastName: 'Doe',
                        middleName: 'H'
                    })

                    expect(formGroup.valid).toBeTrue()
                    expect(notUndefined(firstName).valid).toBeTrue()
                    expect(notUndefined(lastName).valid).toBeTrue()
                })
            }
        )
    })

    describe('with nested control names', () => {
        beforeEach(() => {
            spectator = createComponent({
                props: {
                    controlGroups: [
                        new VerticalControls([
                            new TextControl('firstName', {}),
                            new TextControl('lastName', {}),
                            new TextControl('contact.email', {
                                isDisabled: () => true
                            }),
                            new TextControl('contact.phone', {
                                isDisabled: () => true
                            })
                        ])
                    ],
                    initialValue: {
                        firstName: testingPersons.johnDoe.firstName,
                        lastName: null,
                        contact: {
                            email: 'john@acme.com',
                            phone: '012/34.56.78'
                        }
                    }
                }
            })
            component = spectator.component
        })

        it('should create', () => {
            expect(component).toBeTruthy()
            expect(Object.keys(component.formGroup.controls).length).toEqual(3)
            expect(component.formGroup.getRawValue()).toEqual({
                firstName: testingPersons.johnDoe.firstName,
                lastName: null,
                contact: {
                    email: 'john@acme.com',
                    phone: '012/34.56.78'
                }
            })
            expect(component.hasChanges).toBe(false)
        })
    })

    describe('for all types of forms', () => {
        beforeEach(() => {
            spectator = createComponent({
                props: {
                    controlGroups,
                    initialValue: testingPersons.johnDoe
                }
            })
            component = spectator.component
        })

        it('should create', () => {
            const initialValue: Record<string, string | null> = {
                firstName: testingPersons.johnDoe.firstName,
                lastName: null,
                middleName: null,
                email: null
            }

            expect(component).toBeTruthy()
            expect(Object.keys(component.formGroup.controls).length).toEqual(4)
            expect(component.formGroup.getRawValue()).toEqual(initialValue)
            expect(component.hasChanges).toBe(false)
        })

        it('should emit when the value changes', (done: DoneFn) => {
            const subscription: Subscription = component.valueChange.subscribe((valueChangeEvent: ValueChangeEvent) => {
                expect(valueChangeEvent.fullValue).toEqual({
                    firstName: 'Jane',
                    lastName: null,
                    middleName: null,
                    email: null
                })
                subscription.unsubscribe()
                done()
            })

            component.formGroup.patchValue({ firstName: 'Jane' })
        })

        it('should get whether a control group is visible', () => {
            const noConditionGroup = new VerticalControls([])
            const invisibleConditionGroup = new VerticalControls([], () => false)

            expect(component.isGroupVisible(noConditionGroup)).toBe(true)
            expect(component.isGroupVisible(invisibleConditionGroup)).toBe(false)
        })

        it('should get whether a control is disabled', () => {
            // Force because it's nullable in the components itself.
            const noOptionsControl = new TextControl('noOptions', null as unknown as TextControlOptions)
            const noConditionControl = new TextControl('noCondition', {})
            const disabledControl = new TextControl('disabled', {
                isDisabled: () => true
            })

            expect(component.isControlDisabled(noOptionsControl)).toBe(false)
            expect(component.isControlDisabled(noConditionControl)).toBe(false)
            expect(component.isControlDisabled(disabledControl)).toBe(true)
        })

        it('should reset the form', () => {
            component.formGroup.patchValue({ firstName: 'Jane' })
            expect(component.hasChanges).toBe(true)

            component.reset(testingPersons.johnDoe)
            expect(component.hasChanges).toBe(false)
            expect(component.formGroup.pristine).toBe(true)
            expect(component.formGroup.untouched).toBe(true)
        })

        it('should enable a form control again when necessary', () => {
            notUndefined(component.formGroup.controls.firstName).disable()
            expect(notUndefined(component.formGroup.controls.firstName).disabled).toBe(true)

            spectator.detectChanges()

            expect(notUndefined(component.formGroup.controls.firstName).disabled).toBe(false)
        })
    })

    describe('specific for readonly forms', () => {
        beforeEach(() => {
            spectator = createComponent({
                props: {
                    controlGroups,
                    initialValue: testingPersons.johnDoe,
                    disabled: true
                }
            })
            component = spectator.component
        })

        it('should generate a disabled form', () => {
            expect(component.formGroup.disabled).toBe(true)
        })

        it('should not enable the form controls again when the form group is disabled', () => {
            // Detecting changes will trigger the ngDoCheck hook.
            spectator.detectChanges()

            expect(component.formGroup.disabled).toBe(true)
        })
    })
})
