# Angular Vernacular CRU Form Controls

This section of the library is focused on a standardized implementation of form controls and dynamic generation of form
control instances based on a type.

## Mixins

Several mixins have been made available to get you started quickly on developing your custom `ControlValueAccessor`
components.

-   `mixinCanBeDisabled`
    -   Extends an optional given base class with the necessary things to disable the control.
-   `mixinOnChange`
    -   Extends an optional given base class with the necessary things to propagate changes from the control to the view
        model.
-   `mixinOnTouched`
    -   Extends an optional given base class with the necessary things to propagate that the control has been touched.

These mixins on their turn are combined into a single `mixinControlValueAccessor` that you can easily use to get
started. The only thing you still need to do is implement the `writeValue` method enforced by the `ControlValueAccessor`
interface.

The final mixin (`mixinFormControlComponent`) is the recommended mixin because it enriches your control component with a
local `FormControl` instance and the `writeValue` method implemented. Both are necessary to let the dynamic control
logic instantiate your control component.

```typescript
// ... component metadata ...
export class TextControlComponent extends mixinFormControlComponent() {
    @Input() public options?: TextControlOptions

    constructor(private readonly changeDetectorRef: ChangeDetectorRef) {
        super()
    }

    public ngOnInit(): void {
        // ... access to `.isDisabled`, `.onTouched()`, `.onChange(value)`, ...
    }
}
```

## Testing

The following test suites are made available for you to use:

-   `canBeDisabledSuite`
-   `onChangeSuite`
-   `onTouchedSuite`

```typescript
describe('MyFormControlComponent', () => {
    const getInstance = () => new MyFormControlComponent()

    canBeDisabledSuite(getInstance)
    onChangeSuite(getInstance)
    onTouchedSuite(getInstance)
})
```

As you can tell from the example, all this behavior is verified without you having to think about implementing the tests
explicitly.

## How to develop your own control component

The following steps guide you through the process of developing your own control component and being able to let it be
dynamically generated.

1. Generate a component using the CLI.
2. Make the component extend `mixinFormControlComponent()`.
3. Register the component with its related type in the `DynamicControlService`
    ```typescript
    dynamicControlService.registerComponent('myFormControl', MyFormControlComponent)
    ```
4. Create an easy-to-use class to generate the definition.

    ```typescript
    export class MyFormControl implements FormControlConfig {
        /** The type of the form control. */
        public readonly type: string = 'myFormControl' // This should match the type used in step 3.

        constructor(
            /** The unique name of the control within the form. */
            public name: string,
            /** The options to be passed to the form control. */
            public options: MyFormControlOptions
        ) {}
    }
    ```

5. Create an instance of the class in the form where you want to use the control.
    ```typescript
    const controls: VerticalControls = new VerticalControls([
        // ... other controls ...
        new MyFormControl('awesomeValueProperty')
    ])
    ```

## Override default control components

The library ships with some default implementations for certain control component types:

-   'text' -> `TextControlComponent`

It is possible to override this, simply by calling the `DynamicControlService.registerComponent` method.

> This behavior is intended. In the future, the list of default implementations will be longer. In our vernacular, we
> have chosen for Angular Material as our UI library but depending on the project another library can be used. The
> default implementations that we'll ship will be focused on Angular Material but feel free to override all of them with
> your own as this will not break the library.

## `DynamicControlDirective`

This directive is the one that is capable of dynamically generating a specific component instance based on the `config`
input it receives. It has a look at the `type` property on the config and delegates the component instantiation to the
`DynamicControlService` which has a `Map` that holds the component type for every known component type.

The directive has an abstraction of a `ControlValueAccessor` implementation and just passes everything it receives to
the generated control component instance. The directive just acts as a man-in-the-middle. This approach is necessary to
be able to use `[formControl]` and `formControlName` on elements where the controls are instantiated by the directive.

> NOTE
>
> There is no known better way of doing this.

The directive adds an attribute `data-control-name` with the name of the control as value so that the control can be
interacted with in tests. Furthermore, as we rely on Tailwind, the `flex` and `flex-col` classes are added to the host
element so that the controls are rendered nicely in a form with multiple controls.

## `DynamicControlService`

Use this service to register control components with its related type. There's also a public method available to
instantiate a component based on a type and config, but this is a low-level API that you'll probably never use.

If you are going to instantiate a component on your own, then the recommendation is to use the
`DynamicControlDirective`.

```html
<ng-container [formGroup]="formGroup">
    <ng-container ppwcodeCruDynamicControl [config]="controlConfig" formControlName="myAwesomeProperty"></ng-container>
</ng-container>
```
