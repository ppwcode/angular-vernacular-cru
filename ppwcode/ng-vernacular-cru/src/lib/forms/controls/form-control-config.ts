/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * Configuration for a form control.
 * The generic type TOptions allows for type safety on the options property.
 */
export interface FormControlConfig<TOptions extends FormControlOptions = FormControlOptions> {
    /** The type of the form control. */
    type: string
    /** The unique name of the control within the form. */
    name: string
    /** The options to be passed to the form control. */
    options?: TOptions
}

/**
 * The default options that should be applied to all form control.
 */
export interface FormControlOptions {
    /** The label of the form control. */
    label?: string
    /** The placeholder of the form control. */
    placeholder?: string
    /** A hint to be shown below the control. */
    hint?: string
    /** Custom validation messages to apply. */
    validationMessages?: Record<string, string>
    /** Checks whether the control should be disabled. */
    isDisabled?(formValue: Record<string, unknown>): boolean
}
