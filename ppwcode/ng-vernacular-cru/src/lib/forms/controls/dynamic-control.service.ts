import {
    ComponentFactory,
    ComponentFactoryResolver,
    ComponentRef,
    Injectable,
    Type,
    ViewContainerRef
} from '@angular/core'

import { AbstractDynamicService } from '../../dynamic-components/abstract-dynamic-service'
import { FormControlComponent, FormControlComponentProperties } from './form-control-component.mixin'

/**
 * This service is responsible for managing the different types of control components for a form control type and
 * instantiating them on a view container reference.
 *
 * Each type can only have a single control component so that a uniform layout will be applied
 * everywhere throughout the application.
 */
@Injectable({
    providedIn: 'root'
})
export class DynamicControlService extends AbstractDynamicService<FormControlComponent> {
    constructor(private readonly componentFactoryResolver: ComponentFactoryResolver) {
        super()
    }

    /**
     * Instantiates a component on the given view container reference based on the provided type.
     * @param viewContainerRef The view container reference to instantiate the component on.
     * @param type The type to instantiate the component for.
     * @param formControlInput The input properties for the form control instance.
     */
    public createComponent<TComponent extends FormControlComponent>(
        viewContainerRef: ViewContainerRef,
        type: string,
        formControlInput: FormControlComponentProperties
    ): ComponentRef<TComponent> {
        const componentFactory: ComponentFactory<TComponent> = this.getComponentFactory(
            type
        ) as ComponentFactory<TComponent>
        const componentRef: ComponentRef<TComponent> = viewContainerRef.createComponent(componentFactory)

        componentRef.instance.options = formControlInput.options
        componentRef.instance.localFormControl = formControlInput.localFormControl

        return componentRef
    }

    /**
     * Gets the component factory based on the given type.
     * If no component type could be found for the given type, an error is thrown.
     * @param type The type to get the component factory for.
     * @private
     */
    private getComponentFactory(type: string): ComponentFactory<FormControlComponent> {
        const componentType: Type<FormControlComponent> | undefined = this.componentTypeMap.get(type)
        if (!componentType) {
            throw new Error(
                `Component type ${type} is not supported by the dynamic control directive.\n` +
                    `Supported types are ${Array.from(this.componentTypeMap.keys()).join(', ')}.`
            )
        }

        return this.componentFactoryResolver.resolveComponentFactory(componentType)
    }
}
