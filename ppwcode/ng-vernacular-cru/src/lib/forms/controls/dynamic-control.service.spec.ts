import { Component, ComponentRef, ViewContainerRef } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { FormControl, ReactiveFormsModule } from '@angular/forms'
import { MatLegacyDialogModule as MatDialogModule } from '@angular/material/legacy-dialog'
import { NoopAnimationsModule } from '@angular/platform-browser/animations'
import { createComponentFactory, Spectator } from '@ngneat/spectator'
import { TranslateModule } from '@ngx-translate/core'

import { DynamicControlService } from './dynamic-control.service'
import { FormControlComponent } from './form-control-component.mixin'
import { PpwcodeVernacularFormsModule } from '../ppwcode-vernacular-forms.module'
import { TextControlComponent } from './text-control/text-control.component'

/**
 * This host component is being used to have a real ViewContainerRef to work with.
 */
@Component({
    template: ''
})
class HostComponent {
    constructor(public readonly viewContainerRef: ViewContainerRef) {}
}

describe('DynamicControlService', () => {
    let spectator: Spectator<HostComponent>
    const createComponent = createComponentFactory({
        component: HostComponent,
        declarations: [HostComponent, TextControlComponent],
        imports: [
            NoopAnimationsModule,
            PpwcodeVernacularFormsModule,
            MatDialogModule,
            ReactiveFormsModule,
            TranslateModule.forRoot()
        ]
    })
    let service: DynamicControlService

    beforeEach(() => {
        spectator = createComponent()
        service = TestBed.inject(DynamicControlService)
    })

    it('should register a component', () => {
        expect(service.componentTypeMap.size).toEqual(1) // 1 for the default assigned types

        service.registerComponent('textInTest', TextControlComponent)

        expect(service.componentTypeMap.size).toEqual(2)
    })

    it('should register an array of components', () => {
        expect(service.componentTypeMap.size).toEqual(1) // 1 for the default assigned types

        service.registerComponents([['textInTest', TextControlComponent]])

        expect(service.componentTypeMap.size).toEqual(2)
    })

    describe('with a component registered', () => {
        it('should create a new instance of the component', () => {
            expect(() => {
                const componentRef: ComponentRef<FormControlComponent> = service.createComponent(
                    spectator.component.viewContainerRef,
                    'text',
                    {
                        localFormControl: new FormControl('')
                    }
                )
                expect(componentRef.instance).toBeInstanceOf(TextControlComponent)
            }).not.toThrow()
        })

        it('should throw when there is no registered component for the given type', () => {
            expect(() => {
                service.createComponent(spectator.component.viewContainerRef, 'nonExistingType', {
                    localFormControl: new FormControl('')
                })
            }).toThrow()
        })
    })
})
