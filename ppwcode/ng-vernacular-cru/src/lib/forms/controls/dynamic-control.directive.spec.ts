import { Component, Type, ViewChild } from '@angular/core'
import { TestBed } from '@angular/core/testing'
import { FormControl, ReactiveFormsModule } from '@angular/forms'
import { MatLegacyDialogModule as MatDialogModule } from '@angular/material/legacy-dialog'
import { NoopAnimationsModule } from '@angular/platform-browser/animations'
import { TranslateModule } from '@ngx-translate/core'

import { DynamicControlDirective } from './dynamic-control.directive'
import { FormControlConfig } from './form-control-config'
import { PpwcodeVernacularFormsModule } from '../ppwcode-vernacular-forms.module'
import { TextControlComponent } from './text-control/text-control.component'

describe('DynamicControlDirective', () => {
    const instantiateTestComponent = <T>(component: Type<T>) => {
        const fixture = TestBed.createComponent(component)
        fixture.detectChanges()

        return fixture.componentInstance
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DynamicTextControlTestComponent, InvalidControlTypeTestComponent],
            imports: [
                NoopAnimationsModule,
                PpwcodeVernacularFormsModule,
                MatDialogModule,
                ReactiveFormsModule,
                TranslateModule.forRoot()
            ]
        })
    })

    it('should handle text controls', () => {
        const testComponent = instantiateTestComponent(DynamicTextControlTestComponent)
        const controlInstance = testComponent.dynamicControlDirective.componentRef.instance
        expect(controlInstance).toBeInstanceOf(TextControlComponent)
        expect(controlInstance.localFormControl).toBe(testComponent.formControl)
    })

    it('should throw when no component factory could be found', () => {
        expect(() => {
            instantiateTestComponent(InvalidControlTypeTestComponent)
        }).toThrowError(
            'Component type awesomecontrol is not supported by the dynamic control directive.\n' +
                `Supported types are text.`
        )
    })
})

/** Component to test that a text control is instantiated. */
@Component({
    template: ` <ng-container ppwcodeCruDynamicControl [config]="config" [formControl]="formControl"></ng-container> `
})
class DynamicTextControlTestComponent {
    public config: FormControlConfig = {
        type: 'text',
        name: 'name',
        options: {
            label: 'Name'
        }
    }

    public formControl = new FormControl('')

    @ViewChild(DynamicControlDirective)
    public dynamicControlDirective!: DynamicControlDirective
}

/** Component to test that an autocomplete control is instantiated. */
@Component({
    template: ` <ng-container ppwcodeCruDynamicControl [config]="config" [formControl]="formControl"></ng-container> `
})
class InvalidControlTypeTestComponent {
    config: FormControlConfig = {
        // "as any" to fake invalid config because TypeScript helps us with type safety when used correctly.
        type: 'awesomecontrol' as any,
        name: 'productCategory',
        options: {
            label: 'Product Category'
        }
    }

    formControl = new FormControl('')

    @ViewChild(DynamicControlDirective)
    public dynamicControlDirective!: DynamicControlDirective
}
