import { ComponentRef, Directive, Input, OnInit, Renderer2, Self, ViewContainerRef } from '@angular/core'
import { ControlValueAccessor, FormControl, NgControl } from '@angular/forms'
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert'

import { DynamicControlService } from './dynamic-control.service'
import { FormControlComponent } from './form-control-component.mixin'
import { FormControlConfig } from './form-control-config'

/**
 * Directive that is capable of rendering controls dynamically.
 * It will instantiate the correct component based on the given configuration
 * and pass the specified options onwards.
 *
 * Implements the ControlValueAccessor as an abstraction layer so that both
 * reactive forms and template driven forms are supported. Acts as a proxy to
 * the ControlValueAccessor implementation on the form control.
 */
@Directive({
    selector: '[ppwcodeCruDynamicControl]'
})
export class DynamicControlDirective implements ControlValueAccessor, OnInit {
    /** The configuration to apply on the control. */
    @Input() public config!: FormControlConfig

    /** A reference to the instantiated component. */
    public componentRef!: ComponentRef<FormControlComponent>

    public delayedInitializers: Array<() => void> = []

    constructor(
        private readonly viewContainerRef: ViewContainerRef,
        @Self() private readonly ngControl: NgControl,
        private readonly renderer: Renderer2,
        private readonly dynamicControlService: DynamicControlService
    ) {
        this.ngControl.valueAccessor = this
    }

    public ngOnInit(): void {
        notUndefined(this.config)

        this.componentRef = this.dynamicControlService.createComponent(this.viewContainerRef, this.config.type, {
            options: this.config.options,
            localFormControl: this.ngControl.control as FormControl
        })
        this.renderer.setAttribute(this.componentRef.location.nativeElement, 'data-control-name', this.config.name)
        this.renderer.addClass(this.componentRef.location.nativeElement, 'flex')
        this.renderer.addClass(this.componentRef.location.nativeElement, 'flex-col')

        // It could happen that Angular is already instantiating the DynamicControlDirective instance as a
        // ControlValueAccessor (thus calling the writeValue, registerOnChange, ... methods) but without the dynamic
        // control component already instantiated. When that's the case, the execution of those functions gets delayed
        // by pushing the call to the next array. After the dynamic control component has been instantiated, we can
        // safely call these 'initializers' again.
        this.delayedInitializers.forEach((initializer: () => void) => initializer())
        this.delayedInitializers = []
    }

    /**
     * ----------------------------------
     * CONTROL VALUE ACCESSOR ABSTRACTION
     * ----------------------------------
     */

    public writeValue(value: unknown): void {
        if (this.componentRef === undefined) {
            this.delayedInitializers.push(() => this.writeValue(value))
        } else {
            this.componentRef.instance.writeValue(value)
        }
    }

    public registerOnChange(fn: (value: unknown) => void): void {
        if (this.componentRef === undefined) {
            this.delayedInitializers.push(() => this.registerOnChange(fn))
        } else {
            this.componentRef.instance.registerOnChange(fn)
        }
    }

    public registerOnTouched(fn: () => void): void {
        if (this.componentRef === undefined) {
            this.delayedInitializers.push(() => this.registerOnTouched(fn))
        } else {
            this.componentRef.instance.registerOnTouched(fn)
        }
    }

    public setDisabledState(isDisabled: boolean): void {
        if (this.componentRef === undefined) {
            this.delayedInitializers.push(() => this.setDisabledState(isDisabled))
        } else {
            this.componentRef.instance.setDisabledState(isDisabled)
        }
    }
}
