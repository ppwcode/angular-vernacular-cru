/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { mixinFormControlComponent } from './form-control-component.mixin'
import { canBeDisabledSuite } from './control-value-accessor/testing/can-be-disabled-suite'
import { onChangeSuite } from './control-value-accessor/testing/on-change-suite'
import { onTouchedSuite } from './control-value-accessor/testing/on-touched-suite'

describe('FormControlComponent mixin', () => {
    const getInstance = () => new (mixinFormControlComponent())()

    canBeDisabledSuite(getInstance)
    onChangeSuite(getInstance)
    onTouchedSuite(getInstance)

    it('should update the form control value if it is different from the current value', () => {
        class BaseClass {
            public property: string = 'value'
        }

        const combinedClass = mixinFormControlComponent<number, typeof BaseClass>(null, BaseClass)
        const instance = new combinedClass()
        expect(instance.property).toEqual('value')
        expect(instance.localFormControl.value).toBeNull()

        instance.writeValue(123)
        expect(instance.localFormControl.value).toEqual(123)

        spyOn(instance.localFormControl, 'setValue').and.callThrough()
        instance.writeValue(123)
        expect(instance.localFormControl.value).toEqual(123)
        expect(instance.localFormControl.setValue).not.toHaveBeenCalled()
    })
})
