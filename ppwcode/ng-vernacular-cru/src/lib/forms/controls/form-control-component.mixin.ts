/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

// eslint-disable-next-line max-classes-per-file
import { ControlValueAccessor, FormControl } from '@angular/forms'

import { Constructor } from '../../utils/constructor'
import {
    ControlValueAccessorCtor,
    mixinControlValueAccessor
} from './control-value-accessor/control-value-accessor.mixin'
import { FormControlOptions } from './form-control-config'

/**
 * Defines the properties specific to the FormControlComponent interface.
 */
export interface FormControlComponentProperties {
    /** The options for the form control. */
    options?: FormControlOptions
    /** A local form control instance to be used within the template. */
    localFormControl: FormControl
}

/**
 * Defines that a component is a form control component.
 */
export interface FormControlComponent extends ControlValueAccessor, FormControlComponentProperties {
    setDisabledState(isDisabled: boolean): void
}

export type FormControlComponentCtor<TValue> = Constructor<FormControlComponent> & ControlValueAccessorCtor<TValue>

/**
 * Mixin that extends the given class with functionality to create a form control component.
 * @param defaultValue The default value to apply for the form control.
 * @param base The base class to apply the form control component logic to.
 */
// eslint-disable-next-line @typescript-eslint/ban-types
export const mixinFormControlComponent = <TValue, TBase extends Constructor<{}>>(
    defaultValue: TValue | null = null,
    base?: TBase
): FormControlComponentCtor<TValue> & TBase => {
    const baseClass: TBase = base ?? (class {} as TBase)

    return class extends mixinControlValueAccessor<TValue, TBase>(baseClass) implements FormControlComponent {
        public options?: FormControlOptions = {}

        public localFormControl: FormControl = new FormControl(defaultValue)

        /** Function called when a developer updates the value of a control in its code. */
        public writeValue(value: TValue | null): void {
            if (value !== this.localFormControl.value) {
                this.localFormControl.setValue(value)
            }
        }
    }
}
