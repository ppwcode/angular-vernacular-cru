/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { CanBeDisabled } from '../mixins/can-be-disabled.mixin'

/**
 * This suite can be used to verify the base implementation of the canBeDisabled mixin on a component.
 * @param instanceGenerator Function that returns the instance to be verified.
 */
export const canBeDisabledSuite = (instanceGenerator: () => CanBeDisabled): void => {
    describe('CanBeDisabled implementation', () => {
        it('should have a base implementation for setDisabledState', () => {
            const instance: CanBeDisabled = instanceGenerator()
            expect(instance.isDisabled).toBe(false)

            instance.setDisabledState(true)
            expect(instance.isDisabled).toBe(true)

            instance.setDisabledState(false)
            expect(instance.isDisabled).toBe(false)
        })
    })
}
