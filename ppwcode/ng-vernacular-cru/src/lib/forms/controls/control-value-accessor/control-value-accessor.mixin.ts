/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Constructor } from '../../../utils/constructor'
import { CanBeDisabledCtor, mixinCanBeDisabled } from './mixins/can-be-disabled.mixin'
import { mixinOnChange, OnChangeCtor } from './mixins/on-change.mixin'
import { mixinOnTouched, OnTouchedCtor } from './mixins/on-touched.mixin'

export type ControlValueAccessorCtor<TValue> = CanBeDisabledCtor & OnChangeCtor<TValue> & OnTouchedCtor

// eslint-disable-next-line @typescript-eslint/ban-types
export const mixinControlValueAccessor = <TValue, TBase extends Constructor<{}>>(
    base?: TBase
): ControlValueAccessorCtor<TValue> & TBase => mixinCanBeDisabled(mixinOnChange(mixinOnTouched(base)))
