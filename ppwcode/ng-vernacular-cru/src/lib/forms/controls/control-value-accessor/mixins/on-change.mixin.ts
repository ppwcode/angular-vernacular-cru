/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/* eslint-disable max-classes-per-file */
import { ControlValueAccessor } from '@angular/forms'

import { Constructor } from '../../../../utils/constructor'

export interface OnChange<TValue> extends Pick<ControlValueAccessor, 'registerOnChange'> {
    onChange: (value: TValue) => void
}

export type OnChangeCtor<TValue> = Constructor<OnChange<TValue>>

// eslint-disable-next-line @typescript-eslint/ban-types
export const mixinOnChange = <TValue, TBase extends Constructor<{}>>(base?: TBase): OnChangeCtor<TValue> & TBase => {
    const baseClass: TBase = base ?? (class {} as TBase)

    return class extends baseClass implements OnChange<TValue> {
        public onChange!: (value: TValue) => void

        public registerOnChange(fn: (value: TValue) => void): void {
            this.onChange = fn
        }
    }
}
