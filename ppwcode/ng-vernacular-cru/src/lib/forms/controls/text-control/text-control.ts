import { FormControlConfig } from '../form-control-config'
import { TextControlOptions } from './text-options'

/**
 * Configuration of a text control.
 */
export class TextControl implements FormControlConfig {
    /** The type of the form control. */
    public readonly type: string = 'text'

    constructor(
        /** The unique name of the control within the form. */
        public name: string,
        /** The options to be passed to the form control. */
        public options?: TextControlOptions
    ) {}
}
