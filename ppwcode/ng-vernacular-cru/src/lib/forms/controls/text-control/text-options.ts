import { FormControlOptions } from '../form-control-config'

/** The options for the text control. */
export interface TextControlOptions extends FormControlOptions {
    /** The way in which the text input is used. Fallback to 'text' if unspecified. */
    inputType?: string
}
