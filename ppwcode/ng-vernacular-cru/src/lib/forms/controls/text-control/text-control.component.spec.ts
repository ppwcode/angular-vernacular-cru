import { ReactiveFormsModule } from '@angular/forms'
import { MatLegacyFormFieldModule as MatFormFieldModule } from '@angular/material/legacy-form-field'
import { MatLegacyInputModule as MatInputModule } from '@angular/material/legacy-input'
import { NoopAnimationsModule } from '@angular/platform-browser/animations'
import { TranslateModule } from '@ngx-translate/core'
import { createComponentFactory, Spectator } from '@ngneat/spectator'

import { TEXT_CONTROL_VALUE_ACCESSOR, TextControlComponent } from './text-control.component'

describe('TextControlComponent', () => {
    let spectator: Spectator<TextControlComponent>
    const createComponent = createComponentFactory({
        component: TextControlComponent,
        imports: [
            NoopAnimationsModule,
            ReactiveFormsModule,
            MatFormFieldModule,
            MatInputModule,
            TranslateModule.forRoot()
        ]
    })

    beforeEach(async () => {
        spectator = createComponent()
    })

    it('should provide it correctly', () => {
        expect(TEXT_CONTROL_VALUE_ACCESSOR.useExisting()).toBe(TextControlComponent)
    })

    it('should support different input types', () => {
        spectator.component.options = undefined
        expect(spectator.component.inputType).toEqual('text')

        spectator.component.options = { label: 'My input field' }
        expect(spectator.component.inputType).toEqual('text')

        spectator.component.options.inputType = 'time'
        expect(spectator.component.inputType).toEqual('time')
    })

    it('should update the value internally when receiving a new value programmatically', () => {
        spyOn(spectator.component.localFormControl, 'setValue').and.callThrough()

        spectator.component.writeValue('John')
        expect(spectator.component.localFormControl.value).toEqual('John')
        expect(spectator.component.localFormControl.setValue).toHaveBeenCalledTimes(1)

        // Calling again with same value should not make any difference.
        spectator.component.writeValue('John')
        expect(spectator.component.localFormControl.value).toEqual('John')
        expect(spectator.component.localFormControl.setValue).toHaveBeenCalledTimes(1)

        spectator.component.writeValue(null)
        expect(spectator.component.localFormControl.value).toEqual(null)
        expect(spectator.component.localFormControl.setValue).toHaveBeenCalledTimes(2)

        spectator.component.writeValue(undefined)
        expect(spectator.component.localFormControl.value).toEqual(undefined)
        expect(spectator.component.localFormControl.setValue).toHaveBeenCalledTimes(3)
    })

    it('should log an error when the control receives an unsupported value type', () => {
        spyOn(console, 'error')

        // Casting this, to force the error.
        spectator.component.writeValue([] as unknown as string)

        expect(console.error).toHaveBeenCalled()
    })
})
