import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ExistingProvider,
    forwardRef,
    Input
} from '@angular/core'
import { NG_VALUE_ACCESSOR } from '@angular/forms'

import { mixinFormControlComponent } from '../form-control-component.mixin'
import { TextControlOptions } from './text-options'

export const TEXT_CONTROL_VALUE_ACCESSOR: ExistingProvider = {
    provide: NG_VALUE_ACCESSOR,
    // eslint-disable-next-line @angular-eslint/no-forward-ref
    useExisting: forwardRef(() => TextControlComponent),
    multi: true
}

/**
 * Component that represents a text field in a form.
 */
@Component({
    selector: 'ppwcode-cru-text-control',
    templateUrl: './text-control.component.html',
    providers: [],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextControlComponent extends mixinFormControlComponent() {
    @Input() public override options?: TextControlOptions

    constructor(private readonly changeDetectorRef: ChangeDetectorRef) {
        super()
    }

    /** The way in which the text input is used. Fallback to 'text' if unspecified. */
    public get inputType(): string {
        return this.options?.inputType ?? 'text'
    }

    public override writeValue(value?: string | null): void {
        if (value === this.localFormControl.value) {
            return
        }

        if (typeof value === 'string' || value === null || typeof value === 'undefined') {
            this.localFormControl.setValue(value)
            this.changeDetectorRef.detectChanges()
        } else {
            console.error(`Received invalid form control value ${value} for type text.`)
        }
    }
}
