/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { ReactiveFormsModule } from '@angular/forms'
import { MatLegacyFormFieldModule as MatFormFieldModule } from '@angular/material/legacy-form-field'
import { MatLegacyInputModule as MatInputModule } from '@angular/material/legacy-input'
import { TranslateModule } from '@ngx-translate/core'

import { DynamicControlDirective } from './controls/dynamic-control.directive'
import { DynamicControlService } from './controls/dynamic-control.service'
import { TextControlComponent } from './controls/text-control/text-control.component'
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component'

@NgModule({
    declarations: [TextControlComponent, DynamicControlDirective, DynamicFormComponent],
    imports: [CommonModule, MatFormFieldModule, ReactiveFormsModule, TranslateModule.forChild(), MatInputModule],
    exports: [TextControlComponent, DynamicControlDirective, DynamicFormComponent]
})
export class PpwcodeVernacularFormsModule {
    constructor(dynamicControlService: DynamicControlService) {
        dynamicControlService.registerComponents([['text', TextControlComponent]])
    }
}
