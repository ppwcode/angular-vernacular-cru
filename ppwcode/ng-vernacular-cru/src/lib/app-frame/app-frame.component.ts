import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout'
import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges,
    TemplateRef
} from '@angular/core'
import { FormControl } from '@angular/forms'
import { debounceTime } from 'rxjs/operators'

import { mixinHandleSubscriptions } from '../performance/handle-subscriptions.mixin'
import { whenPropertyChanges } from '../utils/ng-on-changes'
import { PreventNullOrUndefined } from '../decorators/prevent-null-or-undefined'
import { NavListItem } from './nav-list-item'
import { ThemePalette } from '@angular/material/core'

const SEARCH_DEBOUNCE_TIME: number = 250

/**
 * This component acts as a frame for the entire application.
 *
 * Using the `isMinimized` and `showSidenav` input bindings you can show or hide the sidenav
 * based on whether the user is logged in for example.
 *
 * When the device is a handset device, the `isMinimized` property will have no effect because the sidenav is either
 * fully shown or fully closed.
 *
 * A sidenav in a minimized state means that only the icons will be shown.
 */
@Component({
    selector: 'ppwcode-cru-app-frame',
    templateUrl: './app-frame.component.html',
    styleUrls: ['./app-frame.component.scss', '../tailwind.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppFrameComponent extends mixinHandleSubscriptions() implements OnInit, OnDestroy, OnChanges {
    /** Whether the sidenav is in a minimized state. */
    @Input() public isMinimized: boolean = false

    /** Whether the sidenav should be shown. */
    @Input() public showSidenav: boolean = false

    /** Whether the breadcrumbs should be shown. */
    @Input() public showBreadcrumbs: boolean = true

    /** Whether the indeterminate progress bar should be shown. */
    @PreventNullOrUndefined(false)
    @Input()
    public showProgressBar: boolean | null = false

    /** Extra menu items to be shown in the sidebar. */
    @PreventNullOrUndefined([])
    @Input()
    public navListItems: NavListItem[] = []

    /** The username to show in the top toolbar. */
    @Input()
    public userName: string | undefined

    /** The template for the username menu to show in the top toolbar. */
    @Input()
    public userNameMenuTemplate: TemplateRef<any> | null = null

    @PreventNullOrUndefined('')
    @Input()
    public searchPlaceholder: string = ''

    @Input()
    public searchButtonColor: ThemePalette = undefined

    @PreventNullOrUndefined('search')
    @Input()
    public searchButtonIcon: string = 'search'

    @PreventNullOrUndefined('material-icons')
    @Input()
    public searchButtonIconFontSet: string = 'material-icons'

    @Input()
    public searchFilterMenuTemplate: TemplateRef<any> | null = null

    /**
     * Event emitted when the user changes the value in the search input.
     * A debounce time of 250ms has been applied before emitting the event.
     */
    @Output() public readonly searchChange: EventEmitter<string> = new EventEmitter<string>()

    /**
     * Event emitted when the user click on his/her username in the toolbar.
     * Exact handling TBD by consuming application.
     */
    @Output() public readonly userNameClick: EventEmitter<void> = new EventEmitter<void>()

    /**
     * Determines whether the sidenav should be shown as minimized.
     * This can differ from the input binding based on state changes within the component.
     */
    public showAsMinimized: boolean = this.isMinimized

    /**
     * Determines whether the sidenav should be shown as opened.
     * This can differ from the input binding based on state changes within the component.
     */
    public showAsOpened: boolean = this.showSidenav

    /** The form control instance used to track the search input by the user. */
    public readonly searchControl: FormControl = new FormControl('')

    /** Gets whether the Handset breakpoint is matched. */
    public get isHandset(): boolean {
        return this.breakpointObserver.isMatched(Breakpoints.Handset)
    }

    constructor(private readonly breakpointObserver: BreakpointObserver) {
        super()
    }

    public ngOnInit(): void {
        this.stopOnDestroy(this.searchControl.valueChanges)
            .pipe(debounceTime(SEARCH_DEBOUNCE_TIME))
            .subscribe((value: string) => this.searchChange.emit(value))
    }

    // eslint-disable-next-line max-lines-per-function
    public ngOnChanges(changes: SimpleChanges): void {
        whenPropertyChanges<AppFrameComponent>(changes, {
            // If the `showSidenav` input binding changes and the sidenav should be shown, open it explicitly.
            showSidenav: () => {
                if (this.showSidenav) {
                    this.openSidenav()
                    if (this.isMinimized) {
                        this.showAsMinimized = true
                    }
                } else {
                    this.closeSidenav()
                }
            },
            // If the `isMinimized` input binding is also set to be minimized, enforce this as well.
            isMinimized: () => {
                this.showAsMinimized = this.isMinimized
            }
        })
    }

    /**
     * Sets the sidenav to be explicitly opened.
     * If the current device is not a handset device, it will also display the labels in the sidenav actions.
     */
    public openSidenav(): void {
        this.showAsOpened = true
        if (!this.isHandset) {
            this.showAsMinimized = false
        }
    }

    /**
     * Closes the sidenav entirely on a handset device.
     * Minimizes the sidenav on a non-handset device.
     */
    public closeSidenav(): void {
        if (this.isHandset) {
            this.showAsOpened = false
        } else {
            this.showAsMinimized = true
        }
    }

    /**
     * Resets the search control value to an empty string.
     */
    public clearSearch(): void {
        if (this.searchControl.value === '') {
            return
        }

        this.searchControl.patchValue('')
    }
}
