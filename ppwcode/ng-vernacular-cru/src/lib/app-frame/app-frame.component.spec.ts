import { createHostFactory, SpectatorHost } from '@ngneat/spectator'
import { AppFrameComponent } from './app-frame.component'
import { HarnessLoader } from '@angular/cdk/testing'
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed'
import { MatSidenavHarness } from '@angular/material/sidenav/testing'
import { Component } from '@angular/core'
import { PpwcodeVernacularCruModule } from '../vernacular-cru.module'
import { MatLegacyButtonHarness as MatButtonHarness } from '@angular/material/legacy-button/testing'
import { Subscription } from 'rxjs'
import { fakeAsync, tick } from '@angular/core/testing'
import { MatLegacyInputHarness as MatInputHarness } from '@angular/material/legacy-input/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { MatLegacyProgressBarHarness as MatProgressBarHarness } from '@angular/material/legacy-progress-bar/testing'

@Component({ template: '' })
class AppFrameHostComponent {}

interface SidenavStateExpectations {
    isOpened: boolean
    isMinimized: boolean
}

const appFrameTestSuite = (isMobile: boolean, expectedMode: 'over' | 'side' | 'push') => {
    let spectator: SpectatorHost<AppFrameComponent, AppFrameComponent>
    let harnessLoader: HarnessLoader
    const createHostComponent = createHostFactory({
        component: AppFrameComponent,
        host: AppFrameHostComponent,
        imports: [PpwcodeVernacularCruModule, RouterTestingModule]
    })

    const expectSidenav = async (expectations: SidenavStateExpectations) => {
        const sidenav: MatSidenavHarness = await harnessLoader.getHarness(MatSidenavHarness)
        expect(await sidenav.isOpen()).toBe(expectations.isOpened)
        expect(await (await sidenav.host()).hasClass('mobile')).toBe(isMobile)
        expect(await (await sidenav.host()).hasClass('minimized')).toBe(expectations.isMinimized)
        expect(await sidenav.getMode()).toBe(expectedMode)
    }

    const expectClosedSidenav = async (expectations: Omit<SidenavStateExpectations, 'isOpened'>) =>
        await expectSidenav({ isOpened: false, ...expectations })
    const expectOpenSidenav = async (expectations: Omit<SidenavStateExpectations, 'isOpened'>) =>
        await expectSidenav({ isOpened: true, ...expectations })

    describe(`for devices: 'handset = ${isMobile}'`, () => {
        it('should create with default values', async () => {
            spectator = createHostComponent('<ppwcode-cru-app-frame></ppwcode-cru-app-frame>')
            spyOnProperty(spectator.component, 'isHandset').and.returnValue(isMobile)
            harnessLoader = TestbedHarnessEnvironment.loader(spectator.hostFixture)

            await expectClosedSidenav({ isMinimized: false })
        })

        it('should create with an already open sidenav', async () => {
            spectator = createHostComponent('<ppwcode-cru-app-frame [showSidenav]="true"></ppwcode-cru-app-frame>')
            spyOnProperty(spectator.component, 'isHandset').and.returnValue(isMobile)
            harnessLoader = TestbedHarnessEnvironment.loader(spectator.hostFixture)

            await expectOpenSidenav({ isMinimized: false })
        })

        it('should create with an already open sidenav but minimized', async () => {
            spectator = createHostComponent(
                '<ppwcode-cru-app-frame [showSidenav]="true" [isMinimized]="true"></ppwcode-cru-app-frame>'
            )
            spyOnProperty(spectator.component, 'isHandset').and.returnValue(isMobile)
            harnessLoader = TestbedHarnessEnvironment.loader(spectator.hostFixture)

            await expectOpenSidenav({ isMinimized: true })
        })

        it('should create with minimized set to true but show to false', async () => {
            spectator = createHostComponent(
                '<ppwcode-cru-app-frame [showSidenav]="false" [isMinimized]="true"></ppwcode-cru-app-frame>'
            )
            spyOnProperty(spectator.component, 'isHandset').and.returnValue(isMobile)
            harnessLoader = TestbedHarnessEnvironment.loader(spectator.hostFixture)

            await expectClosedSidenav({ isMinimized: true })
        })

        describe('changes to input bindings', () => {
            it('should open the sidenav', async () => {
                spectator = createHostComponent('<ppwcode-cru-app-frame></ppwcode-cru-app-frame>')
                spyOnProperty(spectator.component, 'isHandset').and.returnValue(isMobile)
                spectator.setInput('showSidenav', true)

                harnessLoader = TestbedHarnessEnvironment.loader(spectator.hostFixture)
                await expectOpenSidenav({ isMinimized: false })
            })

            it('should display the progress bar', async () => {
                spectator = createHostComponent('<ppwcode-cru-app-frame></ppwcode-cru-app-frame>')
                spectator.setInput('showProgressBar', true)

                harnessLoader = TestbedHarnessEnvironment.loader(spectator.hostFixture)
                const matProgressHarness = await harnessLoader.getHarness(MatProgressBarHarness)
                await expectAsync(matProgressHarness.getMode()).toBeResolvedTo('indeterminate')
            })

            if (isMobile) {
                it('should close the sidenav entirely', async () => {
                    spectator = createHostComponent(
                        '<ppwcode-cru-app-frame [showSidenav]="true"></ppwcode-cru-app-frame>'
                    )
                    spyOnProperty(spectator.component, 'isHandset').and.returnValue(isMobile)
                    spectator.setInput('showSidenav', false)

                    harnessLoader = TestbedHarnessEnvironment.loader(spectator.hostFixture)
                    await expectClosedSidenav({ isMinimized: false })
                })
            } else {
                it('should minimize the sidenav when showSidenav becomes false', async () => {
                    spectator = createHostComponent(
                        '<ppwcode-cru-app-frame [showSidenav]="true"></ppwcode-cru-app-frame>'
                    )
                    spyOnProperty(spectator.component, 'isHandset').and.returnValue(isMobile)
                    spectator.setInput('showSidenav', false)

                    harnessLoader = TestbedHarnessEnvironment.loader(spectator.hostFixture)
                    await expectOpenSidenav({ isMinimized: true })
                })

                it('should maximize the sidenav when isMinimized becomes false', async () => {
                    spectator = createHostComponent(
                        '<ppwcode-cru-app-frame [showSidenav]="true" [isMinimized]="true"></ppwcode-cru-app-frame>'
                    )
                    spyOnProperty(spectator.component, 'isHandset').and.returnValue(isMobile)
                    spectator.setInput('isMinimized', false)

                    harnessLoader = TestbedHarnessEnvironment.loader(spectator.hostFixture)
                    await expectOpenSidenav({ isMinimized: false })
                })
            }
        })

        describe('clicking the sidenav toggle icons', () => {
            if (isMobile) {
                it('should open the sidenav with the menu icon in the main toolbar', async () => {
                    spectator = createHostComponent('<ppwcode-cru-app-frame></ppwcode-cru-app-frame>')
                    spyOnProperty(spectator.component, 'isHandset').and.returnValue(isMobile)
                    harnessLoader = TestbedHarnessEnvironment.loader(spectator.hostFixture)

                    const matButton = await harnessLoader.getHarness(
                        MatButtonHarness.with({
                            selector: `[aria-label="Toggle sidenav"]`
                        })
                    )
                    await matButton.click()

                    await expectOpenSidenav({ isMinimized: false })
                })
            } else {
                it('should maximize the sidenav with the chevron in the sidenav', async () => {
                    spectator = createHostComponent(
                        '<ppwcode-cru-app-frame [showSidenav]="true" [isMinimized]="true"></ppwcode-cru-app-frame>'
                    )
                    spyOnProperty(spectator.component, 'isHandset').and.returnValue(isMobile)
                    harnessLoader = TestbedHarnessEnvironment.loader(spectator.hostFixture)

                    // On non-handset devices, there's only one button.
                    const matButton = await harnessLoader.getHarness(MatButtonHarness)
                    await matButton.click()

                    await expectOpenSidenav({ isMinimized: false })
                })

                it('should minimize the sidenav with the chevron in the sidenav', async () => {
                    spectator = createHostComponent(
                        '<ppwcode-cru-app-frame [showSidenav]="true" [isMinimized]="false"></ppwcode-cru-app-frame>'
                    )
                    spyOnProperty(spectator.component, 'isHandset').and.returnValue(isMobile)
                    harnessLoader = TestbedHarnessEnvironment.loader(spectator.hostFixture)

                    // On non-handset devices, there's only one button.
                    const matButton = await harnessLoader.getHarness(MatButtonHarness)
                    await matButton.click()

                    await expectOpenSidenav({ isMinimized: true })
                })
            }
        })

        describe('search', () => {
            beforeEach(() => {
                spectator = createHostComponent(
                    '<ppwcode-cru-app-frame [showSidenav]="true" [isMinimized]="false"></ppwcode-cru-app-frame>'
                )
                spyOnProperty(spectator.component, 'isHandset').and.returnValue(isMobile)
                harnessLoader = TestbedHarnessEnvironment.loader(spectator.hostFixture)
            })

            it('should emit after 250ms', fakeAsync(async () => {
                let subscriptionHits: number = 0

                const subscription: Subscription = spectator.component.searchChange.subscribe(() => subscriptionHits++)
                const searchInput = await harnessLoader.getHarness(MatInputHarness)

                await searchInput.setValue('a')
                expect(subscriptionHits).toEqual(0)
                tick(200)
                expect(subscriptionHits).toEqual(0)
                tick(51) // 251ms total
                expect(subscriptionHits).toEqual(1)

                subscription.unsubscribe()
            }))

            it('should clear the input if it is not empty', async () => {
                spyOn(spectator.component.searchControl, 'patchValue').and.callThrough()
                const searchInput = await harnessLoader.getHarness(MatInputHarness)

                await searchInput.setValue('abc')
                expect(spectator.component.searchControl.value).toEqual('abc')

                spectator.component.clearSearch()
                expect(spectator.component.searchControl.value).toEqual('')
                expect(spectator.component.searchControl.patchValue).toHaveBeenCalledTimes(1)

                spectator.component.clearSearch()
                expect(spectator.component.searchControl.value).toEqual('')
                expect(spectator.component.searchControl.patchValue).toHaveBeenCalledTimes(1)

                await searchInput.setValue('defg')
                spectator.component.clearSearch()
                expect(spectator.component.searchControl.patchValue).toHaveBeenCalledTimes(2)
            })
        })
    })
}

describe('AppFrameComponent', () => {
    appFrameTestSuite(true, 'over')
    appFrameTestSuite(false, 'side')
})
