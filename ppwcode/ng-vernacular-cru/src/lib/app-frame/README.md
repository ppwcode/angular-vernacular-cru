# Angular Vernacular App Frame

This section of the library contains an implementation of a standardized app-frame to be used in a project.

## `AppFrameComponent`

The `AppFrameComponent` is the standardized component to wrap pages of an application in the same layout. It contains a
toolbar, sidenav, progress-bar and a section to show search results.

### Responsiveness

The component has responsive behaviour implemented to automatically hide the sidenav on initial load when it's a smaller
device. Using the input binding `[showSidenav]` you can manipulate whether the sidenav should be shown. An example use
case might be that the sidenav should only be visible for authenticated users.

### Search results

Upon using the search input field, the `(searchChange)` output event will be triggered. As a developer you can handle
this event, projecting the search results into the component by using an `ng-container` element with the `searchResults`
attribute.

```html
<ppwcode-cru-app-frame>
    <ng-container searchResults>
        <div *ngFor="let item of results$ | async as results">
            <!-- Template for a single search result item -->
        </div>
    </ng-container>
</ppwcode-cru-app-frame>
```

This will render the search results in its designated segment of the sidenav.

> NOTE
>
> Handling the search input change is demonstrated in the demo project. This has however been quite standardized
> already, meaning that a standardized implementation might become available quite soon in the library.

### Toolbar with breadcrumbs

The toolbar uses `ppwcode-cru-breadcrumb-bar` to show breadcrumbs. To get your breadcrumbs in this toolbar,
[read the docs on `ppwcode-cru-breadcrumb-bar`](../breadcrumbs/README.md).

### Page content

Below the toolbar, the content of the page will be projected. In most cases this will be the router outlet. To make sure
the page content is projected in the right section, use an `ng-container` element with the `page` attribute.

```html
<ppwcode-cru-app-frame>
    <ng-container page>
        <div class="fill">
            <router-outlet></router-outlet>
        </div>
    </ng-container>
</ppwcode-cru-app-frame>
```
