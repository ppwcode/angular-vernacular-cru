import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core'

@Component({
    selector: 'ppwcode-cru-user-name-button',
    templateUrl: './user-name-button.component.html',
    styleUrls: ['./user-name-button.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserNameButtonComponent {
    /** The username to show in the top toolbar. */
    @Input()
    public userName: string | undefined

    /** The template for the username menu to show in the top toolbar. */
    @Input()
    public userNameMenuTemplate: TemplateRef<any> | null = null

    /**
     * Event emitted when the user click on his/her username in the toolbar.
     * Exact handling TBD by consuming application.
     */
    @Output() public readonly userNameClick: EventEmitter<void> = new EventEmitter<void>()
}
