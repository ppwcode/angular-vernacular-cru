import { createComponentFactory, Spectator } from '@ngneat/spectator'
import { UserNameButtonComponent } from './user-name-button.component'
import { MatLegacyMenuModule as MatMenuModule } from '@angular/material/legacy-menu'

describe('UserNameButtonComponent', () => {
    let spectator: Spectator<UserNameButtonComponent>
    const createComponent = createComponentFactory({
        component: UserNameButtonComponent,
        imports: [MatMenuModule],
        mocks: []
    })

    beforeEach(() => {
        spectator = createComponent()
    })

    it('should create', () => {
        expect(spectator.component).toBeTruthy()
    })
})
