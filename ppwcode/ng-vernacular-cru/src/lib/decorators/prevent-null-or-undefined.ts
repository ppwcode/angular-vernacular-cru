/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * This decorator prevents the property from receiving a value that is `null` or `undefined`.
 * When it does, it will set the property to the provided fallback value instead.
 * @param fallbackValue The value to use when `null` or `undefined` is tried to be set on the property.
 * @constructor
 */
export function PreventNullOrUndefined<T>(fallbackValue: T): (target: unknown, propertyKey: string) => void {
    return function (target: unknown, propertyKey: string): void {
        let value: T

        const getter = function (): T {
            return value
        }

        const setter = function (newValue: T): void {
            value = newValue ?? fallbackValue
        }

        Object.defineProperty(target, propertyKey, {
            get: getter,
            set: setter
        })
    }
}
