/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { PreventNullOrUndefined } from './prevent-null-or-undefined'

describe('PreventNullOrUndefined decorator', () => {
    class TestClass {
        @PreventNullOrUndefined(123)
        public myProperty: number = 0
    }

    ;[null, undefined].forEach((value: unknown) => {
        // This test is necessary for dynamically assigned values which are made generic by using the `unknown` type.
        it(`should fall back for ${value}`, () => {
            const instance = new TestClass()
            expect(instance.myProperty).toEqual(0)

            instance.myProperty = value as number
            expect(instance.myProperty).toEqual(123)
        })
    })
})
