# Angular Vernacular CRU Decorators

This section of the library focuses on decorators that have been developed.

## `PreventNullOrUndefined`

This decorator is mainly used to resolve the issue that the Angular `AsyncPipe` can also return `null`.

In most cases, we don't want `null` or even `undefined` from being a possible value. Even though these are considered to
be falsy values, what if we want the default to be `true` in case of a `boolean`?

If we assign `true` as the default value, the first value as a result of the `async` pipe will be `null`. If the second
value of the pipe is `true` again, this might even cause the dreadful
`"Expression has changed after it was checked"`-error.

The `PreventNullOrUndefined` decorator will make sure that if `null` or `undefined` is set as the value for the
property, it will instead assign the `fallbackValue`.

```typescript
class MyComponent {
    @PreventNullOrUndefined(true)
    @Input()
    public showProgressBar: boolean | null = true
}
```

As you can tell from the example, to resolve the `"can't be of type 'null'"` error while linting the template, we still
need to make the property nullable. We know that, because of the decorator, the value will never be null so where you
need access to the property just use the `notNull` function of `@ppwcode/js-ts-oddsandends`.
