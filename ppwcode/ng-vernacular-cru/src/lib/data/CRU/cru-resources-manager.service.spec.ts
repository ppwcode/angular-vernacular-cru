import { createServiceFactory, SpectatorService, SpyObject } from '@ngneat/spectator'

import { CruResourcesManagerService } from './cru-resources-manager.service'
import { GenericReadFacade } from './abstract-read-facade'
import { TestFacade } from '../../testing/cru-facade'

describe('CruResourcesManagerService', () => {
    let spectator: SpectatorService<CruResourcesManagerService>
    let testFacade: SpyObject<TestFacade>
    const createService = createServiceFactory({
        service: CruResourcesManagerService,
        mocks: [TestFacade]
    })

    beforeEach(() => {
        spectator = createService()
        testFacade = spectator.inject(TestFacade)
    })

    it('should register a resource with its facade', () => {
        expect(spectator.service.resourcesMap.size).toEqual(0)

        spectator.service.registerResource('TestingPerson', TestFacade)

        expect(spectator.service.resourcesMap.size).toEqual(1)
    })

    it('should throw when the facade for the given type is not found', () => {
        expect(() => {
            spectator.service.getFacadeInstance('NonExistingType')
        }).toThrow()
    })

    describe('with a registered resource', () => {
        beforeEach(() => spectator.service.registerResource('TestingPerson', TestFacade))

        it('should return the facade instance', () => {
            const facade: GenericReadFacade = spectator.service.getFacadeInstance('TestingPerson')

            expect(facade).toBe(testFacade)
        })
    })
})
