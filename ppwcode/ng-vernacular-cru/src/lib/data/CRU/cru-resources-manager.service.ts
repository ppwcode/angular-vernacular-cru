import { Injectable, Injector, Type } from '@angular/core'
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert'

import { CruStateModel } from '../../+state/cru-state-model'
import { AbstractReadFacade, GenericReadFacade } from './abstract-read-facade'
import { GetContext } from '../api-interaction/abstract-read-service'

/**
 * This service is intended to manage the resources for which a CRU is possible in the application.
 *
 * The service should know about _all_ resources and their corresponding AbstractCruFacade implementation
 * so that the generic layers of the library are able to call the correct methods on the facade when necessary.
 */
@Injectable({
    providedIn: 'root'
})
export class CruResourcesManagerService {
    /** Keeps track of the registered resources with their corresponding facade type. */
    public resourcesMap: Map<string, Type<GenericReadFacade>> = new Map<string, Type<GenericReadFacade>>()

    constructor(private readonly injector: Injector) {}

    /**
     * Registers a resource for the given type together with its facade that manages the resource.
     * @param type The type of the resource.
     * @param facade The facade that manages the resource.
     */
    public registerResource<TResource extends Record<string, unknown>, TResourceDto extends Record<string, unknown>>(
        type: string,
        facade: Type<AbstractReadFacade<TResource, CruStateModel<TResource>, TResourceDto, GetContext<string>>>
    ): void {
        this.resourcesMap.set(
            type,
            facade as Type<
                AbstractReadFacade<
                    Record<string, unknown>,
                    CruStateModel<Record<string, unknown>>,
                    Record<string, unknown>,
                    GetContext<string>
                >
            >
        )
    }

    /**
     * Gets the actual instance of the facade from the Angular injector for the given type of resource.
     * @param resourceType The type of resource. This is not the type of the facade!
     */
    public getFacadeInstance(resourceType: string): GenericReadFacade {
        const facadeType: Type<GenericReadFacade> = notUndefined(this.resourcesMap.get(resourceType))

        return this.injector.get(facadeType)
    }
}
