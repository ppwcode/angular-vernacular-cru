/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { RouterTestingModule } from '@angular/router/testing'
import { createServiceFactory, mockProvider, SpectatorService, SpyObject } from '@ngneat/spectator'
import { NgxsModule } from '@ngxs/store'
import { of, throwError } from 'rxjs'
import { first } from 'rxjs/operators'

import { TestState } from '../../testing/state'
import { TestingPerson, testingPersons } from '../../testing/testing-person'
import { NgxsDispatchPluginModule } from '@ngxs-labs/dispatch-decorator'
import { NgxsSelectSnapshotModule } from '@ngxs-labs/select-snapshot'
import { TestCruService } from '../../testing/cru-service'
import { TestFacade } from '../../testing/cru-facade'

describe('AbstractCruFacade', () => {
    let spectator: SpectatorService<TestFacade>
    let service: SpyObject<TestCruService>
    let state: TestState
    let resource: TestingPerson

    const createService = createServiceFactory({
        service: TestFacade,
        imports: [
            NgxsModule.forRoot([TestState], { developmentMode: true }),
            NgxsDispatchPluginModule.forRoot(),
            NgxsSelectSnapshotModule.forRoot(),
            RouterTestingModule
        ],
        providers: [mockProvider(TestCruService)]
    })

    beforeEach(() => {
        resource = testingPersons.johnDoe
        spectator = createService()
        service = spectator.inject(TestCruService)
        state = spectator.inject(TestState)
    })

    afterEach(() => state.reset())

    it('should create a resource if there is no id given', async () => {
        const newPerson: TestingPerson = { ...resource, id: undefined }
        service.create.and.returnValue(of(newPerson))

        await spectator.service.upsert(newPerson).pipe(first()).toPromise()

        expect(service.create).toHaveBeenCalledWith(newPerson)
    })

    it('should update a resource if there is an existing id for it', async () => {
        service.update.and.returnValue(of(resource))

        await spectator.service.upsert(resource).pipe(first()).toPromise()

        expect(service.update).toHaveBeenCalledWith(resource, {})
    })

    it('should handle 409 error on upsert', async () => {
        spectator.service.selectResource(resource)
        service.update.and.returnValue(throwError({ status: 409 }))

        await expectAsync(spectator.service.upsert(resource).pipe(first()).toPromise()).toBeResolvedTo(
            testingPersons.johnDoe
        )

        expect(service.update).toHaveBeenCalledWith(resource, {})
    })

    it('should handle 412 error on upsert', async () => {
        service.update.and.returnValue(throwError({ status: 412, error: testingPersons.johnDoe }))

        const result = await spectator.service.upsert(resource).pipe(first()).toPromise()

        expect(service.update).toHaveBeenCalledWith(resource, {})
        expect(result).toEqual(testingPersons.johnDoe)
    })

    it('should throw on all other errors on upsert', async () => {
        service.update.and.returnValue(throwError({ status: 400 }))

        await expectAsync(spectator.service.upsert(resource).pipe(first()).toPromise()).toBeRejected()
    })
})
