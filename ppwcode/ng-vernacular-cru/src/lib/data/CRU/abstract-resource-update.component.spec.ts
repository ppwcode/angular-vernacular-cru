/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator'
import { RouterTestingModule } from '@angular/router/testing'
import { TestFacade } from '../../testing/cru-facade'
import { of, throwError } from 'rxjs'
import { TestUpdateComponent } from '../../testing/update-component'
import { testingPersons } from '../../testing/testing-person'
import { ActivatedRoute, Router } from '@angular/router'
import { first } from 'rxjs/operators'
import { TestResolver } from '../../testing/resolver'
import { DynamicFormComponent } from '../../forms/dynamic-form/dynamic-form.component'
import { FormBuilder } from '@angular/forms'
import { TranslateModule, TranslateService } from '@ngx-translate/core'

describe('AbstractResourceUpdateComponent', () => {
    let spectator: Spectator<TestUpdateComponent>
    let router: SpyObject<Router>
    let facade: SpyObject<TestFacade>

    const createComponent = createComponentFactory({
        component: TestUpdateComponent,
        imports: [RouterTestingModule, TranslateModule],
        declarations: [DynamicFormComponent],
        providers: [
            FormBuilder,
            { provide: ActivatedRoute, useValue: { snapshot: { params: {}, data: { resourceType: 'person' } } } }
        ],
        mocks: [Router, TestFacade, TestResolver, TranslateService]
    })

    beforeEach(() => {
        spectator = createComponent({ detectChanges: false })

        facade = spectator.inject(TestFacade)
        facade.selectedResource$ = of(testingPersons.johnDoe)
        facade.selectedResourceHistory$ = of([{ createdAt: testingPersons.johnDoe.createdAt, href: 'hrefvalue' }])

        router = spectator.inject(Router)
        router.navigate.and.returnValue(Promise.resolve())
        router.navigateByUrl.and.returnValue(of(true))

        spectator.detectChanges()
    })

    it('should create', () => {
        expect(spectator.component).toBeTruthy()
    })

    it('should resolve the selected resource', async () => {
        await expectAsync(spectator.component.selectedResource$.pipe(first()).toPromise()).toBeResolvedTo(
            testingPersons.johnDoe
        )
    })

    it('should save the resource', async () => {
        facade.upsert.and.returnValue(of(testingPersons.johnDoe))

        spectator.component.dynamicFormComponent.formGroup.patchValue({ lastName: 'Smith' })
        spectator.detectChanges()

        await spectator.component.saveForm()

        expect(facade.upsert).toHaveBeenCalledWith({ ...testingPersons.johnDoe, lastName: 'Smith' }, {})
    })

    it('should catch errors thrown when saving the resource', async () => {
        facade.upsert.and.returnValue(throwError('Mock Error'))

        spectator.component.dynamicFormComponent.formGroup.patchValue({ lastName: 'Smith' })
        spectator.detectChanges()

        await spectator.component.saveForm()
        expect(facade.upsert).toHaveBeenCalledWith({ ...testingPersons.johnDoe, lastName: 'Smith' }, {})
    })
})
