/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Store } from '@ngxs/store'
import { Observable } from 'rxjs'

import { AbstractCruState } from '../../+state/abstract-cru-state'
import { CruStateModel } from '../../+state/cru-state-model'
import { AbstractReadListService } from '../api-interaction/abstract-read-list-service'
import { AbstractReadFacade } from './abstract-read-facade'
import { GetContext } from '../api-interaction/abstract-read-service'

/** Represents an implemented AbstractReadFacade in its most generic way using `Record<string, unknown>`. */
export declare type GenericReadListFacade = AbstractReadListFacade<
    Record<string, unknown>,
    CruStateModel<Record<string, unknown>>,
    Record<string, unknown>,
    GetContext<string>
>

/**
 * This abstract class represents a base implementation for a read facade.
 *
 * It manages the process of getting resources in the application in a generic way.
 */
export abstract class AbstractReadListFacade<
    TResource extends Record<string, unknown>,
    TStateModel extends CruStateModel<TResource>,
    TResourceDto extends Record<string, unknown>,
    TGetContext extends GetContext<string>
> extends AbstractReadFacade<TResource, TStateModel, TResourceDto, TGetContext> {
    protected constructor(
        store: Store,
        cruState: AbstractCruState<TResource, TStateModel>,
        protected override readonly cruService: AbstractReadListService<TResource, TResourceDto, TGetContext>
    ) {
        super(store, cruState, cruService)
    }

    /**
     * Gets all the resources.
     */
    public getAll(): Observable<Array<TResource>> {
        return this.cruService.getAll()
    }
}
