/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Directive, OnInit, ViewChild } from '@angular/core'
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert'
import { AbstractResourceComponent } from './abstract-resource.component'
import { ActivatedRoute, Router } from '@angular/router'
import { AbstractCruFacade } from './abstract-cru-facade'
import { DynamicFormComponent } from '../../forms/dynamic-form/dynamic-form.component'
import { catchError, first, switchMap, tap } from 'rxjs/operators'
import { navigateToSelectedResource } from '../../routing/navigation'
import { EMPTY } from 'rxjs'
import { DateTime } from '@ppwcode/openapi/time/DateTime'
import { GetContext } from '../api-interaction/abstract-read-service'

@Directive()
export abstract class AbstractResourceUpdateComponent<
        R extends { createdAt: DateTime },
        F extends AbstractCruFacade<any, any, any, G>,
        G extends GetContext<string> = GetContext<string>
    >
    extends AbstractResourceComponent<R, F, G>
    implements OnInit
{
    @ViewChild(DynamicFormComponent) public override dynamicFormComponent!: DynamicFormComponent

    constructor(protected readonly router: Router, protected readonly route: ActivatedRoute) {
        super()
    }

    public async saveForm(): Promise<void> {
        await this.selectedResource$
            .pipe(
                first(),
                switchMap((selectedResource: R | null) =>
                    this.facade.upsert(
                        {
                            ...selectedResource,
                            ...notUndefined(this.dynamicFormComponent.formGroup).value
                        },
                        this.route.snapshot.params
                    )
                ),
                tap((savedResource: R) => this.facade.selectResource(savedResource)),
                switchMap(() => navigateToSelectedResource(this.router, this.facade, this.route.snapshot.params)),
                catchError(() => EMPTY)
            )
            .toPromise()
    }
}
