/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { RouterTestingModule } from '@angular/router/testing'
import { createServiceFactory, mockProvider, SpectatorService, SpyObject } from '@ngneat/spectator'
import { NgxsModule } from '@ngxs/store'

import { TestState } from '../../testing/state'
import { NgxsDispatchPluginModule } from '@ngxs-labs/dispatch-decorator'
import { NgxsSelectSnapshotModule } from '@ngxs-labs/select-snapshot'
import { TestFacade } from '../../testing/cru-facade'
import { TestCruService } from '../../testing/cru-service'

describe('AbstractReadFacade', () => {
    let spectator: SpectatorService<TestFacade>
    let service: SpyObject<TestCruService>
    let state: TestState

    const createService = createServiceFactory({
        service: TestFacade,
        imports: [
            NgxsModule.forRoot([TestState], { developmentMode: true }),
            NgxsDispatchPluginModule.forRoot(),
            NgxsSelectSnapshotModule.forRoot(),
            RouterTestingModule
        ],
        providers: [mockProvider(TestCruService)]
    })

    beforeEach(() => {
        spectator = createService()
        service = spectator.inject(TestCruService)
        state = spectator.inject(TestState)
    })

    afterEach(() => state.reset())

    it('should get all resources', () => {
        spectator.service.getAll()
        expect(service.getAll).toHaveBeenCalled()
    })
})
