/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { AfterViewInit, Directive, OnInit, ViewChild } from '@angular/core'
import { combineLatest, Observable } from 'rxjs'

import { distinctUntilChanged, map } from 'rxjs/operators'
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert'
import { mixinHandleSubscriptions } from '../../performance/handle-subscriptions.mixin'
import { HistoricVersion } from './historic-version'
import { AbstractReadFacade } from './abstract-read-facade'
import { DynamicFormComponent } from '../../forms/dynamic-form/dynamic-form.component'
import { DateTime } from '@ppwcode/openapi/time/DateTime'
import { GetContext } from '../api-interaction/abstract-read-service'

@Directive()
export abstract class AbstractResourceComponent<
        R extends { createdAt: DateTime },
        F extends AbstractReadFacade<R, any, any, G>,
        G extends GetContext<string> = GetContext<string>
    >
    extends mixinHandleSubscriptions()
    implements OnInit, AfterViewInit
{
    @ViewChild(DynamicFormComponent) public dynamicFormComponent?: DynamicFormComponent

    public selectedResource$!: Observable<R | null>
    public selectedResourceHistory$!: Observable<Array<HistoricVersion> | null>
    public isLatestVersion$!: Observable<boolean>

    public abstract facade: F

    public get isBusy$(): Observable<boolean> {
        return this.facade.isBusy$
    }

    public ngOnInit(): void {
        this.selectedResource$ = this.facade.selectedResource$
        this.selectedResourceHistory$ = this.facade.selectedResourceHistory$
        this.isLatestVersion$ = combineLatest([this.selectedResource$, this.selectedResourceHistory$]).pipe(
            map(
                ([resource, history]: [R | null, Array<HistoricVersion> | null]) =>
                    resource !== null && history !== null && resource.createdAt === notUndefined(history[0]).createdAt
            )
        )
    }

    public ngAfterViewInit(): void {
        this.stopOnDestroy(this.selectedResource$.pipe(distinctUntilChanged())).subscribe(
            (resource: R | null): void => {
                if (resource && this.dynamicFormComponent) {
                    this.dynamicFormComponent.reset(resource)
                }
            }
        )
    }
}
