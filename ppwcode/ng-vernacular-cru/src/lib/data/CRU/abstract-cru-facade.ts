/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Store } from '@ngxs/store'
import { Observable, of, throwError } from 'rxjs'
import { catchError, finalize, first, map, switchMap, tap } from 'rxjs/operators'

import { AbstractCruState } from '../../+state/abstract-cru-state'
import { CruStateModel } from '../../+state/cru-state-model'
import { AbstractCruService } from '../api-interaction/abstract-cru-service'
import { HttpErrorResponse } from '@angular/common/http'
import { AbstractReadListFacade } from './abstract-read-list-facade'
import { GetContext } from '../api-interaction/abstract-read-service'

/**
 * This abstract class represents a base implementation for a CRU facade.
 *
 * It manages the process of getting, creating and updating resources in the application in a generic way.
 */
export abstract class AbstractCruFacade<
    TResource extends Record<string, unknown>,
    TStateModel extends CruStateModel<TResource>,
    TResourceDto extends Record<string, unknown>,
    TGetContext extends GetContext<string>,
    TPathContext extends Record<string, unknown> = TResourceDto,
    TCreateContext extends Record<string, unknown> = TResourceDto,
    TUpdatePayload extends Record<string, unknown> = TResourceDto
> extends AbstractReadListFacade<TResource, TStateModel, TResourceDto, TGetContext> {
    protected constructor(
        store: Store,
        cruState: AbstractCruState<TResource, TStateModel>,
        protected override cruService: AbstractCruService<
            TResource,
            TResourceDto,
            TGetContext,
            TPathContext,
            TCreateContext,
            TUpdatePayload
        >
    ) {
        super(store, cruState, cruService)
    }

    /**
     * Converts a resource into its creation context specific DTO equivalent.
     *
     * This base implementation just returns the passed resource value. Override in your own facade if you need to.
     * @param resource The resource to convert.
     */
    public resourceToCreateContext(resource: TResource): TCreateContext {
        return resource as unknown as TCreateContext
    }

    /**
     * Converts a resource into its update context specific  DTO equivalent.
     *
     * This base implementation just returns the passed resource value. Override in your own facade if you need to.
     * @param resource The resource to convert.
     */
    public resourceToUpdateContext(resource: TResource): TUpdatePayload {
        return resource as unknown as TUpdatePayload
    }

    /**
     * Gets the unique identifier for the resource.
     * This is intended for the non-readable identifier. It could be possible in some scenarios that this is a
     * readable identifier though.
     * @param resource The resource to get the identifier from.
     * @param pathContext Extra context to be used to determine the unique identifier. Optional.
     */
    public abstract getResourceUniqueIdentifier(resource: TResource, pathContext?: TPathContext): string | undefined

    /**
     * Creates or updates a resource depending on whether it already has a unique identifier assigned.
     * @param resource The resource to create or update.
     * @param pathContext The context required to determine the correct path. Optional.
     */
    public upsert(resource: TResource, pathContext: TPathContext = {} as TPathContext): Observable<TResource> {
        this.cruState.setBusy(true)
        return of(this.getResourceUniqueIdentifier(resource, pathContext) === undefined).pipe(
            switchMap((isCreate: boolean) => {
                if (isCreate) {
                    return this.cruService.create(this.resourceToCreateContext(resource))
                } else {
                    return this.cruService.update(this.resourceToUpdateContext(resource), pathContext)
                }
            }),
            catchError((err: HttpErrorResponse) => {
                if (err.status === 409) {
                    // Conflict, return current value in store
                    return this.store.select(this.cruState.token).pipe(
                        map((state: TStateModel) => state.selectedResource),
                        first()
                    )
                }
                if (err.status === 412) {
                    // 412 will contain most recent data in body
                    return of(err.error)
                }
                // Throw all other errors
                return throwError(err)
            }),
            tap(() => {
                this.cruState.setHistoryStale(true)
            }),
            finalize(() => {
                this.cruState.setBusy(false)
            })
        )
    }
}
