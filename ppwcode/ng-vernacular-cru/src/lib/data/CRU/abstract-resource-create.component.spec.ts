/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator'
import { TestCreateComponent } from '../../testing/create-component'
import { RouterTestingModule } from '@angular/router/testing'
import { TestFacade } from '../../testing/cru-facade'
import { of, throwError } from 'rxjs'
import { testingPersons } from '../../testing/testing-person'
import { Router } from '@angular/router'
import { TranslateModule, TranslateService } from '@ngx-translate/core'
import { DynamicFormComponent } from '../../forms/dynamic-form/dynamic-form.component'
import { FormBuilder } from '@angular/forms'

describe('AbstractResourceCreateComponent', () => {
    let spectator: Spectator<TestCreateComponent>
    let router: SpyObject<Router>
    let facade: SpyObject<TestFacade>

    const createComponent = createComponentFactory({
        component: TestCreateComponent,
        imports: [RouterTestingModule, TranslateModule],
        declarations: [DynamicFormComponent],
        providers: [FormBuilder],
        mocks: [Router, TestFacade, TranslateService]
    })

    beforeEach(() => {
        spectator = createComponent({ detectChanges: false })

        facade = spectator.inject(TestFacade)
        facade.selectedResource$ = of(null)

        router = spectator.inject(Router)
        router.navigate.and.returnValue(Promise.resolve())
        router.navigateByUrl.and.returnValue(of(true))

        spectator.detectChanges()
    })

    it('should create', () => {
        expect(spectator.component).toBeTruthy()
    })

    it('should save the resource', async () => {
        facade.upsert.and.returnValue(of(testingPersons.johnDoe))

        spectator.component.dynamicFormComponent.formGroup.patchValue({ firstName: 'John' })
        spectator.component.dynamicFormComponent.formGroup.patchValue({ lastName: 'Smith' })
        spectator.detectChanges()

        await spectator.component.saveForm()

        expect(facade.upsert).toHaveBeenCalledWith({ structureVersion: 1, firstName: 'John', lastName: 'Smith' })
    })

    it('should not navigate if there is no resource returned', async () => {
        facade.upsert.and.returnValue(of(undefined))

        await spectator.component.saveForm()

        expect(router.navigateByUrl).not.toHaveBeenCalled()
        expect(router.navigate).not.toHaveBeenCalled()
    })

    it('should not navigate if there is no ID to navigate to', async () => {
        facade.upsert.and.returnValue(of({ ...testingPersons.johnDoe, id: undefined }))

        await spectator.component.saveForm()

        expect(router.navigateByUrl).not.toHaveBeenCalled()
        expect(router.navigate).not.toHaveBeenCalled()
    })

    it('should catch errors thrown when saving the resource', async () => {
        facade.upsert.and.returnValue(throwError('Mock Error'))

        spectator.component.dynamicFormComponent.formGroup.patchValue({ firstName: 'John' })
        spectator.component.dynamicFormComponent.formGroup.patchValue({ lastName: 'Smith' })
        spectator.detectChanges()

        await spectator.component.saveForm()
        expect(facade.upsert).toHaveBeenCalledWith({ structureVersion: 1, firstName: 'John', lastName: 'Smith' })
    })
})
