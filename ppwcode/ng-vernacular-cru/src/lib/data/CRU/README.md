# Angular Vernacular CRU Implementation

This section of the library is focused on the implementation of managing data in the application.

## Angular Sandbox Pattern

The library encourages you to apply the
[Angular Sandbox Pattern](https://blog.strongbrew.io/A-scalable-angular-architecture-part2/) as the architecture for
your application. It's a scalable architecture that encourages segregation and KISS.

In short, it means that your application will consist of three layers.

Another benefit of this approach is that it improves testability by a lot. We have these smaller KISS services that
don't do much, a facade where it's very easy to mock the dependencies and a UI that doesn't do much more than triggering
the facade and displaying stuff.

### 1. KISS services

KISS services are services that only do what they're supposed to do.

-   E.g.: `PersonsService.getAllPersons` should only call the API to get the persons and nothing else!
-   E.g.: `PersonsState.loadPersons` should only load the persons into the state and nothing else!

These services should never be called directly from the UI components!

### 2. A sandbox

A sandbox can be seen as the orchestrator. It receives a task from the UI and delegates smaller parts of the task to the
KISS services. We call a service with sandbox logic a "facade".

In the following scenario, the UI needs the list of persons to display. The list is loaded from state management if the
persons are loaded in the state.

1. Check if the persons are already in the state.
2. If the persons are loaded, get them from the state and return them. If they aren't, continue with step 3.
3. Delegate the task to load the persons from the API to the `PersonsService`.
4. When the `PersonsService` returns a list of persons, store them in the state.
5. Return the persons that are loaded in the state.

> In step 5, we have both the `PersonsService` response and the persons in the state available to return. The sandbox
> has no idea (and it shouldn't!) what the state service is doing to the person instances before putting them in the
> state. The UI doesn't know (and it shouldn't!) where the data is coming from (state or API), so the data that is
> passed to the UI should eventually always come from the same source to be sure.

Implemented in a sandbox Facade, it could look like this. Notice that we are using an `Observable` as the return type.
This is to ensure that the UI is able to show some kind of progress indicator while it doesn't have the list of persons
to show.

> This is an example of the pattern, not how it should be done with this library!

```typescript
@Injectable({ providedIn: 'root' })
export class PersonsFacade {
    /** Stream containing the list of persons in the state. */
    @Select(PersonsState.personsList) private readonly persons$!: Observable<Array<Person>>

    /** Stream containing whether the list of persons is loaded in the state. */
    @Select(PersonsState.personsListLoaded)
    private readonly personsLoaded$!: Observable<boolean>

    constructor(private readonly personsService: PersonsService, private readonly store: Store) {}

    public getPersons(): Observable<Array<Person>> {
        return this.personsLoaded$.pipe(
            // Step 1
            take(1),
            switchMap((personsLoaded: boolean) => {
                if (personsLoaded) {
                    return this.persons$ // Step 2
                }

                return this.personsService.getAllPersons().pipe(
                    // Step 3
                    tap((persons: Array<Person>) => this.store.dispatch(new PersonsLoaded(persons))), // Step 4
                    switchMap(() => this.persons$) // Step 5
                )
            })
        )
    }
}
```

From this example, we can conclude that the UI only needs to call `PersonsFacade.getPersons` to get the list of persons,
and it doesn't know where they are coming from. The `PersonsService.getAllPersons` only gets the persons and then
returns them. `PersonsFacade.getPersons` orchestrates what happens when the list of persons is requested.

### 3. UI components

The UI components are completely segregated from the KISS services. They should only talk to the sandbox to get things
done because the sandbox knows how to handle it. The UI doesn't need to know where data is coming from or going to, it
should only do "UI stuff" like taking care of what the user is doing (handling clicks, input, navigation, ...).

## `AbstractCruFacade<TResource, TStateModel, TResourceDto>`

The `AbstractCruFacade` class contains a standardized implementation for a facade that handles CRU manipulations on a
specific type of resource.

The three generic types enforce that you have stricter typing when overriding the standardized implementation.

-   `TResource`
    -   The type of resource that the facade is working with.
    -   Because of earlier decisions this type must extend `Record<string, unknown>`
-   `TStateModel`
    -   The type of the model for `TResource` in the state.
    -   This should extend `CruStateModel<TResource>`.
-   `TResourceDto`
    -   The type of the DTO that is returned by the API.

The facade contains logic to ...

-   ... get all resource instances,
    -   They are not stored in the state!
-   ... get a single instance by its identifier,
    -   This is not explicitly stored in the state!
-   ... create or update an instance,
    -   Depending on whether an identifier has been set on the resource, it will call the `create` or `update` method on
        the service.
-   ... select a resource,
    -   Updates the state so that the resource is set as the selected resource.
-   ... converts the resource to its DTO equivalent,
    -   In case of the standardized implementation, it just returns the passed instance.
-   ... set the selectors for state management,
    -   This method must be called at the end of your facade constructors, otherwise state management will not work!
    -   Because this is an abstract implementation, we couldn't use `@Select` decorators as we really need the explicit
        state type. The current implementation is not the best and can be improved.
-   ... enforce you to implement the following abstract methods:
    -   Get the unique identifier of a resource.
    -   Get the most important text to display when the resource is presented to the user, a.k.a. the business key and
        most of the time the value of a `name`-like property.
    -   Get the text to display as the summary of the resource. This can be some information that you want to show next
        to the most important text.

Implementation example:

```typescript
@Injectable({ providedIn: 'root' })
export class PersonsFacade extends AbstractCruFacade<Person, PersonsStateModel, personSchema> {
    public readonly resourceAppPath: string = '/persons'

    constructor(store: Store, personsState: PersonsState, personsService: PersonsService, router: Router) {
        super(
            store,
            personsState, // Passed for `AbstractCruState`
            personsService, // Passed for `AbstractCruService`
            router
        )
        this.setSelectors(PersonsState) // Must be called!
    }

    public getResourceUniqueIdentifier(resource: Person): string | undefined {
        return resource.id?.toString()
    }

    public getResourceDisplayText(resource: Person): string {
        return `${resource.firstName} ${resource.lastName}`
    }

    public getResourceSummaryText(resource: Person): string {
        return `${resource.address.postalCode} ${resource.address.municipality}`
    }

    public override resourceToDto(resource: Person): personSchema {
        return personToDto(resource)
    }
}
```

As you can tell from the example, it's very easy to override the default implementation of the methods.

## GenericCruFacade

Represents an implemented AbstractCruFacade in its most generic way using `Record<string, unknown>`.

Alias to `AbstractCruFacade<Record<string, unknown>, CruStateModel<Record<string, unknown>>, Record<string, unknown>>`.

## CruResourcesManagerService

This service is intended to manage the resources for which a CRU is possible in the application.

The service should know about _all_ resources and their corresponding AbstractCruFacade implementation so that the
generic layers of the library are able to call the correct methods on the facade when necessary.

You can register a resource by calling `registerResource` in the constructor of your app module:

```typescript
@NgModule({
    declarations: [AppComponent, HomeComponent],
    imports: [
        PpwcodeVernacularCruModule
        // ... other imports
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(cruResourceManager: CruResourcesManagerService) {
        cruResourceManager.registerResource('/I/person', PersonsFacade)
        cruResourceManager.registerResource('/I/stock', StocksFacade)
    }
}
```

Internally, the library will call `getFacadeInstance` to get the facade instance from the Angular injector when it needs
to. In case you are developing your own generic implementations, you can call this method as well.
