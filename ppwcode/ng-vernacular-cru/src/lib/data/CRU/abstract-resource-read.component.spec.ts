/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator'
import { RouterTestingModule } from '@angular/router/testing'
import { TestFacade } from '../../testing/cru-facade'
import { firstValueFrom, of } from 'rxjs'
import { TestReadComponent } from '../../testing/read-component'
import { ActivatedRoute, Router } from '@angular/router'
import { testingPersons } from '../../testing/testing-person'
import { NgxsModule } from '@ngxs/store'
import { TestState } from '../../testing/state'
import { TestCruService } from '../../testing/cru-service'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { TestResolver } from '../../testing/resolver'
import { NgxsDispatchPluginModule } from '@ngxs-labs/dispatch-decorator'

describe('AbstractResourceReadComponent', () => {
    let spectator: Spectator<TestReadComponent>
    let router: SpyObject<Router>
    let activatedRoute: SpyObject<ActivatedRoute>
    let facade: TestFacade

    const createComponent = createComponentFactory({
        component: TestReadComponent,
        imports: [
            RouterTestingModule,
            HttpClientTestingModule,
            NgxsModule.forRoot([TestState]),
            NgxsDispatchPluginModule.forRoot()
        ],
        providers: [
            TestFacade,
            TestCruService,
            TestResolver,
            {
                provide: ActivatedRoute,
                useValue: {
                    queryParams: of({}),
                    snapshot: { queryParamMap: new Map(), data: { resourceType: 'person' } }
                }
            }
        ],
        mocks: [Router]
    })

    beforeEach(() => {
        spectator = createComponent({ detectChanges: false })

        facade = spectator.inject(TestFacade)
        facade.selectedResource$ = of(testingPersons.johnDoe)
        facade.selectedResourceHistory$ = of([{ createdAt: testingPersons.johnDoe.createdAt, href: 'hrefvalue' }])

        router = spectator.inject(Router)
        router.navigate.and.returnValue(Promise.resolve())

        activatedRoute = spectator.inject(ActivatedRoute)

        spectator.detectChanges()
    })

    it('should create', () => {
        expect(spectator.component).toBeTruthy()
    })

    it('should resolve the selected resource', async () => {
        await expectAsync(firstValueFrom(spectator.component.selectedResource$)).toBeResolvedTo(testingPersons.johnDoe)
    })

    it('should have isBusy$ flag', async () => {
        await expectAsync(firstValueFrom(spectator.component.isBusy$)).toBeResolvedTo(true)
    })

    it('should correctly determine isLatestVersion$ flag', async () => {
        await expectAsync(firstValueFrom(spectator.component.isLatestVersion$)).toBeResolvedTo(true)
    })

    it('should update the timestamp in the router when changing historic version', async () => {
        const now: string = new Date().toISOString()
        await spectator.component.onVersionSelected({ createdAt: now, href: 'hrefvalue' })

        expect(router.navigate).toHaveBeenCalledWith([], {
            queryParams: { at: now },
            relativeTo: activatedRoute
        })
    })
})
