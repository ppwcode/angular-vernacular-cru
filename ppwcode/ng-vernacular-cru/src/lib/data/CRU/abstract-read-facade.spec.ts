/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { RouterTestingModule } from '@angular/router/testing'
import { createServiceFactory, mockProvider, SpectatorService, SpyObject } from '@ngneat/spectator'
import { NgxsModule, Store } from '@ngxs/store'
import { firstValueFrom, of } from 'rxjs'
import { first } from 'rxjs/operators'

import { TestState } from '../../testing/state'
import { TestingPerson, TestingPersonDto, testingPersons } from '../../testing/testing-person'
import { HistoricVersion } from './historic-version'
import { NgxsDispatchPluginModule } from '@ngxs-labs/dispatch-decorator'
import { NgxsSelectSnapshotModule } from '@ngxs-labs/select-snapshot'
import { PresentationMode } from '../../presentations/presentation-mode'
import { TestFacade } from '../../testing/cru-facade'
import { TestCruService } from '../../testing/cru-service'

describe('AbstractReadFacade', () => {
    let spectator: SpectatorService<TestFacade>
    let service: SpyObject<TestCruService>
    let state: TestState
    let store: Store
    let resource: TestingPerson

    const createService = createServiceFactory({
        service: TestFacade,
        imports: [
            NgxsModule.forRoot([TestState], { developmentMode: true }),
            NgxsDispatchPluginModule.forRoot(),
            NgxsSelectSnapshotModule.forRoot(),
            RouterTestingModule
        ],
        providers: [mockProvider(TestCruService)]
    })

    beforeEach(() => {
        resource = testingPersons.johnDoe
        spectator = createService()
        service = spectator.inject(TestCruService)
        state = spectator.inject(TestState)
        store = spectator.inject(Store)
    })

    afterEach(() => state.reset())

    it('should get whether the state is busy', async () => {
        await expectAsync(spectator.service.isBusy$.pipe(first()).toPromise()).toBeResolvedTo(false)
    })

    it('should get the selected resource, which initially is null', async () => {
        await expectAsync(spectator.service.selectedResource$.pipe(first()).toPromise()).toBeResolvedTo(null)
        spectator.service.selectResource(resource)
        await expectAsync(spectator.service.selectedResource$.pipe(first()).toPromise()).toBeResolvedTo(resource)
    })

    it('should get the selected resource history, which initially is null', async () => {
        await expectAsync(spectator.service.selectedResourceHistory$.pipe(first()).toPromise()).toBeResolvedTo(null)
    })

    it('should get the current presentation mode', async () => {
        await expectAsync(spectator.service.presentationMode$.pipe(first()).toPromise()).toBeResolvedTo(
            PresentationMode.UNSET
        )
    })

    it('should get all resources', () => {
        spectator.service.getAll()
        expect(service.getAll).toHaveBeenCalled()
    })

    it('should get a single resource by its id and load the history', async () => {
        const timestamp: string = new Date().toISOString()
        const historicVersions: Array<HistoricVersion> = [
            {
                createdAt: new Date().toISOString(),
                href: `/api/I/testperson/16`
            }
        ]
        service.get.and.returnValue(of(resource))
        service.getHistory.and.returnValue(of(historicVersions))

        await firstValueFrom(spectator.service.getResourceWithHistory({ id: '16' }, timestamp))

        expect(service.get).toHaveBeenCalledWith({ id: '16' }, timestamp)
        expect(service.getHistory).toHaveBeenCalledWith(`16/history`, false)
    })

    it('should be able to cache bust when loading the history', async () => {
        store.reset({
            ...store.snapshot(),
            teststate: { isHistoryStale: true }
        })
        const timestamp: string = new Date().toISOString()
        const historicVersions: Array<HistoricVersion> = [
            {
                createdAt: new Date().toISOString(),
                href: `/api/I/testperson/16`
            }
        ]
        service.get.and.returnValue(of(resource))
        service.getHistory.and.returnValue(of(historicVersions))

        await firstValueFrom(spectator.service.getResourceWithHistory({ id: '16' }, timestamp))

        expect(service.get).toHaveBeenCalledWith({ id: '16' }, timestamp)
        expect(service.getHistory).toHaveBeenCalledWith(`16/history`, true)
    })

    it('should get a single resource by its id and not load the history', async () => {
        const historicVersions: Array<HistoricVersion> = [
            {
                createdAt: new Date().toISOString(),
                href: `/api/I/testperson/16`
            }
        ]
        spyOn(spectator.service, 'getResourceHistoryPath').and.returnValue(null)
        service.get.and.returnValue(of(resource))
        service.getHistory.and.returnValue(of(historicVersions))

        await firstValueFrom(spectator.service.getResourceWithHistory({ id: '16' }))

        expect(service.get).toHaveBeenCalledWith({ id: '16' }, undefined)
        expect(service.getHistory).not.toHaveBeenCalled()
    })

    it('should get a single resource by its id, without loading the history', async () => {
        const historicVersions: Array<HistoricVersion> = [
            {
                createdAt: new Date().toISOString(),
                href: `/api/I/testperson/16`
            }
        ]
        spyOn(spectator.service, 'getResourceHistoryPath').and.returnValue(null)
        service.get.and.returnValue(of(resource))
        service.getHistory.and.returnValue(of(historicVersions))

        await firstValueFrom(spectator.service.getResource({ id: '16' }))

        expect(service.get).toHaveBeenCalledWith({ id: '16' }, undefined)
        expect(service.getHistory).not.toHaveBeenCalled()
    })

    it('should select a resource', async () => {
        store.reset({
            ...store.snapshot(),
            teststate: { selectedResource: null }
        })
        const expectedNull = store.selectSnapshot((state) => state.teststate.selectedResource)
        expect(expectedNull).toBeNull()
        spectator.service.selectResource(resource)
        const expectedResource = store.selectSnapshot((state) => state.teststate.selectedResource)
        expect(expectedResource).toBe(resource)
    })

    it('should do nothing if same resource is selected twice', async () => {
        spyOn(state, 'selectResource').and.callThrough()

        const expectedResource = store.selectSnapshot((state) => state.teststate.selectedResource)
        expect(expectedResource).toBeNull()

        spectator.service.selectResource(expectedResource)
        // It should not have updated the state.
        expect(state.selectResource).toHaveBeenCalledTimes(0)
    })

    it('should return itself when converting to a dto', () => {
        // Casting because the DTO type is actually different in TestFacade.
        // Here we are just testing the abstract implementation.
        expect(spectator.service.resourceToDto(resource)).toBe(resource as unknown as TestingPersonDto)
    })

    it('should get the display text for a resource', () => {
        expect(spectator.service.getResourceDisplayText(resource)).toEqual('John')
    })

    it('should get the summary text for a resource', () => {
        expect(spectator.service.getResourceSummaryText(resource)).toEqual('16')
    })
})
