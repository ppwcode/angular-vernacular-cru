/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Directive, OnInit } from '@angular/core'
import { AbstractResourceComponent } from './abstract-resource.component'
import { ActivatedRoute, Router } from '@angular/router'
import { switchMap } from 'rxjs/operators'
import { AbstractCruResolver } from '../../routing/abstract-cru-resolver'
import { HistoricVersion } from './historic-version'
import { DateTime } from '@ppwcode/openapi/time/DateTime'
import { AbstractReadFacade } from './abstract-read-facade'
import { GetContext } from '../api-interaction/abstract-read-service'

@Directive()
export abstract class AbstractResourceReadComponent<
        R extends { createdAt: DateTime },
        F extends AbstractReadFacade<R, any, any, G>,
        S extends AbstractCruResolver<R, R, G>,
        G extends GetContext<string> = GetContext<string>
    >
    extends AbstractResourceComponent<R, F, G>
    implements OnInit
{
    protected abstract resolver: S

    constructor(protected readonly router: Router, protected readonly activatedRoute: ActivatedRoute) {
        super()
    }

    public override ngOnInit(): void {
        super.ngOnInit()

        this.stopOnDestroy(this.activatedRoute.queryParams)
            .pipe(switchMap(() => this.resolver.resolve(this.activatedRoute.snapshot)))
            .subscribe()
    }

    public async onVersionSelected(version: HistoricVersion): Promise<void> {
        await this.router.navigate([], {
            queryParams: { at: version.createdAt },
            relativeTo: this.activatedRoute
        })
    }
}
