/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { StateToken, Store } from '@ngxs/store'
import { Observable, of } from 'rxjs'
import { finalize, map, switchMap, tap } from 'rxjs/operators'

import { AbstractCruState } from '../../+state/abstract-cru-state'
import { CruStateModel } from '../../+state/cru-state-model'
import { HistoricVersion } from './historic-version'
import { PresentationMode } from '../../presentations/presentation-mode'
import { mixinHandleSubscriptions } from '../../performance/handle-subscriptions.mixin'
import { AbstractReadService, GetContext } from '../api-interaction/abstract-read-service'

/** Represents an implemented AbstractReadFacade in its most generic way using `Record<string, unknown>`. */
export declare type GenericReadFacade = AbstractReadFacade<
    Record<string, unknown>,
    CruStateModel<Record<string, unknown>>,
    Record<string, unknown>,
    GetContext<string>
>

/**
 * This abstract class represents a base implementation for a read facade.
 *
 * It manages the process of getting resources in the application in a generic way.
 */
export abstract class AbstractReadFacade<
    TResource extends Record<string, unknown>,
    TStateModel extends CruStateModel<TResource>,
    TResourceDto extends Record<string, unknown>,
    TGetContext extends GetContext<string>
> extends mixinHandleSubscriptions() {
    /** Stream emitting when the busy state changes. */
    public isBusy$!: Observable<boolean>

    /** Stream emitting when the selected resource changes. */
    public selectedResource$!: Observable<TResource | null>

    /** Stream emitting the historic versions of the selected resource. */
    public selectedResourceHistory$!: Observable<Array<HistoricVersion> | null>

    /** Stream emitting when the presentationMode state changes. */
    public presentationMode$!: Observable<PresentationMode>

    /** Base path of the resource in the application without a trailing slash. */
    public abstract readonly resourceAppPath: string

    /** Type of the resource this facade deals with. */
    public abstract readonly resourceType: string

    /** Last known value of the isHistoryStale$ observable */
    private isHistoryStale!: boolean

    private readonly stateToken!: StateToken<TStateModel>

    protected constructor(
        protected readonly store: Store,
        protected readonly cruState: AbstractCruState<TResource, TStateModel>,
        protected readonly cruService: AbstractReadService<TResource, TResourceDto, TGetContext>
    ) {
        super()
        this.stateToken = cruState.token
        this.setSelectors(this.stateToken)
    }

    /**
     * Gets a single resource by its identifier.
     * @param context The context for getting the resource. This is usually the ID.
     * @param versionTimestamp An optional specific version to load.
     */
    public getResource(context: TGetContext, versionTimestamp?: string): Observable<TResource> {
        return this.loadResource(context, false, versionTimestamp)
    }

    /**
     * Gets a single resource, including history, by its identifier.
     * @param context The context for getting the resource. This is usually the ID.
     * @param versionTimestamp An optional specific version to load.
     */
    public getResourceWithHistory(context: TGetContext, versionTimestamp?: string): Observable<TResource> {
        return this.loadResource(context, true, versionTimestamp)
    }

    /**
     * Selects a resource in the state if it isn't already.
     * @param resource The resource to select.
     */
    public selectResource(resource: TResource | null): void {
        if (resource === this.store.selectSnapshot(this.stateToken).selectedResource) {
            return
        }

        this.cruState.selectResource(resource)
    }

    /**
     * Converts a resource into its DTO equivalent.
     *
     * This base implementation just returns the passed resource value. Override in your own facade if you need to.
     * @param resource The resource to convert.
     */
    public resourceToDto(resource: TResource): TResourceDto {
        return resource as unknown as TResourceDto
    }

    /**
     * Gets the text to display for the resource.
     * This is intended as a readable identifier for the resource, to be shown to the user.
     * @param resource The resource to get the display text for.
     */
    public abstract getResourceDisplayText(resource: TResource): string

    /**
     * Gets the path from where the history for a resource should be fetched.
     * Returning null will cause the historic version to not be loaded.
     * @param resource The resource to get the path from.
     * @param context Extra context to be used to determine the history path. Optional.
     */
    public abstract getResourceHistoryPath(resource: TResource, context?: TGetContext): string | null

    /**
     * Sets the selectors to get the streams from state management.
     * This function MUST be called in the child constructor at the end.
     * @param stateToken The state token of the state for this facade.
     */
    protected setSelectors(stateToken: StateToken<TStateModel>): void {
        this.selectedResource$ = this.store.select(stateToken).pipe(map((state: TStateModel) => state.selectedResource))
        this.isBusy$ = this.store.select(stateToken).pipe(map((state: TStateModel) => state.isBusy))
        this.selectedResourceHistory$ = this.store
            .select(stateToken)
            .pipe(map((state: TStateModel) => state.selectedResourceHistoricVersions))
        this.presentationMode$ = this.store.select(stateToken).pipe(map((state: TStateModel) => state.presentationMode))

        this.stopOnDestroy(
            this.store.select(stateToken).pipe(map((state: TStateModel) => state.isHistoryStale))
        ).subscribe((isStale: boolean) => {
            this.isHistoryStale = isStale
        })
    }

    private loadResource(context: TGetContext, withHistory: boolean, versionTimestamp?: string): Observable<TResource> {
        this.cruState.setBusy(true)
        return this.cruService.get(context, versionTimestamp).pipe(
            switchMap((resource: TResource) => {
                if (withHistory) {
                    const historyPath: string | null = this.getResourceHistoryPath(resource, context)
                    if (historyPath) {
                        return this.cruService.getHistory(historyPath, this.isHistoryStale).pipe(
                            tap((historicVersions: Array<HistoricVersion>) =>
                                this.cruState.loadHistory(historicVersions)
                            ),
                            map(() => resource)
                        )
                    }
                }
                return of(resource)
            }),
            tap(() => {
                if (this.isHistoryStale) {
                    this.cruState.setHistoryStale(false)
                }
            }),
            finalize(() => {
                this.cruState.setBusy(false)
            })
        )
    }
}
