/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { HttpHeaders, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs'

import { HistoricVersion, HistoricVersionResponse } from '../CRU/historic-version'
import { Endpoint } from './endpoint'
import { EndpointService } from './endpoint.service'
import { HttpMethod } from './http-method'

export type GetContext<T extends string> = Record<T, string>

export type DefaultGetContext = GetContext<'id'>

/**
 * This abstract class represents a base implementation for implementing a read-only service.
 *
 * It provides the necessary functions to ...
 * - get an individual resource by its identifier,
 *
 * The way this can be leveraged is by implementing the abstract `endpoints` property and tell the service
 * how it should treat related endpoints.
 */
export abstract class AbstractReadService<
    TResource extends Record<string, unknown>,
    TResourceDto extends Record<string, unknown>,
    TGetContext extends Record<string, unknown> = DefaultGetContext
> {
    /**
     * Definitions of all the endpoints used by the abstract CRU implementation.
     */
    public abstract endpoints: {
        /** Endpoint to get a single resource by its identifier. */
        get: Endpoint<TResourceDto, TResource, TGetContext>
    }
    protected abstract resourceType: string

    constructor(protected readonly endpointService: EndpointService) {}

    /** Gets a single resource by its identifier. */
    public get(context: TGetContext, versionTimestamp?: string): Observable<TResource> {
        let params: HttpParams = new HttpParams()
        if (versionTimestamp) {
            params = params.set('at', versionTimestamp)
        }
        return this.endpointService.call(this.endpoints.get, context, null, params)
    }

    /** Gets the historic versions from the given path. */
    public getHistory(path: string, bustCache: boolean = false): Observable<Array<HistoricVersion>> {
        const endpoint: Endpoint<HistoricVersionResponse, Array<HistoricVersion>> = new Endpoint<
            HistoricVersionResponse,
            Array<HistoricVersion>,
            null
        >(HttpMethod.GET, path, `${this.resourceType}_history`, null, null, (response) => response.versions)

        return this.endpointService.call(
            endpoint,
            null,
            null,
            undefined,
            bustCache ? new HttpHeaders().set('cache-control', 'no-cache') : undefined
        )
    }
}
