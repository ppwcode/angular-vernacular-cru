/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { HttpClientTestingModule, TestRequest } from '@angular/common/http/testing'
import { createServiceFactory, SpectatorService } from '@ngneat/spectator'
import { HttpCallTester } from '@ppwcode/ng-testing'

import { testingPersonDtos, testingPersons } from '../../testing/testing-person'
import { EndpointService } from './endpoint.service'
import { HistoricVersion } from '../CRU/historic-version'
import { HttpHeaders } from '@angular/common/http'
import { TestCruService } from '../../testing/cru-service'

describe('AbstractCruService', () => {
    let spectator: SpectatorService<TestCruService>
    const createService = createServiceFactory({
        service: TestCruService,
        imports: [HttpClientTestingModule],
        providers: [EndpointService]
    })

    beforeEach(() => {
        spectator = createService()
    })

    it('should get all resources', () => {
        HttpCallTester.expectOneCallToUrl(`/api/I/testperson`)
            .whenSubscribingTo(spectator.service.getAll())
            .withResponse(Object.values(testingPersonDtos))
            .expectStreamResultTo((result) => {
                expect(result).toEqual(Object.values(testingPersons))
            })
            .verify()
    })

    it('should get a resource by its unique identifier', () => {
        HttpCallTester.expectOneCallToUrl(`/api/I/testperson/16`)
            .whenSubscribingTo(spectator.service.get({ id: '16' }))
            .withResponse(testingPersonDtos.johnDoe)
            .expectStreamResultTo((result) => {
                expect(result).toEqual(testingPersons.johnDoe)
            })
            .verify()
    })

    it('should get a resource by its unique identifier for a fixed timestamp', () => {
        const timestamp = new Date().toISOString()
        HttpCallTester.expectOneCallToUrl(`/api/I/testperson/16?at=${timestamp}`)
            .whenSubscribingTo(spectator.service.get({ id: '16' }, timestamp))
            .withResponse(testingPersonDtos.johnDoe)
            .expectStreamResultTo((result) => {
                expect(result).toEqual(testingPersons.johnDoe)
            })
            .verify()
    })

    it('should create a resource', () => {
        HttpCallTester.expectOneCallToUrl(`/api/I/testperson`)
            .whenSubscribingTo(spectator.service.create(testingPersonDtos.johnDoe))
            .withResponse(testingPersonDtos.johnDoe)
            .expectRequestTo((request: TestRequest) => {
                expect(request.request.method).toEqual('POST')
                expect(request.request.body).toEqual(testingPersonDtos.johnDoe)
            })
            .verify()
    })

    it('should update a resource', () => {
        HttpCallTester.expectOneCallToUrl(`/api/I/testperson/16`)
            .whenSubscribingTo(spectator.service.update(testingPersonDtos.johnDoe, testingPersons.johnDoe))
            .withResponse(testingPersonDtos.johnDoe)
            .expectRequestTo((request: TestRequest) => {
                expect(request.request.method).toEqual('PUT')
                expect(request.request.body).toEqual(testingPersonDtos.johnDoe)
            })
            .verify()
    })

    it('should add custom header when a resource', () => {
        const payload = { ...testingPersonDtos.johnDoe, createdAt: 'someTimestamp' }
        HttpCallTester.expectOneCallToUrl(`/api/I/testperson/16`)
            .whenSubscribingTo(spectator.service.update(payload, testingPersons.johnDoe))
            .withResponse(testingPersonDtos.johnDoe)
            .expectRequestTo((request: TestRequest) => {
                expect(request.request.method).toEqual('PUT')
                expect(request.request.body).toEqual(payload)
                expect(request.request.headers).toEqual(new HttpHeaders().set('x-if-unmodified-since', 'someTimestamp'))
            })
            .verify()
    })

    it('should get the history', () => {
        const historicVersions: Array<HistoricVersion> = [
            {
                createdAt: new Date().toISOString(),
                href: `/api/I/testperson/16`
            }
        ]
        HttpCallTester.expectOneCallToUrl(`/api/I/testperson/16/history`)
            .whenSubscribingTo(spectator.service.getHistory(`/api/I/testperson/16/history`))
            .withResponse({ structureVersion: 1, versions: historicVersions })
            .expectStreamResultTo((result) => {
                expect(result).toEqual(historicVersions)
            })
            .verify()
    })

    it('should be able to bust the history caching to retrieve data directly from server', () => {
        const historicVersions: Array<HistoricVersion> = [
            {
                createdAt: new Date().toISOString(),
                href: `/api/I/testperson/16`
            }
        ]
        HttpCallTester.expectOneCallToUrl(`/api/I/testperson/16/history`)
            .whenSubscribingTo(spectator.service.getHistory(`/api/I/testperson/16/history`, true))
            .withResponse({ structureVersion: 1, versions: historicVersions })
            .expectRequestTo((request) => {
                expect(request.request.headers).toEqual(new HttpHeaders().set('cache-control', 'no-cache'))
            })
            .expectStreamResultTo((result) => {
                expect(result).toEqual(historicVersions)
            })
            .verify()
    })
})
