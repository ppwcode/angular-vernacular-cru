import {
    HttpClient,
    HttpContext,
    HttpEvent,
    HttpEventType,
    HttpHeaders,
    HttpParams,
    HttpRequest
} from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable, of } from 'rxjs'
import { filter, map, switchMap, tap } from 'rxjs/operators'

import { Endpoint } from './endpoint'

@Injectable({
    providedIn: 'root'
})
export class EndpointService {
    constructor(protected readonly httpClient: HttpClient) {}

    public call<
        TResponse = unknown,
        TResult = TResponse,
        TContext extends Record<string, unknown> | null = null,
        TPayload extends Record<string, unknown> | null = null
    >(
        endpoint: Endpoint<TResponse, TResult, TContext>,
        context: TContext,
        payload: TPayload,
        params?: HttpParams,
        customHeaders?: HttpHeaders
    ): Observable<TResult> {
        return of(null).pipe(
            switchMap(() => {
                // This validation should be part of the stream so that a developer can act on a stream error instead
                // of a globally thrown exception.
                endpoint.assertValidRequestBody(payload)
                return this.callEndpointPath<TResponse, TPayload>(
                    endpoint.method,
                    endpoint.getEndpointPath(context),
                    endpoint.httpContext,
                    payload,
                    endpoint.httpResponseHandler,
                    params,
                    customHeaders
                )
            }),
            tap((response: TResponse) => endpoint.assertValidResponse(response)),
            map((response: TResponse) => endpoint.mapResponse(response))
        ) as Observable<TResult>
    }

    public callEndpointPath<TResponse, TPayload extends Record<string, unknown> | null = null>(
        method: string,
        endpointPath: string,
        endpointHttpContext: HttpContext,
        body: TPayload,
        httpResponseHandler: (response: HttpEvent<unknown>) => TResponse,
        params?: HttpParams,
        headers?: HttpHeaders
    ): Observable<TResponse> {
        const httpRequest: HttpRequest<TPayload> = new HttpRequest(method, endpointPath, body, {
            context: endpointHttpContext,
            params,
            headers
        })
        return this.httpClient.request(httpRequest).pipe(
            filter((event: HttpEvent<unknown>) => event.type === HttpEventType.Response),
            map(httpResponseHandler)
        )
    }
}
