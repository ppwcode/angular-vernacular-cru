/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Observable } from 'rxjs'
import { Endpoint } from './endpoint'
import { EndpointService } from './endpoint.service'
import { AbstractReadService, DefaultGetContext } from './abstract-read-service'

/**
 * This abstract class represents a base implementation for implementing a read-only service.
 *
 * It provides the necessary functions to ...
 * - get all resources,
 * - get an individual resource by its identifier,
 *
 * The way this can be leveraged is by implementing the abstract `endpoints` property and tell the service
 * how it should treat related endpoints.
 */
export abstract class AbstractReadListService<
    TResource extends Record<string, unknown>,
    TResourceDto extends Record<string, unknown>,
    TGetContext extends Record<string, unknown> = DefaultGetContext
> extends AbstractReadService<TResource, TResourceDto, TGetContext> {
    /**
     * Definitions of all the endpoints used by the abstract CRU implementation.
     */
    public abstract override endpoints: {
        /** Endpoint to get all the resources from. */
        getAll: Endpoint<Array<TResourceDto>, Array<TResource>>
        /** Endpoint to get a single resource by its identifier. */
        get: Endpoint<TResourceDto, TResource, TGetContext>
    }

    constructor(endpointService: EndpointService) {
        super(endpointService)
    }

    /** Gets all the resources. */
    public getAll(): Observable<Array<TResource>> {
        return this.endpointService.call(this.endpoints.getAll, null, null)
    }
}
