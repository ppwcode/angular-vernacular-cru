import { createServiceFactory, SpectatorService } from '@ngneat/spectator'
import { EndpointService } from './endpoint.service'
import { HttpCallTester } from '@ppwcode/ng-testing'
import { Endpoint } from './endpoint'
import {
    TestingPerson,
    TestingPersonDto,
    testingPersonDtos,
    testingPersonFromDto,
    testingPersons
} from '../../testing/testing-person'
import Joi from 'joi'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { first } from 'rxjs/operators'
import { HttpMethod } from './http-method'
import { HttpHeaders } from '@angular/common/http'

describe('EndpointService', () => {
    let spectator: SpectatorService<EndpointService>
    const createService = createServiceFactory({
        service: EndpointService,
        imports: [HttpClientTestingModule]
    })

    let staticEndpoint!: Endpoint<TestingPersonDto, TestingPersonDto, TestingPersonDto>
    let dynamicEndpoint!: Endpoint<TestingPersonDto, TestingPerson, TestingPersonDto>
    let requestBodySchema!: Joi.Schema
    let responseSchema!: Joi.Schema

    beforeEach(() => {
        requestBodySchema = Joi.object({
            personId: Joi.string().required(),
            firstName: Joi.string().required(),
            createdAt: Joi.string().required()
        })
        responseSchema = requestBodySchema

        staticEndpoint = new Endpoint(HttpMethod.GET, '/api', 'test')
        dynamicEndpoint = new Endpoint(
            HttpMethod.GET,
            (person: TestingPersonDto) => `/api/persons/${person.personId}`,
            'test',
            requestBodySchema,
            responseSchema,
            testingPersonFromDto
        )
    })

    beforeEach(() => (spectator = createService()))

    describe('endpoints', () => {
        it('should throw on an invalid request body', async () => {
            await expectAsync(
                spectator.service
                    .call<TestingPersonDto, TestingPerson, TestingPersonDto>(
                        dynamicEndpoint,
                        {} as TestingPersonDto,
                        null
                    )
                    .pipe(first())
                    .toPromise()
            ).toBeRejected()
        })

        it('should throw on an invalid response', () => {
            HttpCallTester.expectOneCallToUrl('/api/persons/16')
                .whenSubscribingTo(
                    spectator.service.call<TestingPersonDto, TestingPerson, TestingPersonDto, TestingPersonDto>(
                        dynamicEndpoint,
                        testingPersonDtos.johnDoe,
                        testingPersonDtos.johnDoe
                    )
                )
                .withResponse({} as TestingPersonDto)
                .verifyFailure()
        })

        it('should get a static endpoint path without response mapping', () => {
            HttpCallTester.expectOneCallToUrl('/api')
                .whenSubscribingTo(
                    spectator.service.call<TestingPersonDto, TestingPersonDto, TestingPersonDto>(
                        staticEndpoint,
                        testingPersonDtos.johnDoe,
                        null
                    )
                )
                .withResponse(testingPersonDtos.johnDoe)
                .expectStreamResultTo((result) => {
                    expect(result).toEqual(testingPersonDtos.johnDoe)
                })
                .verify()
        })

        it('should get a dynamic endpoint path with response mapping', () => {
            HttpCallTester.expectOneCallToUrl('/api/persons/16')
                .whenSubscribingTo(
                    spectator.service.call<TestingPersonDto, TestingPerson, TestingPersonDto, TestingPersonDto>(
                        dynamicEndpoint,
                        testingPersonDtos.johnDoe,
                        testingPersonDtos.johnDoe
                    )
                )
                .withResponse(testingPersonDtos.johnDoe)
                .expectStreamResultTo((result) => {
                    expect(result).toEqual(testingPersons.johnDoe)
                })
                .verify()
        })

        it('should be able to set custom headers on a request', () => {
            const headers = new HttpHeaders().set('cache-control', 'no-cache').set('x-if-unmodified-since', 'test')
            HttpCallTester.expectOneCallToUrl('/api/persons/16')
                .whenSubscribingTo(
                    spectator.service.call<TestingPersonDto, TestingPerson, TestingPersonDto, TestingPersonDto>(
                        dynamicEndpoint,
                        testingPersonDtos.johnDoe,
                        testingPersonDtos.johnDoe,
                        undefined,
                        headers
                    )
                )
                .withResponse(testingPersonDtos.johnDoe)
                .expectRequestTo((req) => {
                    expect(req.request.headers).toEqual(headers)
                })
                .expectStreamResultTo((result) => {
                    expect(result).toEqual(testingPersons.johnDoe)
                })
                .verify()
        })
    })
})
