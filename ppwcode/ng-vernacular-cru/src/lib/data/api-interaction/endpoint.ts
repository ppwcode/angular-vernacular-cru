/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import * as Joi from 'joi'
import { HttpMethod } from './http-method'
import { HttpContext, HttpContextToken, HttpEvent, HttpResponse } from '@angular/common/http'

/* istanbul ignore next */
export const RESOURCE_TYPE = new HttpContextToken(() => undefined)

/**
 * Contains information about an endpoint and how the application should interact with the endpoint.
 *
 * <TResponse> Represents the type of the API response.
 * <TResult> Represents the type of the result of the endpoint after mapping.
 * <TContext> Represents the type of the context used to determine dynamic endpoint paths.
 */
export class Endpoint<
    TResponse = unknown,
    TResult = TResponse,
    TContext extends Record<string, unknown> | null = null
> {
    constructor(
        /**
         * The method used to call the endpoint.
         */
        public method: HttpMethod,
        /**
         * The path of the endpoint or a function returning the path of the endpoint with dynamic values inserted.
         */
        public endpointPath: string | ((context: TContext) => string),
        /**
         * The HTTP context for this endpoint.
         * Used to tailor error messages etc. to the specific context of this endpoint.
         */
        public endpointHttpContext: string | undefined,
        /**
         * The Joi schema used to validate whether the request body matches expectations.
         * Please specify a schema to ensure a single point of truth in the application.
         */
        public requestBodySchema: Joi.Schema | null = null,
        /**
         * The Joi schema used to validate whether the API has responded with an expected type of response.
         * Please specify a schema to ensure a single point of truth in the application.
         */
        public responseSchema: Joi.Schema | null = null,
        /**
         * Optional mapper function to map the API response to a local structure.
         * This is mostly useful for mapping date values or reorganizing properties.
         * Called after validating the response using the response schema.
         */
        public responseMapper: ((response: TResponse) => TResult) | null = null,
        /**
         * Optional mapper function to map the raw HttpResponse to an API response.
         * This is mostly useful for accessing headers on the HttpResponse.
         * Called right after receiving the response, before validation and mapping.
         */
        public httpResponseHandler: (response: HttpEvent<unknown>) => TResponse = (response: HttpEvent<unknown>) =>
            (response as HttpResponse<TResponse>).body as unknown as TResponse
    ) {}

    /**
     * Gets the endpoint path to call.
     *
     * The given value for the `context` parameter will be passed to the `endpointPath` function so that you can
     * include these values in the endpoint path. The parameter is ignored if the `endpointPath` value is a string.
     * @param context The context values used for determining the endpoint path.
     */
    public getEndpointPath(context?: TContext): string {
        if (typeof this.endpointPath === 'string') {
            return this.endpointPath
        }

        return this.endpointPath(context ?? ({} as TContext))
    }

    /**
     * Asserts that the given request body matches the schema for the request body.
     * If no request body schema is configured for the endpoint, the request body is considered to be valid.
     * @throws a `Joi.ValidationError` if the request body fails to match the schema.
     * @param requestBody The request body value to validate.
     */
    public assertValidRequestBody(requestBody: unknown): void {
        if (this.requestBodySchema === null) {
            return
        }

        const { error }: Joi.ValidationResult = this.requestBodySchema.validate(requestBody)
        if (error) {
            throw error
        }
    }

    /**
     * Asserts that the given response matches the schema for the response.
     * If no response schema is configured for the endpoint, the response is considered to be valid.
     * @throws a `Joi.ValidationError` if the response fails to match the schema.
     * @param response The response value to validate.
     */
    public assertValidResponse(response: TResponse): void {
        if (this.responseSchema === null) {
            // When no schema is given for the response, this is considered to be valid.
            return
        }

        const { error }: Joi.ValidationResult = this.responseSchema.validate(response)
        if (error) {
            throw error
        }
    }

    /**
     * Maps the given API response to a local structure.
     * If no mapper function is configured, the response is returned as-is.
     * @param response The response to map.
     */
    public mapResponse(response: TResponse): TResult {
        if (this.responseMapper === null) {
            return response as unknown as TResult
        }

        return this.responseMapper(response)
    }

    /**
     * Takes the raw HTTP response and maps it to an API response.
     * Usually this means returning the body of the response.
     * @param response The response to handle.
     */
    public handleHttpResponse(response: HttpResponse<TResponse>): TResponse {
        return this.httpResponseHandler(response)
    }

    public get httpContext(): HttpContext {
        return new HttpContext().set(RESOURCE_TYPE, this.endpointHttpContext)
    }
}
