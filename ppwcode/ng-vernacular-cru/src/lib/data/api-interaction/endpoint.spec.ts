/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import Joi from 'joi'
import { Endpoint } from './endpoint'
import {
    TestingPerson,
    TestingPersonDto,
    testingPersonDtos,
    testingPersonFromDto,
    testingPersons
} from '../../testing/testing-person'
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert'
import { HttpMethod } from './http-method'
import { HttpResponse } from '@angular/common/http'

describe('Endpoint', () => {
    let staticEndpoint!: Endpoint<TestingPersonDto, TestingPersonDto, TestingPerson>
    let dynamicEndpoint!: Endpoint<TestingPersonDto, TestingPerson, TestingPerson>
    let requestBodySchema!: Joi.Schema
    let responseSchema!: Joi.Schema

    beforeEach(() => {
        requestBodySchema = Joi.object({
            personId: Joi.string().required(),
            firstName: Joi.string().required(),
            createdAt: Joi.string().required()
        })
        responseSchema = requestBodySchema

        staticEndpoint = new Endpoint(HttpMethod.GET, '/api', 'test')
        dynamicEndpoint = new Endpoint(
            HttpMethod.GET,
            (person: TestingPerson) => `/api/persons/${person.id}`,
            'test',
            requestBodySchema,
            responseSchema,
            testingPersonFromDto
        )
    })

    it('should store the method used to call the api', () => {
        expect(staticEndpoint.method).toEqual(HttpMethod.GET)
        expect(dynamicEndpoint.method).toEqual(HttpMethod.GET)
    })

    it('should return a static url', () => {
        expect(staticEndpoint.getEndpointPath()).toEqual('/api')
        expect(staticEndpoint.getEndpointPath(testingPersons.johnDoe)).toEqual('/api')
    })

    it('should return a dynamically generated url based on context values', () => {
        expect(dynamicEndpoint.getEndpointPath()).toEqual('/api/persons/undefined')
        expect(dynamicEndpoint.getEndpointPath(testingPersons.johnDoe)).toEqual('/api/persons/16')
    })

    it('should validate the request body using a Joi schema', () => {
        // null as schema
        expect(() => staticEndpoint.assertValidRequestBody({})).not.toThrow()

        // Joi.object as schema
        expect(() => dynamicEndpoint.assertValidRequestBody({})).toThrow()
        expect(() => dynamicEndpoint.assertValidRequestBody(testingPersonDtos.johnDoe)).not.toThrow()
    })

    it('should validate the response body using a Joi schema', () => {
        // null as schema
        expect(() => staticEndpoint.assertValidResponse({} as TestingPersonDto)).not.toThrow()

        // Joi.object as schema
        expect(() => dynamicEndpoint.assertValidResponse({} as TestingPersonDto)).toThrow()
        expect(() => dynamicEndpoint.assertValidResponse(testingPersonDtos.johnDoe)).not.toThrow()
    })

    it('should map the response body', () => {
        // null as mapper
        const noMapperResult: TestingPersonDto = staticEndpoint.mapResponse(testingPersonDtos.johnDoe)
        expect(noMapperResult).toEqual(testingPersonDtos.johnDoe)

        // personFromDto mapper
        const mapperResult: TestingPerson = dynamicEndpoint.mapResponse(testingPersonDtos.johnDoe)
        expect(mapperResult).toEqual(notUndefined(testingPersons.johnDoe))
    })

    it('should handle the HTTP response', () => {
        // default mapper
        const defaultMapperResult: TestingPersonDto = staticEndpoint.handleHttpResponse({
            body: testingPersonDtos.johnDoe
        } as HttpResponse<TestingPersonDto>)
        expect(defaultMapperResult).toEqual(testingPersonDtos.johnDoe)
    })
})
