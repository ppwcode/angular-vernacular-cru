# Angular Vernacular CRU API Interaction

This section of the library is focused on interaction made with an API.

## `Endpoint<TResponse, TResult, TContext>`

An `Endpoint` instance represents information about an and how the application should interact with it.

It consists of a few generic types:

-   `TResponse`
    -   The type of the response that is returned by the API. This is the DTO type.
    -   Defaults to `unknown`.
-   `TResult`
    -   The type of the result after mapping the response. This is the type used in the application.
    -   Defaults to `TResponse`.
-   `TContext`
    -   The type of the context used to determine dynamic endpoint paths. Used for type safety of the `endpointPath`
        function.
    -   Defaults to `null`, otherwise it should extend `Record<string, unknown>`.

An endpoint contains information about the `method` it should use to call the API, the path on the API it should call,
schemas for validating the request body and response, and an optional mapper to map the result into a model used in the
application.

The endpoint path could also be a function that returns the path. This is used for generating paths where parameters
should be filled out.

The `Endpoint` class has several methods that developers can (and the library will) use to verify stuff before calling
the endpoint.

### `getEndpointPath`

Gets the endpoint path to call.

The given value for the `context` parameter will be passed to the `endpointPath` function so that you can include these
values in the endpoint path. The parameter is ignored if the `endpointPath` value is a string.

### `assertValidRequestBody`

Asserts that the given request body matches the schema for the request body. If no request body schema is configured for
the endpoint, the request body is considered to be valid.

This function returns nothing because it acts as an assertion. It'll throw if an error is found in the request body.

### `assertValidResponse`

Asserts that the given response matches the schema for the response. If no response schema is configured for the
endpoint, the response is considered to be valid.

### `mapResponse`

Maps the given API response to a local structure. If no mapper function is configured, the response is returned as-is.

### Example

```typescript
const getSingleStockEndpoint = new Endpoint<Stock, Stock, { identifier: string }>(
    HttpMethod.GET,
    (context: { identifier: string }) => `/api/I/stock/${context.identifier}`,
    null,
    Stock.tailor('read')
)
```

## `EndpointService`

The `EndpointService` leverages the information stored and functionality available in an `Endpoint`.

Using the `call` method, you can call the given endpoint with the given context and the following process will take
place:

1. Start an `Observable` so that any validations before the actual endpoint call can be handled in the same way as if
   the endpoint call fails.
2. Assert that the request body is valid by passing the context.
3. Generate the endpoint path if still necessary.
4. Instantiate an `HttpRequest`.
5. Send the request.
6. Extract the response body from the request.
7. Validate the response body.
8. Map the response to the application specific model.

Steps 4, 5 and 6 are handled separately by `callEndpointPath`. You can call this method as well, but be aware that it
bypasses validations entirely as this method is isolated and thus unaware of `Endpoint` specific logic.

## `AbstractCruService<TResource, TResourceDto, TCreateContext, TUpdateContext>`

The `AbstractCruService` is a standardized implementation for calling an API that supports CRU operations.

You need to extend this service, as it will force you to implement the `endpoints` property. This property forces you to
create an `Endpoint` definition to get all the resource, get a single resource by its identifier, create a new resource
and update an existing resource.

> NOTE
>
> In case you are using the same endpoint to create or update a resource, duplicate the definition and assign it to both
> properties of the `endpoints` property. We understand that this results in duplicate code, but it gets you going with
> what you expect the service to do and allows us to focus on other parts of the library for the time being.

For all endpoints defined in the `endpoints` property there are related methods available. It uses the functionality
provided in the `EndpointService` to do the actual calls to the endpoints.

There are some generics that need to be applied to this service:

-   `TResource`
    -   The type of the resource used throughout the application.
-   `TResourceDto`
    -   The type of the resource when returned by the API or sent in the body.
-   `TCreateContext`
    -   Defaults to `TResourceDto`.
    -   Allows you to explicitly set a different DTO type for when you are creating a resource instance.
-   `TUpdateContext`
    -   Defaults to `TResourceDto`.
    -   Allows you to explicitly set a different DTO type for when you are updating a resource instance.
