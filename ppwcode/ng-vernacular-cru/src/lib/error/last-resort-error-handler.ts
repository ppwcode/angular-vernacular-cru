import { ErrorHandler, Inject, Injectable, InjectionToken } from '@angular/core'

export interface LastResortConfig {
    isDev: () => boolean
}
export const LAST_RESORT_CONFIG = new InjectionToken<LastResortConfig>('LastResortConfigToken')
export const WINDOW_TOKEN = new InjectionToken('Window')

@Injectable({ providedIn: 'root' })
export class LastResortErrorHandler implements ErrorHandler {
    constructor(
        @Inject(LAST_RESORT_CONFIG) private readonly config: LastResortConfig,
        @Inject(WINDOW_TOKEN) private readonly window: Window
    ) {}

    handleError(error: unknown): void {
        console.error(error)
        if (this.config.isDev()) {
            const confirmation = confirm(
                'An unhandled error has occurred. We apologize for the inconvenience. Please reload the page and try again. If this problem occurs again, please contact support. Ok reloads the root, cancel continues.'
            )
            // istanbul ignore if
            if (confirmation) {
                this.window.location.assign('/')
            }
        } else {
            alert(
                'An unhandled error has occurred. We apologize for the inconvenience. Please reload the page and try again. If this problem occurs again, please contact support.'
            )
            this.window.location.assign('/')
        }
    }
}
