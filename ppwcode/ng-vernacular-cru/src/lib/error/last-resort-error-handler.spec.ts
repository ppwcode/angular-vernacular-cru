import { TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { LAST_RESORT_CONFIG, LastResortErrorHandler, WINDOW_TOKEN } from './last-resort-error-handler'
import createSpy = jasmine.createSpy
import { BehaviorSubject } from 'rxjs'

const assignSpy = createSpy('assign')
const windowMock = {
    location: { ...window.location, assign: assignSpy } as unknown as Location
}

describe('last resort error handler', () => {
    let errorHandler: LastResortErrorHandler
    let isDev$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true)

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            providers: [
                { provide: WINDOW_TOKEN, useValue: windowMock },
                { provide: LAST_RESORT_CONFIG, useValue: { isDev: () => isDev$.getValue() } }
            ]
        })
        errorHandler = TestBed.inject(LastResortErrorHandler)
    })

    describe('handleError', () => {
        it('should handle an error sent to the error handler, and not show a popup on the page', () => {
            isDev$.next(true)
            spyOn(console, 'error')
            spyOn(window, 'alert')

            const error: Error = new Error('Something went wrong')

            errorHandler.handleError(error)
            expect(console.error).toHaveBeenCalledWith(error)
            expect(window.alert).not.toHaveBeenCalled()
        })

        it('should reload if productionMode is true', () => {
            isDev$.next(false)
            spyOn(console, 'error')
            spyOn(window, 'alert')

            const error: Error = new Error('Something went wrong')

            errorHandler.handleError(error)
            expect(console.error).toHaveBeenCalledWith(error)
            expect(window.alert).toHaveBeenCalled()
            expect(assignSpy).toHaveBeenCalledWith('/')
        })
    })
})
