import { ChangeDetectionStrategy, Component, Input } from '@angular/core'

@Component({
    selector: 'ppwcode-cru-spinner',
    templateUrl: './spinner.component.html',
    styleUrls: ['./spinner.component.scss', '../tailwind.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpinnerComponent {
    @Input() public color: 'primary' | 'accent' | 'warn' = 'accent'
    @Input() public backdropColor: string = 'rgba(0,0,0,0.25)'
    @Input() public diameter: number = 100
}
