import { createComponentFactory, Spectator } from '@ngneat/spectator'

import { SpinnerComponent } from './spinner.component'
import { MatLegacyProgressSpinnerModule as MatProgressSpinnerModule } from '@angular/material/legacy-progress-spinner'

describe('SpinnerComponent', () => {
    let spectator: Spectator<SpinnerComponent>
    const createComponent = createComponentFactory({
        component: SpinnerComponent,
        imports: [MatProgressSpinnerModule]
    })

    beforeEach(() => {
        spectator = createComponent()
    })

    it('should create', () => {
        expect(spectator.component).toBeTruthy()
    })
})
