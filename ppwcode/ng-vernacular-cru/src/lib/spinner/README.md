# Angular Vernacular CRU Spinner

This section of the library is focused on a standardized spinner.

## `SpinnerComponent`

This component is an abstraction of an Angular Material spinner component. If used throughout the application, it
ensures that all spinners look the same. If we were to change the default spinner used in our projects, we would only
need to update the implementation here in the library, and you would benefit from it by only needing to upgrade.

```html
<ppwcode-cru-spinner *ngIf="isBusy$ | async"></ppwcode-cru-spinner>
```
