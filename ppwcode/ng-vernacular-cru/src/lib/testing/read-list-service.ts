/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Injectable } from '@angular/core'
import { Endpoint } from '../data/api-interaction/endpoint'
import { EndpointService } from '../data/api-interaction/endpoint.service'
import { TestingPerson, TestingPersonDto, testingPersonFromDto } from './testing-person'
import { HttpMethod } from '../data/api-interaction/http-method'
import { AbstractReadListService } from '../data/api-interaction/abstract-read-list-service'
import { DefaultGetContext } from '../data/api-interaction/abstract-read-service'

@Injectable()
export class TestReadListService extends AbstractReadListService<TestingPerson, TestingPersonDto> {
    protected resourceType: string = 'test'

    public endpoints: {
        getAll: Endpoint<Array<TestingPersonDto>, Array<TestingPerson>>
        get: Endpoint<TestingPersonDto, TestingPerson, DefaultGetContext>
    } = {
        getAll: new Endpoint<Array<TestingPersonDto>, Array<TestingPerson>>(
            HttpMethod.GET,
            '/api/I/testperson',
            this.resourceType,
            null,
            null,
            (persons: Array<TestingPersonDto>) => persons.map(testingPersonFromDto)
        ),
        get: new Endpoint<TestingPersonDto, TestingPerson, DefaultGetContext>(
            HttpMethod.GET,
            (context: DefaultGetContext) => `/api/I/testperson/${context.id}`,
            this.resourceType,
            null,
            null,
            testingPersonFromDto
        )
    }
    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor(endpointService: EndpointService) {
        super(endpointService)
    }
}
