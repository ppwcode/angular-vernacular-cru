/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { AbstractResourceReadComponent } from '../data/CRU/abstract-resource-read.component'
import { TestingPerson } from './testing-person'
import { TestFacade } from './cru-facade'
import { TestResolver } from './resolver'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
    selector: 'ppwcode-cru-test-read',
    template: '',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestReadComponent extends AbstractResourceReadComponent<TestingPerson, TestFacade, TestResolver> {
    constructor(public facade: TestFacade, protected resolver: TestResolver, router: Router, route: ActivatedRoute) {
        super(router, route)
    }
}
