/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Injectable } from '@angular/core'
import { Store } from '@ngxs/store'
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert'

import { TestState, TestStateModel } from './state'
import { TestingPerson, TestingPersonDto } from './testing-person'
import { TestCruService } from './cru-service'
import { AbstractCruFacade } from '../data/CRU/abstract-cru-facade'
import { GetContext } from '../data/api-interaction/abstract-read-service'

@Injectable()
export class TestFacade extends AbstractCruFacade<TestingPerson, TestStateModel, TestingPersonDto, GetContext<'id'>> {
    public readonly resourceAppPath: string = '/testpersons'
    readonly resourceType: string = 'person'

    constructor(store: Store, state: TestState, service: TestCruService) {
        super(store, state, service)
        this.setSelectors(state.token)
    }

    public getResourceUniqueIdentifier(resource: TestingPerson): string | undefined {
        return resource.id?.toString()
    }

    public getResourceDisplayText(resource: TestingPerson): string {
        return resource.firstName
    }

    public getResourceSummaryText(resource: TestingPerson): string {
        return notUndefined(resource.id).toString()
    }

    public getResourceHistoryPath(resource: TestingPerson): string | null {
        return `${resource.id}/history`
    }
}
