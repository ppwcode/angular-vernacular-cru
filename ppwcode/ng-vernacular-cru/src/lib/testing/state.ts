/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Injectable } from '@angular/core'
import { State } from '@ngxs/store'

import { AbstractCruState } from '../+state/abstract-cru-state'
import { CruStateModel, getDefaultCruState } from '../+state/cru-state-model'
import { TestingPerson } from './testing-person'

export declare type TestStateModel = CruStateModel<TestingPerson>

const STATE_IDENTIFIER = 'teststate'

@State<TestStateModel>({
    name: STATE_IDENTIFIER,
    defaults: getDefaultCruState<TestingPerson>()
})
@Injectable()
export class TestState extends AbstractCruState<TestingPerson, TestStateModel> {
    protected stateIdentifier: string = STATE_IDENTIFIER
}
