/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { DateTime } from '@ppwcode/openapi/time/DateTime'
import { SearchResult } from '../search/search-results'

export interface TestingSearchResultDto extends SearchResult {
    personId?: string
    firstName: string
    createdAt: string
}

export interface TestingSearchResult extends SearchResult {
    id?: string
    firstName: string
    createdAt: DateTime
}

export const testingPersonFromDto = (personDto: TestingSearchResultDto): TestingSearchResult => ({
    id: personDto.personId,
    firstName: personDto.firstName,
    createdAt: personDto.createdAt,
    discriminator: personDto.discriminator,
    href: personDto.href
})

export const testingSearchResultDtos: { johnDoe: TestingSearchResultDto } = {
    johnDoe: {
        personId: '16',
        firstName: 'John',
        createdAt: '2020-01-23T15:22:39.212Z',
        discriminator: 'person',
        href: '/person/doe/john'
    }
}

export const testingSearchResults: { johnDoe: TestingSearchResult } = {
    johnDoe: testingPersonFromDto(testingSearchResultDtos.johnDoe)
}
