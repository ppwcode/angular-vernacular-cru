/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { TestingPerson } from './testing-person'
import { TestFacade } from './cru-facade'
import { ActivatedRoute, Router } from '@angular/router'
import { AbstractResourceUpdateComponent } from '../data/CRU/abstract-resource-update.component'
import { AbstractControlGroup } from '../forms/control-groups/control-groups'
import { TextControl } from '../forms/controls/text-control/text-control'
import { VerticalControls } from '../forms/control-groups/vertical-controls'

@Component({
    selector: 'ppwcode-cru-test-update',
    template:
        '<ng-container *ngIf="selectedResource$ | async as selectedResource">' +
        '            <ppwcode-cru-dynamic-form' +
        '                #dynamicFormComponent' +
        '                [controlGroups]="controlGroups"' +
        '                [initialValue]="selectedAffiliate"' +
        '                [validators]="validators"></ppwcode-cru-dynamic-form>' +
        '</ng-container>',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestUpdateComponent extends AbstractResourceUpdateComponent<TestingPerson, TestFacade> {
    public controlGroups: Array<AbstractControlGroup> = [
        new VerticalControls([
            new TextControl('firstName', { label: 'person.first_name' }),
            new TextControl('lastName', { label: 'person.last_name' })
        ])
    ]

    constructor(public facade: TestFacade, router: Router, route: ActivatedRoute) {
        super(router, route)
    }
}
