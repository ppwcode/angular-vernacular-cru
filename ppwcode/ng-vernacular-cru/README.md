# NgVernacularCru

> NOTE
>
> This library is still in early stages of development. Lots of architectural decisions still need to be made based on
> real-world projects. Please use this library, but only in case you are also prepared to extend the library and sit
> together with other developers to conclude on a way forward for your missing functionality.
>
> Where changes or considerations should still be made, this is clarified in the README docs using a NOTE like this.

## Documentation

The library is documented inside the `src/lib` folder with README files.

-   [State management](./src/lib/+state/README.md)
-   [App Frame](./src/lib/app-frame/README.md)
-   [Breadcrumbs](./src/lib/breadcrumbs/README.md)
-   [Data - Api Interaction](./src/lib/data/api-interaction/README.md)
-   [Data - CRU](./src/lib/data/CRU/README.md)
-   [Decorators](./src/lib/decorators/README.md)
-   [Dynamic Components](./src/lib/dynamic-components/README.md)
-   [Forms](./src/lib/forms/README.md)
-   [Performance](./src/lib/performance/README.md)
-   [Presentations](./src/lib/presentations/README.md)
-   [Routing](./src/lib/routing/README.md)
-   [Search](./src/lib/search/README.md)
-   [Spinner](./src/lib/spinner/README.md)

## Extending the library

### Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Publishing

After building your library with `npm run build`, go to the dist folder `cd dist/ppwcode/ng-vernacular-cru` and run
`npm publish`.

### Running unit tests

Run `ng test ng-vernacular-cru` to execute the unit tests via [Karma](https://karma-runner.github.io).
